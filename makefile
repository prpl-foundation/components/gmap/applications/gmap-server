include makefile.inc

NOW = $(shell date +"%Y-%m-%d(%H:%M:%S %z)")

# Extra destination directories
PKGDIR = ./output/$(MACHINE)/pkg/

define create_changelog
	@$(ECHO) "Update changelog"
	mv CHANGELOG.md CHANGELOG.md.bak
	head -n 9 CHANGELOG.md.bak > CHANGELOG.md
	$(ECHO) "" >> CHANGELOG.md
	$(ECHO) "## Release $(VERSION) - $(NOW)" >> CHANGELOG.md
	$(ECHO) "" >> CHANGELOG.md
	$(GIT) log --pretty=format:"- %s" $$($(GIT) describe --tags | grep -v "merge" | cut -d'-' -f1)..HEAD  >> CHANGELOG.md
	$(ECHO) "" >> CHANGELOG.md
	tail -n +10 CHANGELOG.md.bak >> CHANGELOG.md
	rm CHANGELOG.md.bak
endef

# targets
all:
	$(MAKE) -C src all

clean:
	$(MAKE) -C src clean
	$(MAKE) -C test clean
	$(MAKE) -C doc clean

install: all
	$(INSTALL) -D -p -m 0644 odl/defaults.d/gmap-server-defaults.odl $(DEST)/etc/amx/$(COMPONENT)/defaults.d/gmap-server-defaults.odl
	$(INSTALL) -D -p -m 0644 odl/gmap-server-definitions.odl $(DEST)/etc/amx/$(COMPONENT)/gmap-server-definitions.odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)/config_definition.odl $(DEST)/etc/amx/$(COMPONENT)/$(COMPONENT)/config_definition.odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)/device_definition.odl $(DEST)/etc/amx/$(COMPONENT)/$(COMPONENT)/device_definition.odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)/device_active_definition.odl $(DEST)/etc/amx/$(COMPONENT)/$(COMPONENT)/device_active_definition.odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)/query_definition.odl $(DEST)/etc/amx/$(COMPONENT)/$(COMPONENT)/query_definition.odl
	$(INSTALL) -D -p -m 0755 odl/gmap-server.odl $(DEST)/etc/amx/$(COMPONENT)/gmap-server.odl
	$(INSTALL) -D -p -m 0660 acl/admin/$(COMPONENT).json $(DEST)$(ACLDIR)/admin/$(COMPONENT).json
	$(INSTALL) -D -p -m 0660 acl/operator/$(COMPONENT).json $(DEST)$(ACLDIR)/operator/$(COMPONENT).json
	$(INSTALL) -d -m 0755 $(DEST)$(BINDIR)
	ln -sfr $(DEST)$(BINDIR)/amxrt $(DEST)$(BINDIR)/$(COMPONENT)
	$(INSTALL) -D -p -m 0644 output/$(MACHINE)/$(COMPONENT).so $(DEST)/usr/lib/amx/$(COMPONENT)/$(COMPONENT).so
	$(INSTALL) -D -p -m 0755 scripts/$(COMPONENT).sh $(DEST)$(INITDIR)/$(COMPONENT)

package: all
	$(INSTALL) -D -p -m 0644 odl/defaults.d/gmap-server-defaults.odl $(PKGDIR)/etc/amx/$(COMPONENT)/defaults.d/gmap-server-defaults.odl
	$(INSTALL) -D -p -m 0644 odl/gmap-server-definitions.odl $(PKGDIR)/etc/amx/$(COMPONENT)/gmap-server-definitions.odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)/config_definition.odl $(PKGDIR)/etc/amx/$(COMPONENT)/$(COMPONENT)/config_definition.odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)/device_definition.odl $(PKGDIR)/etc/amx/$(COMPONENT)/$(COMPONENT)/device_definition.odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)/device_active_definition.odl $(PKGDIR)/etc/amx/$(COMPONENT)/$(COMPONENT)/device_active_definition.odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)/query_definition.odl $(PKGDIR)/etc/amx/$(COMPONENT)/$(COMPONENT)/query_definition.odl
	$(INSTALL) -D -p -m 0755 odl/gmap-server.odl $(PKGDIR)/etc/amx/$(COMPONENT)/gmap-server.odl
	$(INSTALL) -D -p -m 0660 acl/admin/$(COMPONENT).json $(PKGDIR)$(ACLDIR)/admin/$(COMPONENT).json
	$(INSTALL) -D -p -m 0660 acl/operator/$(COMPONENT).json $(PKGDIR)$(ACLDIR)/operator/$(COMPONENT).json
	$(INSTALL) -d -m 0755 $(PKGDIR)$(BINDIR)
	rm -f $(PKGDIR)$(BINDIR)/$(COMPONENT)
	ln -sfr $(PKGDIR)$(BINDIR)/amxrt $(PKGDIR)$(BINDIR)/$(COMPONENT)
	$(INSTALL) -D -p -m 0644 output/$(MACHINE)/$(COMPONENT).so $(PKGDIR)/usr/lib/amx/$(COMPONENT)/$(COMPONENT).so
	$(INSTALL) -D -p -m 0755 scripts/$(COMPONENT).sh $(PKGDIR)$(INITDIR)/$(COMPONENT)
	cd $(PKGDIR) && $(TAR) -czvf ../$(COMPONENT)-$(VERSION).tar.gz .
	cp $(PKGDIR)../$(COMPONENT)-$(VERSION).tar.gz .
	make -C packages

changelog:
	$(call create_changelog)

doc:
	$(MAKE) -C doc doc

	$(eval ODLFILES += odl/defaults.d/gmap-server-defaults.odl)
	$(eval ODLFILES += odl/gmap-server-definitions.odl)
	$(eval ODLFILES += odl/$(COMPONENT)/config_definition.odl)
	$(eval ODLFILES += odl/$(COMPONENT)/device_definition.odl)
	$(eval ODLFILES += odl/$(COMPONENT)/device_active_definition.odl)
	$(eval ODLFILES += odl/$(COMPONENT)/query_definition.odl)
	$(eval ODLFILES += odl/gmap-server.odl)

	mkdir -p output/xml
	mkdir -p output/html
	mkdir -p output/confluence
	amxo-cg -Gxml,output/xml/$(COMPONENT).xml $(or $(ODLFILES), "")
	amxo-xml-to -x html -o output-dir=output/html -o title="$(COMPONENT)" -o version=$(VERSION) -o sub-title="Datamodel reference" output/xml/*.xml
	amxo-xml-to -x confluence -o output-dir=output/confluence -o title="$(COMPONENT)" -o version=$(VERSION) -o sub-title="Datamodel reference" output/xml/*.xml

test:
	$(MAKE) -C test run
	$(MAKE) -C test coverage

.PHONY: all clean changelog install package doc test
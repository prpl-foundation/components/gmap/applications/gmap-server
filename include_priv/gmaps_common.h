/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#if !defined(__GMAPS_COMMON_H__)
#define __GMAPS_COMMON_H__

#ifdef __cplusplus
extern "C"
{
#endif

// doxygen has problems with these attributes
#if !defined(USE_DOXYGEN)
#define GMAPS_PRIVATE __attribute__ ((visibility("hidden")))
#define GMAPS_CONSTRUCTOR __attribute__((constructor))
#define GMAPS_DESTRUCTOR __attribute__((destructor))
#define GMAPS_UNUSED __attribute__((unused))
#define GMAPS_INLINE static inline
#else
#define GMAPS_PRIVATE
#define GMAPS_CONSTRUCTOR
#define GMAPS_DESTRUCTOR
#define GMAPS_UNUSED
#define GMAPS_INLINE
#endif

// The following is required to have the non-standardized C lib functions available
// when compiling with --std=c18 (or any other ANSI standard), enforcing __STRICT_ANSI__.
#define _DEFAULT_SOURCE 1

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>

#include <amxp/amxp_signal.h>
#include <amxp/amxp_slot.h>
#include <amxp/amxp_expression.h>

#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_object_event.h>
#include <amxd/amxd_object_expression.h>
#include <amxd/amxd_transaction.h>
#include <amxd/amxd_path.h>

#include <amxb/amxb.h>

#include <amxo/amxo.h>
#include <amxo/amxo_mibs.h>
#include <amxo/amxo_save.h>

#include <gmap/gmap.h>
#include <gmap/gmap_device.h>
#include <gmap/gmap_devices_flags.h>

typedef struct _gmaps_function_data {
    char* action_name;
    amxd_object_t* device;
    amxc_var_t user_data;
    amxc_llist_t callids;
} gmaps_function_data_t;

typedef struct _gmap_device_priv_data {
    struct _gmaps_tpb_node* tpb_node;   /**< Topology builder node */
    amxc_set_t tags;                    /**< Set of tags for this device. */
    amxc_llist_it_t traversal;          /**< Iterator used during traversal and other short (within one function call) algorithms. */
    amxc_llist_it_t query;              /**< Iterator used by the query system. May remain in use over several event loop iterations. */
} gmap_device_priv_data_t;

void GMAPS_PRIVATE gmap_server_init(amxd_dm_t* dm,
                                    amxo_parser_t* parser);
void GMAPS_PRIVATE gmap_server_cleanup(void);

amxd_dm_t* GMAPS_PRIVATE gmap_get_dm(void);
amxo_parser_t* GMAPS_PRIVATE gmap_get_parser(void);
amxc_var_t* GMAPS_PRIVATE gmap_get_config(void);
amxc_htable_t* GMAPS_PRIVATE gmap_get_table(void);
amxc_htable_t* GMAPS_PRIVATE gmap_get_blocked_devices(void);
amxd_object_t* GMAPS_PRIVATE gmap_dm_get_devices(void);
amxd_object_t* GMAPS_PRIVATE gmap_dm_get_devices_device(void);
amxd_object_t* GMAPS_PRIVATE gmap_dm_get_devices_query(void);
amxd_object_t* GMAPS_PRIVATE gmap_dm_get_devices_config(void);
void GMAPS_PRIVATE gmaps_time_init(void);
void GMAPS_PRIVATE gmaps_time_cleanup(void);
bool GMAPS_PRIVATE gmap_is_ntp_synchronized(void);
bool GMAPS_PRIVATE gmaps_common_is_mac(const char* str);
/** @deprecated
 * Returns whether the given space-separated strig contains the given tag. */
bool GMAPS_PRIVATE gmaps_common_contains_tag(const ssv_string_t taglist, const char* tag);

gmap_device_priv_data_t* GMAPS_PRIVATE gmaps_device_private_data(const amxd_object_t* device);
amxd_object_t* GMAPS_PRIVATE gmaps_device_from_private_data(const gmap_device_priv_data_t* priv);

#ifdef __cplusplus
}
#endif

#endif // __GMAPS_COMMON_H__

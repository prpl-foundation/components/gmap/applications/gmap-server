/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(__GMAPS_DEVICE_TOPOLOGY_H__)
#define __GMAPS_DEVICE_TOPOLOGY_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <stdint.h>

#include "gmaps_common.h"
#include "gmaps_devices_traverse.h"

typedef struct _topology_data {
    amxc_llist_t stack;
    uint32_t flags;
} topology_data_t;

typedef struct _item {
    amxc_llist_it_t lit;
    amxc_var_t* data;
} item_t;

#ifdef __cplusplus
}
#endif

/**
 * @brief
 * Builds the device tree, starting from the specified device using a traverse mode.
 *
 * @note
 * The returned variant map needs to be deleted using amxc_var_delete()
 *
 * @param device a pointer to the device from where the tree will start
 * @param expression details for devices that are matching the expression are added to the end result
 * @param mode the traverse mode, must be any of @ref gmap_traverse_mode_t
 * @param flags topology flags See @ref GMAP_TOPOLOGY_NO_RECURSE, @ref GMAP_TOPOLOGY_NO_DETAILS

 * @return
 * return an amxc_var_t llist containing the topology
 */

amxc_var_t* gmaps_device_topology(
    amxd_object_t* device,
    const char* expression,
    gmap_traverse_mode_t mode,
    uint32_t flags,
    gmap_status_t* status);

/**
 * @brief
 * This is the callback function used by gmaps_traverse_tree to handle the different traverse cases.
 *
 * @param device A pointer to the device that is currently being handled in the traverse
 * @param action Which case the traverse needs to be handled
 * @param userdata Contains the flags and data of the already handled devices
 *
 * @return gmap_traverse_status_t
 */
gmap_traverse_status_t gmap_traverse_topology(
    amxd_object_t* device,
    gmap_traverse_action_t action,
    void* userdata);

/**
 * @brief
 * Adds the passed device to the stack of existing topology data. It also pushes it to the stack
 *
 * @param stack contains the data of the already handled devices. The first element is the last pushed device
 * @param data the parent device of the passed device
 * @param device A pointer to the device that needs to be added
 * @param flags topology flags See @ref GMAP_TOPOLOGY_NO_RECURSE, @ref GMAP_TOPOLOGY_NO_DETAILS
 */
void gmap_topology_add_device(
    amxc_llist_t* stack,
    amxc_var_t* data,
    amxd_object_t* device,
    uint32_t flags);

/**
 * @brief
 * pushes a new device to the @brief
 * Returns the data of the current top of the stack
 * @brief
 * Pushed a device on the stack. Now the device before this the top of the stack.
 */
void gmap_topology_push(amxc_llist_t* stack, amxc_var_t* device_table);

/**
 * @brief
 * pushes a new device to the @brief
 * Returns the data of the current top of the stack
 * @brief
 * Pops a device from the stack. Now the device before this one is the top of the stack.
 * A pop is used for when a device is the bottom of an hierarchical branch.
 */
amxc_var_t* gmap_topology_pop(amxc_llist_t* stack);

/**
 * @brief
 * Returns the data of the current top of the stack
 */
amxc_var_t* gmap_topology_current(amxc_llist_t* stack);

/**
 * Frees a stack item
 */
void free_stack_item(amxc_llist_it_t* it);

/**
 * @brief
 * Handles the passed children list.
 * If no random mac addresses should be included, or the list is empty.
 * The children list is removed.
 *
 * @param children a pointer to the list of children
 * @param flags topology flags See @ref GMAP_TOPOLOGY_INCLUDE_RANDOM_MAC
 */
void handle_children(amxc_var_t* children, uint32_t flags);

#endif // __GMAPS_DEVICE_TOPOLOGY_H__
/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <string.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "gmaps_priv.h"

#include <amxd/amxd_object_event.h>
#include <amxd/amxd_object_hierarchy.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_object_parameter.h>
#include <uuid/uuid.h>

// Some versions of uuid.h do not have this:
#ifndef UUID_STR_LEN
#define UUID_STR_LEN    37
#endif

#define ME "uuid"


/** Automatically generated device keys (i.e. device object names) start with this prefix. */
#define DEVICE_ID_PREFIX "ID-"

const char* gmaps_uuid_get_key_by_mac(const char* search_mac) {
    amxd_object_t* device_template = gmap_dm_get_devices_device();
    when_str_empty_trace(search_mac, exit, ERROR, "EMPTY");
    when_null_trace(device_template, exit, ERROR, "Unexpected object hierarchy");

    // Currently not much optimized
    amxd_object_for_each(instance, it, device_template) {
        amxd_object_t* dev_obj = amxc_llist_it_get_data(it, amxd_object_t, it);
        amxc_var_t candidate_mac_var;
        amxc_var_init(&candidate_mac_var);
        amxd_object_get_param(dev_obj, "PhysAddress", &candidate_mac_var);
        const char* candidate_mac = amxc_var_constcast(cstring_t, &candidate_mac_var);
        bool match = candidate_mac != NULL && 0 == strcasecmp(candidate_mac, search_mac);
        amxc_var_clean(&candidate_mac_var);
        if(!match) {
            continue;
        }
        bool iface = gmaps_device_tag_has(dev_obj, "interface");
        if(iface) {
            continue;
        }
        return amxd_object_get_name(dev_obj, AMXD_OBJECT_NAMED);
    }

exit:
    return NULL;
}

char* gmaps_uuid_generate_key(void) {
    size_t prefix_len = strlen(DEVICE_ID_PREFIX);
    uuid_t bin_uuid = {0};
    uuid_generate(bin_uuid);
    char* key = calloc(1, strlen(DEVICE_ID_PREFIX) + UUID_STR_LEN);
    strcpy(key, DEVICE_ID_PREFIX);
    uuid_unparse_lower(bin_uuid, key + prefix_len);
    return key;
}

gmap_status_t gmaps_uuid_new_device_or_get_key(const char* mac,
                                               const char* discovery_source,
                                               const ssv_string_t tags,
                                               bool persistent,
                                               const char* default_name,
                                               const amxc_var_t* parameters,
                                               char** target_key,
                                               bool* target_already_exists) {
    amxd_object_t* template_obj = gmap_dm_get_devices_device();
    char* new_key = NULL;
    const char* existing_key = NULL;
    amxc_var_t parameters_modified;
    amxc_var_t* mac_in_param = amxc_var_get_key(parameters, "PhysAddress", AMXC_VAR_FLAG_DEFAULT);
    when_null_trace(template_obj, invalid_parameter, ERROR, "NULL");
    when_str_empty_trace(mac, invalid_parameter, ERROR, "EMPTY");
    when_null_trace(target_key, invalid_parameter, ERROR, "NULL");
    when_null_trace(target_already_exists, invalid_parameter, ERROR, "NULL");

    when_false_trace(mac_in_param == NULL || amxc_var_type_of(mac_in_param) == AMXC_VAR_ID_CSTRING,
                     invalid_parameter, ERROR, "If PhysAddress is present in parameters, it must be a string");
    when_false_trace(mac_in_param == NULL || 0 == strcmp(amxc_var_constcast(cstring_t, mac_in_param), mac),
                     invalid_parameter, ERROR, "PhysAddress in parameters disagrees with given mac");
    when_false_trace(gmaps_common_is_mac(mac),
                     invalid_parameter, ERROR, "Given mac is not a mac: '%s'", mac);
    when_false_trace(gmaps_common_contains_tag(tags, "mac"),
                     invalid_parameter, ERROR, "Tags '%s' must contain \"mac\"", tags);

    existing_key = gmaps_uuid_get_key_by_mac(mac);
    if(existing_key != NULL) {
        *target_key = strdup(existing_key);
        *target_already_exists = true;
        return gmap_status_ok;
    }

    amxc_var_init(&parameters_modified);
    amxc_var_set_type(&parameters_modified, AMXC_VAR_ID_HTABLE);
    amxc_var_copy(&parameters_modified, parameters); // might do nothing (`parameters` can be NULL)
    amxc_var_add_key(cstring_t, &parameters_modified, "PhysAddress", mac);

    *target_already_exists = false;
    new_key = gmaps_uuid_generate_key();
    gmap_status_t status = gmaps_new_device(template_obj, new_key, discovery_source, tags, persistent, default_name, &parameters_modified);
    amxc_var_clean(&parameters_modified);
    if(status != gmap_status_ok) {
        free(new_key);
        *target_key = NULL;
    } else {
        *target_key = new_key;
    }

    return status;


invalid_parameter:
    return gmap_status_invalid_parameter;
}

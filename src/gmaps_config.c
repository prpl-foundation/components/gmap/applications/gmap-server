/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <debug/sahtrace.h>

#include "gmaps_priv.h"

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#define ME "config"

typedef enum {
    gmap_config_file_default,
    gmap_config_file_saved
} config_file_t;

GMAPS_INLINE amxc_string_t* gmaps_config_get_new_file_name(const char* module,
                                                           config_file_t type) {
    amxc_string_t* ret = NULL;
    amxc_string_new(&ret, 0);

    switch(type) {
    case gmap_config_file_default:
        amxc_string_append(ret, "${gmap_config_defaults}", 23);
        break;
    case gmap_config_file_saved:
        amxc_string_append(ret, "${gmap_config_save}", 19);
        break;
    }

    amxc_string_appendf(ret, "/gmap_conf_%s.odl", module);
    amxc_string_resolve(ret, gmap_get_config());

    return ret;
}

gmap_status_t gmaps_new_config(amxd_object_t* config_obj,
                               const char* module) {
    gmap_status_t status = gmap_status_unknown_error;
    amxd_object_t* module_obj = NULL;

    when_null_status(config_obj, exit, status = gmap_status_invalid_parameter);
    when_str_empty_status(module, exit, status = gmap_status_invalid_parameter);

    module_obj = amxd_object_get_child(config_obj, module);
    if(module_obj == NULL) {
        when_failed_status(amxd_object_new(&module_obj,
                                           amxd_object_singleton,
                                           module),
                           exit,
                           status = gmap_status_data_model_error);
        when_failed_status(amxd_object_add_object(config_obj,
                                                  module_obj),
                           exit,
                           status = gmap_status_data_model_error);
    }

    status = gmap_status_ok;

exit:
    return status;
}

gmap_status_t gmaps_set_config(amxd_object_t* config_obj,
                               const char* module,
                               const char* option,
                               amxc_var_t* value) {
    gmap_status_t status = gmap_status_unknown_error;
    amxd_object_t* module_obj = NULL;
    amxd_param_t* config_param = NULL;
    amxd_trans_t transaction;
    when_failed_status(amxd_trans_init(&transaction),
                       exit,
                       status = gmap_status_data_model_error);

    when_null_status(config_obj, exit, status = gmap_status_invalid_parameter);
    when_str_empty_status(module, exit, status = gmap_status_invalid_parameter);
    when_str_empty_status(option, exit, status = gmap_status_invalid_parameter);
    when_null_status(value, exit, status = gmap_status_invalid_parameter);

    module_obj = amxd_object_get_child(config_obj, module);
    if(module_obj == NULL) {
        when_failed_status(gmaps_new_config(config_obj, module),
                           exit,
                           status = gmap_status_internal_error);
        module_obj = amxd_object_get_child(config_obj, module);
        when_null_status(module_obj,
                         exit,
                         status = gmap_status_internal_error);
    }

    config_param = amxd_object_get_param_def(module_obj, option);
    if(config_param == NULL) {
        if((amxd_param_new(&config_param, option, amxc_var_type_of(value)) != amxd_status_ok) ||
           (amxd_param_set_attr(config_param, amxd_pattr_persistent, true) != amxd_status_ok) ||
           (amxd_object_add_param(module_obj, config_param) != amxd_status_ok)) {
            status = gmap_status_data_model_error;
            goto exit;
        }
    }

    // Backwards compatibility: use transaction here to send out notification.
    amxd_trans_select_object(&transaction, module_obj);
    amxd_trans_set_param(&transaction, option, value);
    when_failed_status(amxd_trans_apply(&transaction, gmap_get_dm()),
                       exit,
                       status = gmap_status_data_model_error);

    status = gmap_status_ok;

exit:
    amxd_trans_clean(&transaction);
    return status;
}

gmap_status_t gmaps_get_config(amxd_object_t* config_obj,
                               const char* module,
                               const char* option,
                               amxc_var_t* ret) {
    gmap_status_t status = gmap_status_unknown_error;
    amxd_object_t* module_obj = NULL;
    amxd_param_t* config_param = NULL;
    when_null_status(config_obj, exit, status = gmap_status_invalid_parameter);
    when_str_empty_status(module, exit, status = gmap_status_invalid_parameter);
    when_str_empty_status(option, exit, status = gmap_status_invalid_parameter);
    when_null_status(ret, exit, status = gmap_status_invalid_parameter);

    module_obj = amxd_object_get_child(config_obj, module);
    when_null_status(module_obj,
                     exit,
                     status = gmap_status_module_not_found);

    config_param = amxd_object_get_param_def(module_obj, option);
    when_null_status(config_param,
                     exit,
                     status = gmap_status_option_not_found);

    when_failed_status(amxd_param_get_value(config_param, ret),
                       exit,
                       status = gmap_status_data_model_error);

    status = gmap_status_ok;

exit:
    return status;
}

gmap_status_t gmaps_save_config(amxd_object_t* config_obj,
                                const char* module) {
    gmap_status_t status = gmap_status_unknown_error;
    amxc_string_t* file_name = NULL;
    amxd_object_t* module_obj = NULL;
    amxc_var_t* config_options = NULL;
    amxc_var_t* populate_behavior = NULL;

    when_null_status(config_obj, exit, status = gmap_status_invalid_parameter);
    when_str_empty_status(module, exit, status = gmap_status_invalid_parameter);

    amxc_var_new(&config_options);

    module_obj = amxd_object_get_child(config_obj, module);
    when_null_status(module_obj,
                     exit,
                     status = gmap_status_module_not_found);

    file_name = gmaps_config_get_new_file_name(module, gmap_config_file_saved);

    // Set populate behaviour config options and save.
    amxc_var_set_type(config_options, AMXC_VAR_ID_HTABLE);
    populate_behavior = amxc_var_add_key(amxc_htable_t,
                                         config_options,
                                         "populate_behavior",
                                         NULL);
    amxc_var_add_key(cstring_t,
                     populate_behavior,
                     "unknown_parameter",
                     "add");
    when_failed_status(amxo_parser_save(gmap_get_parser(),
                                        amxc_string_get(file_name, 0),
                                        module_obj,
                                        UINT32_MAX,
                                        config_options,
                                        false),
                       exit,
                       status = gmap_status_file_not_available);

    status = gmap_status_ok;

exit:
    amxc_string_delete(&file_name);
    amxc_var_delete(&config_options);

    return status;
}

gmap_status_t gmaps_load_config(GMAPS_UNUSED amxd_object_t* config_obj,
                                const char* module) {
    gmap_status_t status = gmap_status_unknown_error;
    amxc_string_t* file_name = NULL;
    struct stat sb;

    when_str_empty_status(module, exit, status = gmap_status_invalid_parameter);

    file_name = gmaps_config_get_new_file_name(module, gmap_config_file_saved);

    if(stat(amxc_string_get(file_name, 0), &sb) == -1) {
        amxc_string_delete(&file_name);
        file_name = gmaps_config_get_new_file_name(module, gmap_config_file_default);
    }
    if(stat(amxc_string_get(file_name, 0), &sb) == -1) {
        status = gmap_status_file_not_available;
        goto exit;
    }
    when_failed_status(amxo_parser_parse_file(gmap_get_parser(),
                                              amxc_string_get(file_name, 0),
                                              amxd_dm_get_root(gmap_get_dm())),
                       exit,
                       status = gmap_status_odl_error);

    status = gmap_status_ok;
    SAH_TRACEZ_INFO(ME, "config %s loaded", module);

exit:
    if(status != gmap_status_ok) {
        SAH_TRACEZ_ERROR(ME, "config %s not loaded, status code: %d", module, status);
    }
    amxc_string_delete(&file_name);
    return status;
}

/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/


#include <stdlib.h>
#include <string.h>

#include "gmaps_priv.h"
#include "debug/sahtrace.h"
#include <debug/sahtrace_macros.h>

/**
 * Get the string version of the argument type
 */
static const char* gmaps_device_get_argument_typename(amxd_func_arg_t* arg) {
    return arg != NULL ? amxc_var_get_type_name_from_id(arg->type) : NULL;
}

/**
 * Fill htable with argument information
 */
static void gmaps_device_add_arg(amxd_func_arg_t* arg, amxc_var_t* data) {
    amxc_var_add_key(cstring_t, data, "Name", arg->name);
    amxc_var_add_key(cstring_t, data, "Type", gmaps_device_get_argument_typename(arg));
    amxc_var_add_key(bool, data, "Mandatory", arg->attr.mandatory ? true : false);
}

/**
 * Fill htable with action information
 */
static void gmaps_device_add_action(amxd_function_t* func, amxc_var_t* data) {
    amxc_var_t* args;
    gmaps_function_data_t* func_data = (gmaps_function_data_t*) func->priv;

    amxc_var_add_key(cstring_t, data, "Function", func->name);
    amxc_var_add_key(cstring_t, data, "Name", func_data->action_name);
    args = amxc_var_add_key(amxc_llist_t, data, "Arguments", NULL);

    amxc_llist_for_each(it, &func->args) {
        gmaps_device_add_arg(amxc_llist_it_get_data(it, amxd_func_arg_t, it), args);
    }
}

/**
 * Fill the parameters htable with a list of action under the "Actions" Key
 */
static void gmaps_device_get_actions(amxd_object_t* object, amxc_var_t* parameters) {

    amxc_var_t actions;
    amxc_var_init(&actions);

    amxc_var_set_type(&actions, AMXC_VAR_ID_LIST);

    amxd_object_for_each(function, it, object) {
        amxd_function_t* func = amxc_llist_it_get_data(it, amxd_function_t, it);
        gmaps_function_data_t* func_data = (gmaps_function_data_t*) func->priv;

        if(func_data && func_data->action_name && (func_data->action_name[0] != '\0')) {
            amxc_var_t* action_data = amxc_var_add_new(&actions);
            amxc_var_set_type(action_data, AMXC_VAR_ID_HTABLE);

            gmaps_device_add_action(func, action_data);
        }
    }

    if(!amxc_llist_is_empty(amxc_var_constcast(amxc_llist_t, &actions))) {
        amxc_var_add_key(amxc_llist_t, parameters, "Actions",
                         amxc_var_constcast(amxc_llist_t, &actions));
    }

    amxc_var_clean(&actions);
}

/**
 *  Function calledd for an alternate device
 */
static uint32_t gmaps_device_get_alternatives_it(amxd_object_t* device, void* priv) {
    amxc_var_t* alt_list = (amxc_var_t*) priv;
    amxc_var_t parameters;
    amxc_var_init(&parameters);

    gmaps_device_get_recursive(device, &parameters, true, 0);

    amxc_var_add(amxc_htable_t, alt_list, amxc_var_constcast(amxc_htable_t, &parameters));

    amxc_var_clean(&parameters);

    return gmap_traverse_continue;
}

/**
 * Include the alternatives in the parameters var
 */
static void gmaps_device_get_alternatives(amxd_object_t* object, amxc_var_t* parameters) {

    amxc_var_t alternatives;   /*< List of alternatives */
    amxc_var_init(&alternatives);

    gmaps_device_for_all_alternatives(object,
                                      gmaps_device_get_alternatives_it,
                                      (void*) &alternatives);

    amxc_var_add_key(amxc_llist_t, parameters, "Alternative",
                     amxc_var_constcast(amxc_llist_t, &alternatives));

    amxc_var_clean(&alternatives);
}


/**
 * Function will add the link information
 */
static void gmaps_device_add_links(amxd_object_t* object,
                                   amxc_var_t* parameters,
                                   const char* dev,
                                   const char* link,
                                   uint32_t flags) {
    amxc_var_t links;
    amxd_object_t* link_template = NULL;

    amxc_var_init(&links);
    amxc_var_set_type(&links, AMXC_VAR_ID_LIST);

    if(strcmp(dev, "UDevice") == 0) {
        link_template = amxd_object_get_child(object, "UDevice");
    } else {
        link_template = amxd_object_get_child(object, "LDevice");
    }

    amxd_object_for_each(instance, link_it, link_template) {
        amxd_object_t* linkdev = amxc_llist_it_get_data(link_it, amxd_object_t, it);
        amxc_var_t* objectinfo = amxc_var_add_new(&links);

        amxc_var_set_type(objectinfo, AMXC_VAR_ID_HTABLE);

        if(flags & GMAP_INCLUDE_FULL_LINKS) {
            gmaps_device_get_recursive(linkdev, objectinfo, false, 0);
        } else {
            amxc_var_set_type(objectinfo, AMXC_VAR_ID_CSTRING);
            amxc_var_set_cstring_t(objectinfo, linkdev->name);
        }
    }

    if(!amxc_llist_is_empty(amxc_var_constcast(amxc_llist_t, &links))) {
        amxc_var_add_key(amxc_llist_t, parameters, link,
                         amxc_var_constcast(amxc_llist_t, &links));
    }

    amxc_var_clean(&links);
}

/**
 * Function will add the information for the child to the parameters var
 */
static void gmaps_device_get_childeren(amxd_object_t* object, amxc_var_t* parameters) {
    amxc_var_t* children;
    amxc_var_new(&children);
    amxc_var_set_type(children, AMXC_VAR_ID_LIST);

    amxd_object_t* alternatives_obj = amxd_object_get_child(object, "Alternative");

    amxd_object_for_each(instance, child_it, alternatives_obj) {
        amxd_object_t* childdev_pointer_obj = amxc_llist_it_get_data(child_it, amxd_object_t, it);
        const char* childdev_alias = amxd_object_get_name(childdev_pointer_obj, AMXD_OBJECT_NAMED);
        amxd_object_t* childdev_obj = gmaps_get_device(childdev_alias);

        amxc_var_t* objectinfo = amxc_var_add_new(children);
        amxc_var_set_type(objectinfo, AMXC_VAR_ID_HTABLE);
        gmaps_device_get_recursive(childdev_obj, objectinfo, false, 0);

    }
    if(!amxc_llist_is_empty(amxc_var_constcast(amxc_llist_t, children))) {
        amxc_var_set_key(parameters, "Childs", children, AMXC_VAR_FLAG_DEFAULT);
    } else {
        amxc_var_delete(&children);
    }
}

/**
 * Main function to get the object details
 */
void gmaps_device_get_recursive(amxd_object_t* object,
                                amxc_var_t* parameters,
                                bool recurse,
                                uint32_t flags) {
    /* object is NOT a template object*/
    if(object->type != amxd_object_template) {
        /* include details*/
        if(!(flags & GMAP_NO_DETAILS)) {
            amxd_object_get_params(object, parameters, amxd_dm_access_protected);
            amxc_var_add_key(cstring_t, parameters, "Id", amxc_var_constcast(cstring_t, amxc_var_get_key(parameters, "Key", 0)));
        }

        /* include actions, if needed */
        if(!(flags & GMAP_NO_ACTIONS)) {
            gmaps_device_get_actions(object, parameters);
        }

        if(flags & GMAP_INCLUDE_ALTERNATIVES) {
            gmaps_device_get_alternatives(object, parameters);
        }

        if(flags & (GMAP_INCLUDE_LINKS | GMAP_INCLUDE_FULL_LINKS)) {
            gmaps_device_add_links(object, parameters, "LDevice", "LLinks", flags);
            gmaps_device_add_links(object, parameters, "UDevice", "ULinks", flags);
        }

        /* need to recurse? */
        if(!recurse || (flags & GMAP_NO_DETAILS)) {
            /* no recurse, leave */
            goto exit;
        }

        amxd_object_for_each(child, it, object) {
            amxd_object_t* child_obj = amxc_container_of(it, amxd_object_t, it);
            //if template add list otherwise add htable
            if(child_obj->type == amxd_object_template) {
                //skip UDevice, LDevice and alternative
                if(strcmp(amxd_object_get_name(child_obj, AMXD_OBJECT_NAMED), "Alternative") == 0) { // implement later
                    continue;
                }
                gmaps_device_get_recursive(child_obj, parameters, recurse, flags);
            } else {
                amxc_var_t* childparams = amxc_var_add_key(amxc_htable_t, parameters, amxd_object_get_name(child_obj, AMXD_OBJECT_NAMED), NULL);
                gmaps_device_get_recursive(child_obj, childparams, recurse, flags);
            }
        }
    } else { //is template
        /* need to recurse? */
        if(!recurse || (flags & GMAP_NO_DETAILS)) {
            /* no recurse, leave */
            goto exit;
        }

        amxc_var_t* template_var = amxc_var_add_key(amxc_llist_t, parameters, amxd_object_get_name(object, AMXD_OBJECT_NAMED), NULL);

        amxd_object_for_each(instance, it, object) {
            amxd_object_t* child_obj = amxc_container_of(it, amxd_object_t, it);
            amxc_var_t* child_params = amxc_var_add(amxc_htable_t, template_var, NULL);
            gmaps_device_get_recursive(child_obj, child_params, recurse, flags);
        }
    }

    gmaps_device_get_childeren(object, parameters);

exit:

    return;

}

/**
 * Get a htable with the description of object
 */
gmap_status_t gmaps_device_get(const char* key,
                               amxc_var_t* values,
                               uint32_t flags) {
    /* TODO_ACL TODO: add ACL control when available in Ambiorix. */
    gmap_status_t retval = gmap_status_unknown_error;
    amxd_object_t* device = NULL;

    when_str_empty_status(key, exit, retval = gmap_status_invalid_key);

    device = gmaps_get_device(key);

    when_null_status(device, exit, retval = gmap_status_device_not_found);

    /* Make sure we are working with a hashtable */
    amxc_var_set_type(values, AMXC_VAR_ID_HTABLE);

    if(!( flags & GMAP_INCLUDE_ALTERNATIVES)) {
        gmaps_device_get_master(&device);
    }

    gmaps_device_get_recursive(device, values, true, flags);

    retval = gmap_status_ok;

exit:
    return retval;
}

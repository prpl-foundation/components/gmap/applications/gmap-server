/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "gmaps_priv.h"

#define ME "query"

/** After a device changed, how much time in miliseconds to wait to re-evaluate query expressions */
#define QUERY_REEVALUATION_WAIT_MS 100

/* no compatibility needed with sop gmap regarding events
   Only one event is used for query notifications: gmap_query
 */
/*
   code written in order to be able to support client and server open queries
   In case server queries will be used we need also to move this code to a library
 */

typedef struct _gmaps_query_data {
    amxc_llist_t subscribers;      /**< List of gmap_query_t structures */
    uint32_t nextId;               /**< Next available unique ID */
    char* expression;              /**< Expression of the query */
    amxp_expr_t* expression_parsed;
    char* name;                    /**< Name of the query */
    amxd_object_t* obj;            /**< query object */
    amxc_llist_t matching_devices; /**< List of all matching devices, gmaps_query_device_entry_t */
} gmaps_query_data_t;

typedef struct _gmaps_query_device_entry {
    amxc_llist_it_t it;            /**< Iterator in a list of devices */
    amxd_object_t* obj;            /**< Device object */
} gmaps_query_device_entry_t;

static void gmaps_query_device_entry_free(amxc_llist_it_t* it) {
    when_null(it, exit);

    free(amxc_llist_it_get_data(it, gmaps_query_device_entry_t, it));

exit:
    return;
}

static struct {
    /* Items stored in the list are the query llist iterator from the device private data. */
    amxc_llist_t verify_entries;
    amxp_timer_t* timer;
} verify_queue;

static void s_query_verify_all_timer_cb(amxp_timer_t* timer, void* priv);

amxd_object_t* gmaps_query_object(gmap_query_t* query) {
    gmaps_query_data_t* query_data = amxc_container_of(query->it.llist, gmaps_query_data_t, subscribers);
    return query_data->obj;
}

void gmaps_query_notify_client(gmap_query_t* query,
                               const char* key,
                               amxc_var_t* device,
                               gmap_query_action_t action) {
    amxc_var_t sig_data;
    amxc_var_t* data = NULL;
    amxc_var_t* device_data = NULL;

    when_null(query, exit);
    when_str_empty(key, exit);

    amxc_var_init(&sig_data);
    amxc_var_set_type(&sig_data, AMXC_VAR_ID_HTABLE);
    data = amxc_var_add_new_key(&sig_data, "Result");
    amxc_var_set_type(data, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(uint32_t, data, "Id", query->id);
    amxc_var_add_key(uint32_t, data, "Index", query->index);
    amxc_var_add_key(uint32_t, data, "Action", action);
    amxc_var_add_key(cstring_t, data, "Key", key);

    if(action != gmap_query_expression_stop_matching) {
        if(device) {
            device_data = amxc_var_add_new_key(data, "Device");
            amxc_var_copy(device_data, device);
        } else {
            SAH_TRACEZ_ERROR(ME, "device is NULL pointer");
        }
    }
    amxd_object_emit_signal(gmaps_query_object(query), "gmap_query", &sig_data);
    amxc_var_clean(&sig_data);
exit:
    return;
}

static amxc_llist_it_t* gmaps_query_get_matching_device(amxc_llist_t* matching_devices, const amxd_object_t* dev) {
    when_null(matching_devices, exit);
    when_null(dev, exit);

    SAH_TRACEZ_INFO(ME, "get matching device");
    amxc_llist_for_each(it, matching_devices) {
        gmaps_query_device_entry_t* entry = amxc_llist_it_get_data(it, gmaps_query_device_entry_t, it);
        if(entry->obj == dev) {
            return &entry->it;
        }
    }
exit:
    return NULL;
}

static gmaps_query_device_entry_t* gmaps_query_add_matching_device(amxc_llist_t* matching_devices, amxd_object_t* dev) {
    gmaps_query_device_entry_t* entry = NULL;
    when_null(matching_devices, exit);
    when_null(dev, exit);

    entry = calloc(1, sizeof(gmaps_query_device_entry_t));
    when_null(entry, exit);
    entry->obj = dev;
    amxc_llist_it_init(&entry->it);
    amxc_llist_append(matching_devices, &entry->it);
exit:
    return entry;
}

static void gmaps_query_execute_all(gmaps_query_data_t* query_data, const char* key, amxc_var_t* device, gmap_query_action_t action) {
    gmap_query_t* gmap_query = NULL;

    when_null(query_data, exit);
    when_str_empty(key, exit);

    amxc_llist_for_each(it, &query_data->subscribers) {
        gmap_query = amxc_container_of(it, gmap_query_t, it);
        gmap_query->fn(gmap_query, key, device, action);
    }
exit:
    return;
}

static void gmaps_query_update_all(amxd_object_t* device, gmaps_query_data_t* query_data, const char* key, amxc_var_t* device_data) {
    gmap_query_t* gmap_query = NULL;

    when_null(query_data, exit);
    when_str_empty(key, exit);

    amxc_llist_for_each(it, &query_data->subscribers) {
        gmap_query = amxc_container_of(it, gmap_query_t, it);
        if((gmap_query->flags & GMAP_QUERY_IGNORE_DEVICE_UPDATED) != 0) {
            continue;
        }
        if((device_data != NULL) && !amxc_var_get_first(device_data)) {
            gmaps_device_get_recursive(device, device_data, true, 0);
        }
        gmap_query->fn(gmap_query, key, device_data, gmap_query_device_updated);
    }
exit:
    return;
}

void gmaps_query_remove_all(amxd_object_t* device) {
    amxd_object_t* query_template_obj = NULL;
    const char* key;
    when_null(device, exit);

    key = amxd_object_get_name(device, AMXD_OBJECT_NAMED);

    SAH_TRACEZ_INFO(ME, "remove all called for device %s", key);
    query_template_obj = gmap_dm_get_devices_query();
    when_null(query_template_obj, exit);

    amxd_object_for_each(instance, it, query_template_obj) {
        amxd_object_t* queryc = amxc_container_of(it, amxd_object_t, it);
        gmaps_query_data_t* query_data = queryc->priv;
        if(!query_data) {
            continue;
        }
        SAH_TRACEZ_INFO(ME, "query name: %s expression: %s", query_data->name, query_data->expression);
        amxc_llist_it_t* matching_device = gmaps_query_get_matching_device(&query_data->matching_devices, device);
        if(matching_device != NULL) {
            SAH_TRACEZ_INFO(ME, "remove matching device");
            amxc_llist_it_clean(matching_device, gmaps_query_device_entry_free);
            gmaps_query_execute_all(query_data, key, NULL, gmap_query_expression_stop_matching);
        }
    }
exit:
    return;
}

static void s_query_verify_all(amxd_object_t* device) {
    amxd_object_t* query_template_obj = NULL;
    amxc_var_t values;
    amxc_var_init(&values);
    amxc_var_set_type(&values, AMXC_VAR_ID_HTABLE);

    when_null(device, exit);

    const char* key = amxd_object_get_name(device, AMXD_OBJECT_NAMED);

    SAH_TRACEZ_INFO(ME, "verify all called for device %s", key);

    query_template_obj = gmap_dm_get_devices_query();
    when_null(query_template_obj, exit);
    amxd_object_for_each(instance, it, query_template_obj) {
        amxd_object_t* queryc = amxc_container_of(it, amxd_object_t, it);
        if(!queryc) {
            continue;
        }
        gmaps_query_data_t* query_data = queryc->priv;
        if(!query_data) {
            continue;
        }
        SAH_TRACEZ_INFO(ME, "check query name: %s expression: %s", query_data->name, query_data->expression);
        amxp_expr_t* expr = query_data->expression_parsed;
        when_null_trace(expr, exit, ERROR, "NULL expression");
        amxc_llist_it_t* matching_device = gmaps_query_get_matching_device(&query_data->matching_devices, device);
        if(matching_device) {
            SAH_TRACEZ_INFO(ME, "already matching");
            if(gmaps_device_matches(device, expr)) {
                SAH_TRACEZ_INFO(ME, "already matched, still match");
                gmaps_query_update_all(device, query_data, key, &values);
            } else {
                SAH_TRACEZ_INFO(ME, "do not match anymore");
                amxc_llist_it_clean(matching_device, gmaps_query_device_entry_free);
                gmaps_query_execute_all(query_data, key, NULL, gmap_query_expression_stop_matching);
            }
        } else {
            SAH_TRACEZ_INFO(ME, "not yet matching");
            if(gmaps_device_matches(device, expr)) {
                SAH_TRACEZ_INFO(ME, "new match");
                gmaps_query_add_matching_device(&query_data->matching_devices, device);
                if(!amxc_var_get_first(&values)) {
                    gmaps_device_get_recursive(device, &values, true, 0);
                }
                gmaps_query_execute_all(query_data, key, &values, gmap_query_expression_start_matching);
            }
        }
    }
exit:
    amxc_var_clean(&values);
    return;
}

static void s_process_verify_queue(void) {
    /* Processing a device should be a read-only operation that doesn't change
     * that device's data model. Processing should therefore not lead to starvation. */
    while(!amxc_llist_is_empty(&verify_queue.verify_entries)) {
        amxc_llist_it_t* it = amxc_llist_take_first(&verify_queue.verify_entries);
        gmap_device_priv_data_t* priv = NULL;
        amxd_object_t* dev = NULL;

        if(it == NULL) {
            break;
        }
        priv = amxc_llist_it_get_data(it, gmap_device_priv_data_t, query);
        dev = gmaps_device_from_private_data(priv);

        if(dev == NULL) {
            continue;
        }

        s_query_verify_all(dev);
    }
}

/**
 * Checks for all devices queued with @ref gmaps_query_verify_all if queries stop/start matching
 * (or are just updated).
 * @implements amxp_timer_cb_t
 */
static void s_query_verify_all_timer_cb(GMAPS_UNUSED amxp_timer_t* timer, GMAPS_UNUSED void* priv) {
    s_process_verify_queue();
}


/**
 * Queue a device to be checked if queries stop/start matching for the device (or is just updated).
 */
void gmaps_query_verify_all(amxd_object_t* device) {
    gmap_device_priv_data_t* priv = gmaps_device_private_data(device);
    when_null(priv, exit);

    if(priv->query.llist != &verify_queue.verify_entries) {
        amxc_llist_append(&verify_queue.verify_entries, &priv->query);
    }

    if((amxp_timer_get_state(verify_queue.timer) != amxp_timer_running)
       && (amxp_timer_get_state(verify_queue.timer) != amxp_timer_started)) {
        amxp_timer_start(verify_queue.timer, QUERY_REEVALUATION_WAIT_MS);
    }

exit:
    return;
}

/**
 *
 * @param result: of type htable, with key the device key and value the device fields.
 */
gmap_status_t gmaps_query_get_match_devices_with_fields(amxd_object_t* query_obj, amxc_var_t* result) {
    gmaps_query_data_t* query_data = NULL;

    query_data = (gmaps_query_data_t*) query_obj->priv;
    when_null(query_data, exit);
    amxc_var_set_type(result, AMXC_VAR_ID_LIST);
    amxc_llist_for_each(it, &query_data->matching_devices) {
        gmaps_query_device_entry_t* entry = amxc_llist_it_get_data(it, gmaps_query_device_entry_t, it);
        const char* dev_key = amxd_object_get_name(entry->obj, AMXD_OBJECT_NAMED);
        amxc_var_t* device_fields = amxc_var_add_new(result);
        amxc_var_set_type(device_fields, AMXC_VAR_ID_HTABLE);
        gmaps_device_get(dev_key, device_fields, 0);
    }
exit:
    return gmap_status_ok;
}

/**
 *
 * @param result: of type list of type string, each element the key of a device that matches the query.
 */
gmap_status_t gmaps_query_get_match_devices_without_fields(amxd_object_t* query_obj, amxc_var_t* result) {
    gmap_status_t status = gmap_status_ok;
    when_null(query_obj, exit);
    when_null(result, exit);

    gmaps_query_data_t* query_data = NULL;
    when_null_trace(query_obj, exit, ERROR, "query obj is NULL pointer");
    s_process_verify_queue();

    query_data = (gmaps_query_data_t*) query_obj->priv;
    when_null(query_data, exit);
    amxc_var_set_type(result, AMXC_VAR_ID_LIST);
    amxc_llist_for_each(it, &query_data->matching_devices) {
        gmaps_query_device_entry_t* entry = amxc_llist_it_get_data(it, gmaps_query_device_entry_t, it);
        const char* dev_key = amxd_object_get_name(entry->obj, AMXD_OBJECT_NAMED);
        amxc_var_add(cstring_t, result, dev_key);
    }
exit:
    return status;
}

static void gmaps_query_add_matching_devices(amxd_object_t* query_obj) {
    gmaps_query_data_t* query_data = NULL;
    amxd_object_t* device_template = NULL;
    amxp_expr_t* expr = NULL;
    when_null_trace(query_obj, exit, ERROR, "NULL argument");
    device_template = gmap_dm_get_devices_device();
    query_data = (gmaps_query_data_t*) query_obj->priv;
    when_null_trace(query_data, exit, ERROR, "NULL query data");
    expr = query_data->expression_parsed;
    when_null_trace(expr, exit, ERROR, "NULL expression");

    amxd_object_for_each(instance, it, device_template) {
        amxd_object_t* device = amxc_llist_it_get_data(it, amxd_object_t, it);
        if(gmaps_device_matches(device, expr)) {
            gmaps_query_device_entry_t* entry = calloc(1, sizeof(gmaps_query_device_entry_t));
            when_null_trace(entry, exit, ERROR, "Out of memory");
            entry->obj = device;
            amxc_llist_it_init(&entry->it);
            SAH_TRACEZ_INFO(ME, "added device to matching list: %s", amxd_object_get_name(device, AMXD_OBJECT_NAMED));
            amxc_llist_append(&query_data->matching_devices, &entry->it);
        }
    }

exit:
    return;
}

static gmap_status_t gmaps_query_destroy(amxd_object_t** query_obj) {
    when_null(query_obj, exit);
    SAH_TRACEZ_INFO(ME, "free query object");
    amxd_object_delete(query_obj);
exit:
    return gmap_status_ok;
}

static void gmaps_query_subscribers_list_it_free(amxc_llist_it_t* const it) {
    when_null(it, exit);
    SAH_TRACEZ_INFO(ME, "free query subscribers_it");
    gmap_query_t* gmap_query = amxc_container_of(it, gmap_query_t, it);
    free(gmap_query);
exit:
    return;
}

static void gmaps_query_free_query_data(gmaps_query_data_t* query_data) {
    when_null(query_data, exit);
    SAH_TRACEZ_INFO(ME, "free query data");
    if(query_data) {
        free(query_data->name);
        free(query_data->expression);
        amxp_expr_delete(&query_data->expression_parsed);
        amxc_llist_clean(&query_data->subscribers, gmaps_query_subscribers_list_it_free);
        amxc_llist_clean(&query_data->matching_devices, gmaps_query_device_entry_free);
    }
    free(query_data);
exit:
    return;
}
static gmap_status_t gmaps_query_create(const char* expression, const char* name, gmap_query_flags_t flags, amxd_object_t** query_obj) {

    amxd_status_t status = amxd_status_ok;
    amxd_object_t* queries = NULL;
    gmaps_query_data_t* query_data = NULL;
    amxc_string_t* flags_str = NULL;
    amxc_var_t* flags_var = NULL;

    amxc_var_t values;
    amxc_var_init(&values);
    amxc_var_set_type(&values, AMXC_VAR_ID_HTABLE);

    when_null(expression, error);
    when_null(name, error);

    SAH_TRACEZ_INFO(ME, "query create expression: %s", expression);

    query_data = calloc(1, sizeof(gmaps_query_data_t));
    when_null_trace(query_data, error, ERROR, "memory allocation failure");
    query_data->name = strdup(name);
    when_null_trace(query_data->name, error, ERROR, "memory allocation failure");
    query_data->expression = strdup(expression);
    when_null_trace(query_data->expression, error, ERROR, "memory allocation failure");
    when_failed_trace(amxp_expr_new(&query_data->expression_parsed, query_data->expression), error, ERROR, "Cannot parse expression");
    amxc_var_add_key(cstring_t, &values, "Expression", expression);
    amxc_var_add_key(cstring_t, &values, "Name", name);
    flags_str = gmap_query_flags_string(flags);
    flags_var = amxc_var_add_new_key(&values, "Flags");
    amxc_var_push(amxc_string_t, flags_var, flags_str);
    amxc_llist_init(&query_data->subscribers);
    amxc_llist_init(&query_data->matching_devices);
    queries = gmap_dm_get_devices_query();
    when_null(queries, error);
    status = amxd_object_add_instance(query_obj, queries, name, 0, &values);
    when_failed_trace(status, error, ERROR, "can not add query object instance");
    amxc_var_clean(&values);
    (*query_obj)->priv = query_data;
    query_data->obj = *query_obj;

    amxc_string_delete(&flags_str);
    return gmap_status_ok;

error:
    gmaps_query_free_query_data(query_data);
    amxc_var_clean(&values);
    amxc_string_delete(&flags_str);
    return gmap_status_unknown_error;
}

static gmap_status_t gmaps_query_find_obj(amxd_object_t** query_obj,
                                          const char* expression) {

    amxd_object_t* query_template_obj = NULL;
    gmap_status_t status = gmap_status_unknown_error;
    when_null(expression, exit);
    query_template_obj = gmap_dm_get_devices_query();
    when_null(query_template_obj, exit);
    *query_obj = NULL;
    amxd_object_for_each(instance, it, query_template_obj) {
        amxd_object_t* queryc = amxc_container_of(it, amxd_object_t, it);
        const amxc_var_t* expr = amxd_object_get_param_value(queryc, "Expression");
        if(!strcmp(amxc_var_constcast(cstring_t, expr), expression)) {
            *query_obj = queryc;
            break;
        }
    }
    status = gmap_status_ok;
exit:
    return status;
}

static gmap_query_t* gmaps_query_find_function(gmaps_query_data_t* query_data, gmap_query_cb_t fn) {
    gmap_query_t* gmap_query = NULL;
    when_null(query_data, exit);
    amxc_llist_for_each(it, &query_data->subscribers) {
        gmap_query = amxc_container_of(it, gmap_query_t, it);
        if(gmap_query->fn == fn) {
            /* the function was already registered, so stop looping */
            break;
        }
        gmap_query = NULL;
    }
exit:
    return gmap_query;
}

static void gmaps_query_open_internal(amxd_object_t* query_obj, gmap_query_t* gmap_query, gmap_query_flags_t flags, gmap_query_cb_t fn, void* user_data) {
    when_null(query_obj, exit);
    when_null(gmap_query, exit);

    gmaps_query_data_t* query_data = (gmaps_query_data_t*) query_obj->priv;
    query_data->nextId++;

    gmap_query->fn = fn;
    gmap_query->data = user_data;
    gmap_query->id = query_data->nextId;
    gmap_query->index = amxd_object_get_index(query_obj);
    gmap_query->refcount = 1;
    gmap_query->flags = flags;

    amxc_llist_append(&query_data->subscribers, &gmap_query->it);
exit:
    return;
}

gmap_status_t gmaps_query_open(const char* expression, const char* name, gmap_query_flags_t flags, gmap_query_cb_t fn, void* user_data, gmap_query_t** gmap_query) {

    gmap_status_t status = gmap_status_unknown_error;
    amxd_object_t* query_obj = NULL;
    gmaps_query_data_t* query_data = NULL;
    *gmap_query = NULL;

    when_str_empty_trace(expression, error, ERROR, "expression is empty string");
    when_str_empty_trace(name, error, ERROR, "name is empty string");
    when_null_trace(fn, error, ERROR, "call back is NULL pointer");

    /* find query object with same expression */
    status = gmaps_query_find_obj(&query_obj,
                                  expression);
    when_failed_trace(status, error, ERROR, "query find obj error");

    if(!query_obj) {
        *gmap_query = calloc(1, sizeof(gmap_query_t));
        when_null_trace(*gmap_query, error, ERROR, "memory allocation failure");
        status = gmaps_query_create(expression, name, flags, &query_obj);
        when_failed_trace(status, error, ERROR, "gmap query create error");
        gmaps_query_add_matching_devices(query_obj);
    } else {
        query_data = (gmaps_query_data_t*) query_obj->priv;
        *gmap_query = gmaps_query_find_function(query_data, fn);
        if(*gmap_query) {
            (*gmap_query)->data = user_data;
            (*gmap_query)->refcount++;
            (*gmap_query)->flags = flags;
            status = gmap_status_ok;
            return status;
        }
    }
    gmaps_query_open_internal(query_obj, *gmap_query, flags, fn, user_data);
    status = gmap_status_ok;
    return status;
error:
    free(*gmap_query);
    return status;
}

const char* gmaps_query_name(gmap_query_t* query) {
    when_null(query, exit);
    gmaps_query_data_t* query_data = amxc_container_of(query->it.llist, gmaps_query_data_t, subscribers);
    return query_data->name;
exit:
    return NULL;
}

gmap_query_t* gmaps_query_find(uint32_t index, uint32_t id) {
    gmap_query_t* gmap_query = NULL;
    gmaps_query_data_t* query_data = NULL;

    amxd_object_t* query_template_obj = gmap_dm_get_devices_query();
    when_null(query_template_obj, exit);
    amxd_object_t* query_obj = amxd_object_get_instance(query_template_obj, NULL, index);
    when_null(query_obj, exit);
    query_data = query_obj->priv;
    when_null(query_data, exit);
    amxc_llist_for_each(it, &query_data->subscribers) {
        gmap_query = amxc_container_of(it, gmap_query_t, it);
        if(gmap_query->id == id) {
            /* the function was already registered, so stop looping */
            break;
        }
        gmap_query = NULL;
    }
exit:
    return gmap_query;
}

void gmaps_query_close(gmap_query_t* gmap_query) {
    amxd_object_t* query_template_obj = NULL;
    amxd_object_t* query_obj = NULL;
    gmaps_query_data_t* query_data = NULL;

    when_null_trace(gmap_query, exit, ERROR, "query is NULL pointer");

    SAH_TRACEZ_INFO(ME, "gmaps query close");

    gmap_query->refcount--;
    SAH_TRACEZ_INFO(ME, "current query refcount: %d", gmap_query->refcount);
    if(gmap_query->refcount) {
        return;
    }
    query_template_obj = gmap_dm_get_devices_query();
    when_null(query_template_obj, exit);
    query_obj = amxd_object_get_instance(query_template_obj, NULL, gmap_query->index);
    when_null_trace(query_obj, exit, ERROR, "can not get object instance");
    query_data = query_obj->priv;
    when_null(query_data, exit);
    amxc_llist_it_take(&gmap_query->it);
    free(gmap_query);
    if(!amxc_llist_is_empty(&query_data->subscribers)) {
        return;
    }
    gmaps_query_free_query_data(query_data);
    gmaps_query_destroy(&query_obj);
exit:
    return;
}

static int gmaps_query_data_cleanup(GMAPS_UNUSED amxd_object_t* object, amxd_object_t* mobject, GMAPS_UNUSED void* priv) {
    SAH_TRACEZ_INFO(ME, "query data cleanup");
    gmaps_query_data_t* query_data = NULL;
    when_null(mobject, exit);
    query_data = (gmaps_query_data_t*) mobject->priv;
    mobject->priv = NULL;
    when_null(query_data, exit);
    when_false(amxd_object_get_type(mobject) == amxd_object_instance, exit);
    gmaps_query_free_query_data(query_data);
exit:
    return 0;
}

void GMAPS_PRIVATE gmaps_query_init(void) {
    SAH_TRACEZ_INFO(ME, "gmaps query init");

    amxc_llist_init(&verify_queue.verify_entries);
    amxp_timer_new(&verify_queue.timer, s_query_verify_all_timer_cb, NULL);
}

void gmaps_query_cleanup(void) {
    SAH_TRACEZ_INFO(ME, "gmaps query cleanup");

    amxd_object_t* query_template_obj = gmap_dm_get_devices_query();
    amxd_object_for_all(query_template_obj, ".[Alias != ''].", gmaps_query_data_cleanup, NULL);

    if(!amxc_llist_is_empty(&verify_queue.verify_entries)) {
        SAH_TRACEZ_ERROR(ME, "Cleaning up verify queue while it still has pending entries");
    }
    amxc_llist_clean(&verify_queue.verify_entries, NULL);

    amxp_timer_delete(&verify_queue.timer);
}

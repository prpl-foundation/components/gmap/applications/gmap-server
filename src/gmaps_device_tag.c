/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/


#include <stdlib.h>
#include <string.h>
#include <debug/sahtrace.h>
#include "debug/sahtrace_macros.h"
#include "gmaps_priv.h"

#define ME "tags"

typedef struct cb_data {
    amxc_set_t* tags;
    bool is_found;
} cb_data_t;

static gmap_traverse_status_t gmaps_device_has_tag_traverse_cb(amxd_object_t* device,
                                                               gmap_traverse_action_t action,
                                                               void* userdata) {
    cb_data_t* cb_data = (cb_data_t*) userdata;
    if(action == gmap_traverse_device_matching) {
        cb_data->is_found |= gmaps_device_tag_has_all(device, cb_data->tags);
    }

    return (cb_data->is_found ? gmap_traverse_done : gmap_traverse_continue);
}

gmap_status_t gmaps_device_has_tag(const char* key, const ssv_string_t tag, const char* expression, gmap_traverse_mode_t mode, bool* result) {
    // ACL TODO: add ACL control when available in Ambiorix.
    gmap_status_t retval = gmap_status_unknown_error;
    amxd_object_t* device = NULL;
    cb_data_t cb_data = { .tags = NULL, .is_found = false};
    gmap_traverse_status_t travresult = gmap_traverse_failed;

    when_null_status(result, exit, retval = gmap_status_invalid_parameter);
    *result = false;

    when_str_empty_status(key, exit, retval = gmap_status_invalid_key);
    when_str_empty_status(tag, exit, retval = gmap_status_invalid_parameter);

    cb_data.tags = gmaps_device_tag_parse(tag);
    when_null_status(cb_data.tags, exit, retval = gmap_status_invalid_parameter);

    device = gmaps_get_device(key);
    when_null_status(device, exit, retval = gmap_status_device_not_found);

    travresult = gmaps_traverse_tree(device, mode, expression,
                                     gmaps_device_has_tag_traverse_cb, (void*) &cb_data);

    *result = cb_data.is_found;
    retval = (travresult == gmap_traverse_done) ? gmap_status_ok : gmap_status_unknown_error;

exit:
    amxc_set_delete(&cb_data.tags);

    return retval;
}

static gmap_traverse_status_t gmaps_device_set_tag_traverse_cb(amxd_object_t* device,
                                                               gmap_traverse_action_t action,
                                                               void* userdata) {
    if(action == gmap_traverse_device_matching) {
        /* Unchecked set because we verified that we won't set the protected tags in gmaps_device_set_tag */
        gmaps_device_tag_set_unchecked(device, (const amxc_set_t*) userdata);
    }
    return gmap_traverse_continue;
}

/**
 * Add tags to the Tags parameter of the given device object.
 * This function does not allow setting the tags to "physical", "self" and "lan".
 * This can only be done at the creation of the device
 */
gmap_status_t gmaps_device_set_tag(const char* key, const ssv_string_t tags, const char* expression, gmap_traverse_mode_t mode) {
    // ACL TODO: add ACL control when available in Ambiorix.
    gmap_status_t retval = gmap_status_unknown_error;
    amxd_object_t* device = NULL;
    amxc_set_t* parsed = NULL;
    gmap_traverse_status_t result = gmap_traverse_failed;

    when_str_empty_status(key, exit, retval = gmap_status_invalid_key);
    when_str_empty_status(tags, exit, retval = gmap_status_ok);

    retval = gmap_status_invalid_parameter;
    parsed = gmaps_device_tag_parse(tags);
    when_null(parsed, exit);

    device = gmaps_get_device(key);

    /* If traversal is not this, don't bother checking if this device already has
     * any of these forbidden tags we're trying to set. We're potentially writing
     * to other devices up/down the device tree, so rather than fail halfway or
     * do the search twice, just forbid the attempt outright as it is at most a
     * no-op.
     * When only the current device is targeted, allow setting the tags it already,
     * has, because some components rely on this behavior. A device that doesn't
     * exist is assumed to not have any tags.
     */
    if(mode == gmap_traverse_this) {
        when_true_trace(amxc_set_has_flag(parsed, "physical") && !gmaps_device_tag_has(device, "physical"), exit,
                        ERROR, "The 'physical' tag can only be set at the creation of the device");
        when_true_trace(amxc_set_has_flag(parsed, "self") && !gmaps_device_tag_has(device, "self"), exit,
                        ERROR, "The 'self' tag can only be set at the creation of the device");
        when_true_trace(amxc_set_has_flag(parsed, "lan") && !gmaps_device_tag_has(device, "lan"), exit,
                        ERROR, "The 'lan' tag can only be set at the creation of the device");
    } else {
        when_true_trace(amxc_set_has_flag(parsed, "physical"), exit,
                        ERROR, "The 'physical' tag can only be set at the creation of the device");
        when_true_trace(amxc_set_has_flag(parsed, "self"), exit,
                        ERROR, "The 'self' tag can only be set at the creation of the device");
        when_true_trace(amxc_set_has_flag(parsed, "lan"), exit,
                        ERROR, "The 'lan' tag can only be set at the creation of the device");
    }

    /* Report on device existence after checking the parameters for validity. */
    when_null_status(device, exit, retval = gmap_status_device_not_found);

    result = gmaps_traverse_tree(device, mode, expression,
                                 gmaps_device_set_tag_traverse_cb, (void*) parsed);

    retval = (result == gmap_traverse_done ? gmap_status_ok : gmap_status_unknown_error);

exit:
    amxc_set_delete(&parsed);

    return retval;
}

static gmap_traverse_status_t gmaps_device_clear_tag_traverse_cb(amxd_object_t* device,
                                                                 gmap_traverse_action_t action,
                                                                 void* userdata) {
    if(action == gmap_traverse_device_matching) {
        gmaps_device_tag_clear(device, (const amxc_set_t*) userdata);
    }
    return gmap_traverse_continue;
}

gmap_status_t GMAPS_PRIVATE gmaps_device_clear_tag(const char* key, const ssv_string_t tags, const char* expression, gmap_traverse_mode_t mode) {
    gmap_status_t retval = gmap_status_unknown_error;
    amxd_object_t* device = NULL;
    amxc_set_t* parsed = NULL;
    gmap_traverse_status_t result = gmap_traverse_failed;

    when_str_empty_status(key, exit, retval = gmap_status_invalid_key);
    when_str_empty_status(tags, exit, retval = gmap_status_ok);

    retval = gmap_status_invalid_parameter;
    parsed = gmaps_device_tag_parse(tags);
    when_null(parsed, exit);
    when_true_trace(amxc_set_has_flag(parsed, "physical"), exit,
                    ERROR, "The 'physical' tag can not be removed");
    when_true_trace(amxc_set_has_flag(parsed, "self"), exit,
                    ERROR, "The 'self' tag can not be removed");
    when_true_trace(amxc_set_has_flag(parsed, "lan"), exit,
                    ERROR, "The 'lan' tag can not be removed");

    device = gmaps_get_device(key);
    when_null_status(device, exit, retval = gmap_status_device_not_found);

    result = gmaps_traverse_tree(device, mode, expression,
                                 gmaps_device_clear_tag_traverse_cb, (void*) parsed);

    retval = (result == gmap_traverse_done ? gmap_status_ok : gmap_status_unknown_error);

exit:
    amxc_set_delete(&parsed);

    return retval;
}
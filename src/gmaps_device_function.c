/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <string.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include "gmaps_priv.h"

#define ME "function"

typedef struct _callid {
    amxc_llist_it_t it;
    uint64_t callid;
} gmaps_callid_t;

static const char* gmaps_get_subobjpath_part(const char* fullpath) {

    const char* path = strstr(fullpath, "Devices.Device.");
    if(path) {
        path = path + strlen("Devices.Device.");
        if(*path) {
            path = strchr(path, '.');
            if(path && (*(path + 1) != 0)) {
                return (path + 1);
            }
        }
    }
    return NULL;
}

static void gmaps_remove_callid_from_list(uint64_t callid, amxc_llist_t* const callids) {
    gmaps_callid_t* callid_l = NULL;
    amxc_llist_for_each(it, callids) {
        callid_l = amxc_container_of(it, gmaps_callid_t, it);
        if(callid_l->callid == callid) {
            break;
        }
        callid_l = NULL;
    }
    if(callid_l) {
        SAH_TRACEZ_INFO(ME, "callid removed %d", (uint32_t) callid_l->callid);
        amxc_llist_it_take(&callid_l->it);
        free(callid_l);
    }
}

static amxd_status_t gmaps_device_csi_function(amxd_object_t* object,
                                               amxd_function_t* func,
                                               amxc_var_t* args,
                                               amxc_var_t* ret) {
    amxc_var_t event_data;
    const char* key = NULL;
    amxc_var_t* device_var = NULL;
    amxc_var_t* userdata_var = NULL;
    amxc_var_t* args_var = NULL;
    char* fullobj_path = NULL;
    const char* subobj_path = NULL;
    gmaps_callid_t* callid = NULL;
    amxd_status_t status = amxd_status_unknown_error;
    gmaps_function_data_t* func_data = (gmaps_function_data_t*) func->priv;

    SAH_TRACEZ_INFO(ME, "enter gmaps_device_csi_function");

    // need to maintain list of the call ids in case of func removal
    callid = calloc(1, sizeof(gmaps_callid_t));
    when_null_trace(callid, exit, ERROR, "mem alloc failure");
    status = amxd_function_defer(func, &callid->callid, ret, NULL, &func_data->callids);
    when_failed_trace(status, exit, ERROR, "can not defer function");
    amxc_llist_append(&func_data->callids, &callid->it);

    // fill the event_data var
    amxc_var_init(&event_data);
    amxc_var_set_type(&event_data, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(uint64_t, &event_data, "ID", callid->callid);
    amxc_var_add_key(cstring_t, &event_data, "Function", func->name);

    // add device map + user data
    key = amxd_object_get_name(func_data->device, AMXD_OBJECT_NAMED);
    device_var = amxc_var_add_new_key(&event_data, "Device");
    gmaps_device_get(key, device_var, GMAP_NO_ACTIONS);

    userdata_var = amxc_var_add_new_key(device_var, "user_data");
    amxc_var_copy(userdata_var, &func_data->user_data);

    // add args
    args_var = amxc_var_add_new_key(&event_data, "Args");
    amxc_var_copy(args_var, args);

    // add (sub)object path
    fullobj_path = amxd_object_get_path(object, AMXD_OBJECT_NAMED | AMXD_OBJECT_TERMINATE);
    subobj_path = gmaps_get_subobjpath_part(fullobj_path);
    amxc_var_add_key(cstring_t,
                     &event_data,
                     "ObjectPath",
                     subobj_path);
    SAH_TRACEZ_INFO(ME, "CSI function: func name %s subobj_path:%s callid %d", func->name, subobj_path, (uint32_t) callid->callid);
    gmaps_event_send(key, GMAP_DEVICE_FUNCTION_CALL_STR, GMAP_DEVICE_FUNCTION_CALL, &event_data);
    free(fullobj_path);
    amxc_var_clean(&event_data);

    status = amxd_status_deferred;
exit:
    if(status != amxd_status_deferred) {
        free(callid);
    }
    return status;
}

gmap_status_t gmaps_device_is_function_implemented(amxd_object_t* device,
                                                   const char* sub_object,
                                                   const char* function,
                                                   amxc_var_t* ret) {
    amxd_object_t* object = NULL;
    amxd_function_t* func = NULL;
    gmap_status_t status = gmap_status_ok;

    if(sub_object) {
        object = amxd_object_findf(device, "%s", sub_object);
    } else {
        object = device;
    }
    func = amxd_object_get_function(object, function);
    when_null_status(func, exit, status = gmap_status_not_implemented);
    amxc_var_set(bool, ret, func->impl != NULL);

exit:
    return status;
}

gmap_status_t gmaps_device_set_function(amxd_object_t* device,
                                        const char* sub_object,
                                        const char* function,
                                        amxc_var_t* data) {
    gmap_status_t status = gmap_status_ok;
    amxd_object_t* object = NULL;
    amxd_function_t* func = NULL;

    if(sub_object) {
        object = amxd_object_findf(device, "%s", sub_object);
    } else {
        object = device;
    }
    func = amxd_object_get_function(object, function);
    when_null_status(func, exit, status = gmap_status_not_implemented);
    when_false_trace((func->impl == NULL), exit, NOTICE, "function %s has already an implementation ", function);
    SAH_TRACEZ_INFO(ME, "Set function: %s  sub_object: %s not yet implemented", function, sub_object);
    if(amxd_object_change_function(object,
                                   function,
                                   gmaps_device_csi_function) != amxd_status_ok) {
        status = gmap_status_unknown_error;
        goto exit;
    }
    SAH_TRACEZ_INFO(ME, "function changed");
    func = amxd_object_get_function(object, function);
    if(!func->priv) {
        func->priv = calloc(1, sizeof(gmaps_function_data_t));
        if(!func->priv) {
            status = gmap_status_unknown_error;
            goto exit;
        }
        gmaps_function_data_t* func_data = (gmaps_function_data_t*) func->priv;
        func_data->device = device;
        amxc_var_init(&func_data->user_data);
        if(data) {
            amxc_var_copy(&func_data->user_data, data);
        }
        amxc_llist_init(&func_data->callids);
    }

exit:
    return status;
}

static void gmaps_cleanctxt_ongoingcalls(amxc_llist_t* callids) {

    amxc_llist_it_t* it = amxc_llist_get_first(callids);
    amxc_llist_it_t* prefetch = NULL;
    gmaps_callid_t* callid_l = NULL;
    while(it) {
        prefetch = amxc_llist_it_get_next(it);
        callid_l = amxc_container_of(it, gmaps_callid_t, it);
        amxd_function_deferred_remove(callid_l->callid);
        amxc_llist_it_take(&callid_l->it);
        free(callid_l);
        callid_l = NULL;
        it = prefetch;
    }
}

gmap_status_t gmaps_device_function_done(uint64_t id,
                                         amxd_status_t status,
                                         amxc_var_t* retval) {
    SAH_TRACEZ_INFO(ME, "csi function done id=%d", (uint32_t) id);
    amxc_llist_t* callids = (amxc_llist_t*) amxd_function_deferred_get_priv(id);
    amxd_function_deferred_done(id,
                                status,
                                NULL, //out_args ?
                                retval);
    gmaps_remove_callid_from_list(id, callids);
    return gmap_status_ok;
}

gmap_status_t gmaps_device_remove_function(amxd_object_t* device,
                                           const char* sub_object,
                                           const char* function) {
    amxd_object_t* object = NULL;
    amxd_function_t* func = NULL;
    gmap_status_t status = gmap_status_unknown_error;
    amxd_status_t ret = amxd_status_ok;

    SAH_TRACEZ_INFO(ME, "enter gmaps_device_remove_function subobject: %s function: %s", sub_object, function);

    if(sub_object) {
        object = amxd_object_findf(device, "%s", sub_object);
    } else {
        object = device;
    }
    func = amxd_object_get_function(object, function);
    when_null_trace(func, exit, ERROR, "function not found: %s", function);
    if(func->priv) {  // should always be the case
        gmaps_function_data_t* func_data = (gmaps_function_data_t*) func->priv;
        amxc_var_clean(&func_data->user_data);
        gmaps_cleanctxt_ongoingcalls(&func_data->callids);
        if(!func_data->action_name) {
            free(func->priv);
            func->priv = NULL;
        }
    }
    ret = amxd_object_change_function(object, function, NULL);
    when_failed_trace(ret, exit, ERROR, "Can not set function to null");

    status = gmap_status_ok;
exit:
    return status;
}

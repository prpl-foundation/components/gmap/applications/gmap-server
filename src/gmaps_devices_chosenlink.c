/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <string.h>

#include "gmaps_priv.h"
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#define ME "link"

/**
 * @file This file manages Devices.Device.[].UDevice and Devices.Device.[].LDevice
 * (for Devices.Device.[].Link: see gmaps_devices_multilink.c)
 *
 * For adding/removing/changing the links, you probably do not want to call functions from this
 * lower-level file, and instead want to call functions from the higher-level gmaps_devices_multilink.c.
 */

/**
 * Creates an upper or lower link for the given @param device, depending on @param link_type.
 * If any link of the given @param link_type already exists between the
 * devices, nothing is done and the exisiting link is returned.
 * @return true on success, false on failure.
 */
static bool gmaps_device_create_link(amxd_object_t* device,
                                     const char* key,
                                     const char* type_parameter,
                                     gmaps_link_type_t link_type) {
    amxd_object_t* linked_template = amxd_object_get_child(device,
                                                           gmaps_device_get_link_text(link_type));
    amxd_object_t* link = amxd_object_get_instance(linked_template, key, 0);
    amxd_status_t status = amxd_status_unknown_error;
    amxd_trans_t trans;
    amxd_trans_init(&trans);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);

    if(link == NULL) {
        amxd_trans_select_object(&trans, linked_template);
        amxd_trans_add_inst(&trans, 0, key);
    } else {
        amxd_trans_select_object(&trans, link);
    }
    amxd_trans_set_value(cstring_t, &trans, "Type", type_parameter);
    amxd_trans_set_value(cstring_t, &trans, "Alias", key);
    status = amxd_trans_apply(&trans, gmap_get_dm());

    amxd_trans_clean(&trans);

    return status == amxd_status_ok;
}

static void s_removeLink(amxd_trans_t* trans, const char* upper, const char* lower) {
    amxd_object_t* upper_dev = gmaps_get_device(upper);
    amxd_object_t* lower_dev = gmaps_get_device(lower);
    amxd_object_t* ulink_templ = amxd_object_get(lower_dev, "UDevice");
    amxd_object_t* llink_templ = amxd_object_get(upper_dev, "LDevice");
    amxd_object_t* ulink = amxd_object_get_instance(ulink_templ, upper, 0);
    amxd_object_t* llink = amxd_object_get_instance(llink_templ, lower, 0);
    when_null_trace(trans, exit, ERROR, "NULL argument");
    when_null_trace(upper, exit, ERROR, "NULL argument");
    when_null_trace(lower, exit, ERROR, "NULL argument");

    if((ulink == NULL) != (llink == NULL)) {
        SAH_TRACEZ_ERROR(ME, "%s %s Invariant violated: upper<->lower link must always be in both directions", upper, lower);
    }
    if(ulink != NULL) {
        amxd_trans_select_object(trans, ulink_templ);
        // Note: remove by index insteadof by name because currently by name does not deal well
        //       with difficult characters like ".", "\"", "[".
        amxd_trans_del_inst(trans, amxd_object_get_index(ulink), NULL);
    }
    if(llink != NULL) {
        amxd_trans_select_object(trans, llink_templ);
        amxd_trans_del_inst(trans, amxd_object_get_index(llink), NULL);
    }

exit:
    return;
}

/**
 *
 * This is a lower-level API and should normally not be called directly.
 * Consider using @ref gmaps_devices_multilink_remove instead!
 */
void gmaps_devices_chosenlink_remove(const char* upper, const char* lower) {
    amxd_status_t status = amxd_status_unknown_error;
    amxd_trans_t trans;
    amxd_trans_init(&trans);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);

    if(((upper == NULL) || (upper[0] == '\0')) &&
       ((lower != NULL) && (lower[0] != '\0'))) {
        amxd_object_t* dev = gmaps_get_device(lower);
        amxd_object_t* dev_upper = amxd_object_get_child(dev, "UDevice");

        amxd_object_for_each(instance, child_it, dev_upper) {
            amxd_object_t* udev = amxc_llist_it_get_data(child_it, amxd_object_t, it);
            char* ukey = amxd_object_get_value(cstring_t, udev, "Alias", NULL);

            s_removeLink(&trans, ukey, lower);

            free(ukey);
        }
    } else if(((lower == NULL) || (lower[0] == '\0')) &&
              ((upper != NULL) && (upper[0] != '\0'))) {
        amxd_object_t* dev = gmaps_get_device(upper);
        amxd_object_t* dev_lower = amxd_object_get_child(dev, "LDevice");

        amxd_object_for_each(instance, child_it, dev_lower) {
            amxd_object_t* ldev = amxc_llist_it_get_data(child_it, amxd_object_t, it);
            char* lkey = amxd_object_get_value(cstring_t, ldev, "Alias", NULL);

            s_removeLink(&trans, upper, lkey);

            free(lkey);
        }
    } else {
        s_removeLink(&trans, upper, lower);
    }

    status = amxd_trans_apply(&trans, gmap_get_dm());
    when_failed_trace(status, exit, ERROR, "Error applying transaction");

exit:
    amxd_trans_clean(&trans);
}

/**
 *
 * This is a lower-level API and should normally not be called directly.
 * Consider using @ref gmaps_devices_multilink_remove instead!
 */
void gmaps_devices_chosenlink_removeAllULink(amxd_object_t* device, const char* keep) {
    amxd_trans_t trans;
    amxd_status_t status = amxd_status_unknown_error;
    amxd_object_t* link = amxd_object_get_child(device, "UDevice");
    const char* key_lower = amxd_object_get_name(device, AMXD_OBJECT_NAMED);
    amxd_trans_init(&trans);

    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);

    amxd_object_for_each(instance, ulink_it, link) {
        amxd_object_t* ulink = amxc_llist_it_get_data(ulink_it, amxd_object_t, it);
        const char* key_upper = amxd_object_get_name(ulink, AMXD_OBJECT_NAMED);
        if((keep == NULL) || (strcmp(keep, key_upper) != 0)) {
            s_removeLink(&trans, key_upper, key_lower);
        }
    }

    status = amxd_trans_apply(&trans, gmap_get_dm());
    when_failed_trace(status, exit, ERROR, "Error applying transaction");

exit:
    amxd_trans_clean(&trans);
}

static unsigned int gmaps_device_removeAllULinkRetCode(amxd_object_t* device, const char* keep) {
    gmaps_devices_chosenlink_removeAllULink(device, keep);
    return 0;
}

/**
 *
 * This is a lower-level API and should normally not be called directly.
 * Consider using @ref gmaps_devices_multilink_remove instead!
 */
void gmaps_devices_chosenlink_removeAllLLink(amxd_object_t* device, const char* keep) {
    amxd_trans_t trans;
    amxd_status_t status = amxd_status_unknown_error;
    amxd_object_t* link = amxd_object_get_child(device, "LDevice");
    const char* key_upper = amxd_object_get_name(device, AMXD_OBJECT_NAMED);
    amxd_trans_init(&trans);

    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);

    amxd_object_for_each(instance, llink_it, link) {
        amxd_object_t* llink = amxc_llist_it_get_data(llink_it, amxd_object_t, it);
        const char* key_lower = amxd_object_get_name(llink, AMXD_OBJECT_NAMED);
        if((keep == NULL) || (strcmp(keep, key_lower) != 0)) {
            s_removeLink(&trans, key_upper, key_lower);
        }
    }

    status = amxd_trans_apply(&trans, gmap_get_dm());
    when_failed_trace(status, exit, ERROR, "Error applying transaction");

exit:
    amxd_trans_clean(&trans);
}

/**
 * Device specific book keeping.
 * TODO: consider to provide a hook for this.
 */
static bool gmaps_device_update_interface_name(amxd_object_t* upper,
                                               amxd_object_t* lower) {
    bool retval = false;
    char* netdevname = amxd_object_get_value(cstring_t, upper, "NetDevName", NULL);
    char* name = NULL;
    amxd_trans_t trans;

    amxd_trans_init(&trans);

    when_str_empty(netdevname, exit);

    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);

    name = amxd_object_get_value(cstring_t, upper, "Name", NULL);
    amxd_trans_select_object(&trans, lower);
    amxd_trans_set_value(cstring_t, &trans, "Layer2Interface", netdevname);
    amxd_trans_set_value(cstring_t, &trans, "InterfaceName", name);
    when_failed_status(amxd_trans_apply(&trans, gmap_get_dm()), exit, retval = false);

    retval = true;
exit:
    free(netdevname);
    free(name);
    amxd_trans_clean(&trans);
    return retval;
}

/**
 * Callback that checks if the @param device has a link in the direction
 * given in @param priv.
 *
 * @return Returns @ref gmap_status_break if the link is
 * found, else @param gmap_status_ok.
 */
static gmap_status_t gmaps_device_cb_check_link(amxd_object_t* device,
                                                void* priv) {
    return gmaps_device_has_link(device, (gmaps_link_type_t) priv, false)
           ? gmap_status_break
           : gmap_status_ok;
}

/**
 * Make Devices.Device.[].UDevice / Devices.Device.[].LDevice
 *
 * This is a lower-level API and should normally not be called directly.
 * Consider using @ref gmaps_devices_multilink_add instead!
 */
gmap_status_t gmaps_devices_chosenlink_make(const char* upper,
                                            const char* lower,
                                            const char* type,
                                            bool single) {
    gmap_status_t status = gmap_status_unknown_error;
    amxd_object_t* upper_dev = gmaps_get_device(upper);
    amxd_object_t* lower_dev = gmaps_get_device(lower);
    amxd_object_t* master_lower_dev = NULL;
    amxd_object_t* master_upper_dev = NULL;
    char* master_upper = NULL;
    char* master_lower = NULL;

    when_null_status(upper_dev, exit, status = gmap_status_device_not_found);
    when_null_status(lower_dev, exit, status = gmap_status_device_not_found);
    when_null_status(type, exit, status = gmap_status_invalid_parameter);

    // A master and an alternative device can not be linked together.
    // They are already linked in a relationship. (master - alternative).
    when_true_status(gmaps_device_are_alternatives(upper_dev, lower_dev),
                     exit,
                     status = gmap_status_invalid_request);

    // Use master for linking if it exists.
    master_upper_dev = upper_dev;
    when_false_status(gmaps_device_get_master(&master_upper_dev),
                      exit,
                      status = gmap_status_internal_error);
    master_lower_dev = lower_dev;
    when_false_status(gmaps_device_get_master(&master_lower_dev),
                      exit,
                      status = gmap_status_internal_error);

    // we need to use upper and lower of these master devices in that case
    if(master_upper_dev != upper_dev) {
        master_upper = amxd_object_get_value(cstring_t, upper_dev, "Master", NULL);
    } else {
        master_upper = strdup(upper);
    }
    if(master_lower_dev != lower_dev) {
        master_lower = amxd_object_get_value(cstring_t, lower_dev, "Master", NULL);
    } else {
        master_lower = strdup(lower);
    }

    // IMPROVEMENT Consider transaction, so that all succeeds or all fails

    if(single) {
        gmaps_devices_chosenlink_removeAllULink(master_lower_dev, NULL);
        gmaps_device_for_all_alternatives(master_lower_dev,
                                          (gmaps_device_task_fn_t) gmaps_device_removeAllULinkRetCode,
                                          NULL);
    }

    // Check that the link already exists.
    when_true_status(gmaps_device_is_linked_to(master_lower_dev, master_upper_dev, gmap_traverse_one_up),
                     exit,
                     status = gmap_status_ok);

    // Create the links (from lower to upper and from upper to lower).
    when_false_status(gmaps_device_create_link(master_upper_dev, master_lower, type, gmaps_link_type_lower_link),
                      exit,
                      status = gmap_status_internal_error);
    when_false_status(gmaps_device_create_link(master_lower_dev, master_upper, type, gmaps_link_type_upper_link),
                      exit,
                      status = gmap_status_internal_error);

    status = gmap_status_ok;

    if(gmaps_device_tag_has(master_lower_dev, "mac")) {
        // Change Layer2Interface and InterfaceName.
        // IMPROVEMENT consider to provide a hook for this.
        // This may not fail setting the link
        when_false_trace(gmaps_device_update_interface_name(master_upper_dev, master_lower_dev),
                         exit, ERROR, "Failed to update interface name for device '%s'", master_lower);
    }

exit:
    free(master_lower);
    free(master_upper);
    return status;
}


/* used for is linked to traverse */
typedef struct _linked_to_data {
    amxd_object_t* search_device;
    bool result;
} linked_to_data_t;

static gmap_traverse_status_t gmaps_traverse_is_linked_to(amxd_object_t* device,
                                                          gmap_traverse_action_t action,
                                                          void* userdata) {
    gmap_traverse_status_t status = gmap_traverse_continue;
    linked_to_data_t* data = userdata;

    switch(action) {
    case gmap_traverse_device_not_matching:
        break;
    case gmap_traverse_device_matching:

        if(strcmp(amxd_object_get_name(device, AMXD_OBJECT_NAMED), amxd_object_get_name(data->search_device, AMXD_OBJECT_NAMED)) == 0) {
            data->result = true;
            status = gmap_traverse_done;
        } else if(!data->result) {
            amxd_object_t* alternatives = NULL;
            amxd_object_t* altdevice = NULL;
            // if a device has alternatives and if one of the alternatives is active, the master device is
            // active as well.
            // loop over all alternatives until one of the alternatives is set to active
            alternatives = amxd_object_get_child(device, "Alternative");
            amxd_object_for_each(instance, lit, alternatives) {
                altdevice = amxc_llist_it_get_data(lit, amxd_object_t, it);
                if(strcmp(amxd_object_get_name(data->search_device, AMXD_OBJECT_NAMED), amxd_object_get_name(altdevice, AMXD_OBJECT_NAMED)) == 0) {
                    data->result = true;
                    status = gmap_traverse_done;
                }


            }
        }
        break;
    case gmap_traverse_start:
        break;
    case gmap_traverse_level_push:
        break;
    case gmap_traverse_device_done:
        break;
    case gmap_traverse_level_pop:
        break;
    case gmap_traverse_stop:
        break;
    default:
        break;
    }

    return status;

}

/**
 * This checks if the device is linked by selected links, i.e. links under
 * Devices.Device.[].UDevice/LDevice,
 * but ignoring links from Devices.Device.[].Link if they are not selected to be a link in
 * Devices.Device.[].UDevice/LDevice.
 * This function is safe to use directly (i.e. no need to use a higher-level API instead).
 */
bool gmaps_device_is_linked_to(amxd_object_t* dev,
                               amxd_object_t* device,
                               gmap_traverse_mode_t mode) {

    bool result = false;
    amxd_object_t* alternatives = NULL;
    amxd_object_t* altdevice = NULL;
    linked_to_data_t data;

    when_null(device, exit);
    when_null(dev, exit);

    // if the device is an alternative for a master device, check the links of the master device
    gmaps_device_get_master(&dev);

    // loop over the alternatives and check if one the alternatives is matching the device key
    if((mode == gmap_traverse_this) ||
       ( mode == gmap_traverse_down) ||
       ( mode == gmap_traverse_up) ||
       ( mode == gmap_traverse_one_down) ||
       ( mode == gmap_traverse_one_up)) {
        alternatives = amxd_object_get_child(dev, "Alternative");
        amxd_object_for_each(instance, lit, alternatives) {
            altdevice = amxc_llist_it_get_data(lit, amxd_object_t, it);

            if(strcmp(amxd_object_get_name(device, AMXD_OBJECT_NAMED), amxd_object_get_name(altdevice, AMXD_OBJECT_NAMED)) == 0) {
                result = true;
            }

        }
    }

    if(!result) {
        data.result = false;
        data.search_device = device;
        gmaps_traverse_tree(dev, mode, "", gmaps_traverse_is_linked_to, &data);
        result = data.result;
    }

exit:
    return result;

}

/**
 * Returns true if the @param device has an edge of the given @param type.
 * If @param include_alternatives is true, also the alternative devices are considered.
 */
bool gmaps_device_has_link(amxd_object_t* device,
                           gmaps_link_type_t type,
                           bool include_alternatives) {
    bool retval = false;
    amxd_object_t* edge_template = amxd_object_get_child(device,
                                                         gmaps_device_get_link_text(type));

    when_true_status(amxd_object_get_instance_count(edge_template) > 0, exit, retval = true);

    if(include_alternatives
       && (gmaps_device_for_all_alternatives(device,
                                             gmaps_device_cb_check_link,
                                             (void*) type) == gmap_status_break)) {
        retval = true;
    }

exit:
    return retval;
}

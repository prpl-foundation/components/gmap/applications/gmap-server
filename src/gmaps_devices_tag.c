/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <string.h>
#include <debug/sahtrace.h>
#include "debug/sahtrace_macros.h"
#include "gmaps_priv.h"

#define ME "tags"


void gmaps_device_tag_device_init(amxd_object_t* device, gmap_device_priv_data_t* data) {
    amxd_status_t status;
    amxc_var_t value;
    const char* tags_string = NULL;

    amxc_var_init(&value);

    when_null(device, exit);
    when_null(data, exit);

    amxc_set_init(&data->tags, false);

    status = amxd_object_get_param(device, "Tags", &value);
    when_failed_trace(status, exit, ERROR, "Failed to init tags for device '%s'", amxd_object_get_name(device, AMXD_OBJECT_NAMED));
    tags_string = amxc_var_constcast(ssv_string_t, &value);
    when_str_empty(tags_string, exit);

    amxc_set_parse(&data->tags, tags_string);

exit:
    amxc_var_clean(&value);
    return;
}

void gmaps_device_tag_device_clean(UNUSED amxd_object_t* device, gmap_device_priv_data_t* data) {
    when_null(data, exit);

    amxc_set_clean(&data->tags);
exit:
    return;
}

amxc_set_t* GMAPS_PRIVATE gmaps_device_tag_parse(const ssv_string_t tags) {
    amxc_set_t* parsed = NULL;

    when_failed_trace(amxc_set_new(&parsed, false), exit, ERROR, "Out of memory");

    if(0 != amxc_set_parse(parsed, tags)) {
        SAH_TRACEZ_ERROR(ME, "Invalid tags string '%s'", tags);
        amxc_set_delete(&parsed);
    }

exit:
    return parsed;
}

static amxc_set_t* gmaps_device_get_tags_internal(const amxd_object_t* device) {
    gmap_device_priv_data_t* priv = gmaps_device_private_data(device);
    amxc_set_t* tags = NULL;

    when_null(priv, exit);

    tags = &priv->tags;

exit:
    return tags;
}

static bool gmaps_device_tag_update_dm(amxd_object_t* device) {
    bool result = false;
    amxc_set_t* tags = gmaps_device_get_tags_internal(device);
    amxd_trans_t trans;
    char* tags_string = NULL;

    amxd_trans_init(&trans);

    when_null_trace(tags, exit, ERROR, "No backing tags known for device '%s'",
                    amxd_object_get_name(device, AMXD_OBJECT_NAMED));

    amxd_trans_select_object(&trans, device);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);

    if(amxc_set_get_count(tags, NULL) == 0) {
        amxd_trans_set_ssv_string_t(&trans, "Tags", "");
    } else {
        tags_string = amxc_set_to_string(tags);
        when_null_trace(tags_string, exit, ERROR, "Out of memory");
        amxd_trans_set_ssv_string_t(&trans, "Tags", tags_string);
    }

    when_failed(amxd_trans_apply(&trans, gmap_get_dm()), exit);

    result = true;
    amxo_parser_apply_mibs(gmap_get_parser(), device, gmaps_device_matches);

exit:
    amxd_trans_clean(&trans);
    free(tags_string);

    return result;
}

bool GMAPS_PRIVATE gmaps_device_tag_has(const amxd_object_t* device, const char* tag) {
    bool result = false;
    amxc_set_t* dev_tags = gmaps_device_get_tags_internal(device);

    when_str_empty(tag, exit);
    when_null(dev_tags, exit);

    result = amxc_set_has_flag(dev_tags, tag);

exit:
    return result;
}

bool GMAPS_PRIVATE gmaps_device_tag_has_all(const amxd_object_t* device, const amxc_set_t* tags) {
    bool result = false;
    amxc_set_t* dev_tags = gmaps_device_get_tags_internal(device);

    when_null(tags, exit);
    when_null(dev_tags, exit);

    result = true;
    /* const-cast to work around amxc_set_get_first_flag not accepting the set as a const* */
    amxc_set_iterate(flag, (amxc_set_t*) tags) {
        if(!amxc_set_has_flag(dev_tags, flag->flag)) {
            result = false;
            break;
        }
    }

exit:
    return result;
}

bool GMAPS_PRIVATE gmaps_device_tag_set_unchecked(amxd_object_t* device, const amxc_set_t* tags) {
    bool result = false;
    amxc_set_t* dev_tags = gmaps_device_get_tags_internal(device);
    uint32_t count = amxc_set_get_count(dev_tags, NULL);

    when_null(tags, exit);
    when_null(dev_tags, exit);

    amxc_set_union(dev_tags, tags);

    if(amxc_set_get_count(dev_tags, NULL) != count) {
        result = gmaps_device_tag_update_dm(device);
    } else {
        result = true;
    }

exit:
    return result;
}

bool GMAPS_PRIVATE gmaps_device_tag_set(amxd_object_t* device, const amxc_set_t* tags) {
    bool result = false;
    amxc_set_t* dev_tags = gmaps_device_get_tags_internal(device);
    uint32_t count = amxc_set_get_count(dev_tags, NULL);

    when_null(tags, exit);
    when_null(dev_tags, exit);

    when_true_trace(amxc_set_has_flag(tags, "physical") && !amxc_set_has_flag(dev_tags, "physical"),
                    exit, ERROR, "The 'physical' tag can only be set at the creation of the device");
    when_true_trace(amxc_set_has_flag(tags, "self") && !amxc_set_has_flag(dev_tags, "self"),
                    exit, ERROR, "The 'self' tag can only be set at the creation of the device");
    when_true_trace(amxc_set_has_flag(tags, "lan") && !amxc_set_has_flag(dev_tags, "lan"),
                    exit, ERROR, "The 'lan' tag can only be set at the creation of the device");

    amxc_set_union(dev_tags, tags);

    if(amxc_set_get_count(dev_tags, NULL) != count) {
        result = gmaps_device_tag_update_dm(device);
    } else {
        result = true;
    }

exit:
    return result;
}

bool GMAPS_PRIVATE gmaps_device_tag_clear(amxd_object_t* device, const amxc_set_t* tags) {
    bool result = false;
    amxc_set_t* dev_tags = gmaps_device_get_tags_internal(device);
    uint32_t count = amxc_set_get_count(dev_tags, NULL);

    when_true_trace(amxc_set_has_flag(tags, "physical"),
                    exit, ERROR, "The 'physical' tag can not be removed");
    when_true_trace(amxc_set_has_flag(tags, "self"),
                    exit, ERROR, "The 'self' tag can not be removed");
    when_true_trace(amxc_set_has_flag(tags, "lan"),
                    exit, ERROR, "The 'lan' tag can not be removed");

    amxc_set_subtract(dev_tags, tags);

    if(amxc_set_get_count(dev_tags, NULL) != count) {
        result = gmaps_device_tag_update_dm(device);
    } else {
        result = true;
    }

exit:
    return result;
}

bool GMAPS_PRIVATE gmaps_device_tag_reset(amxd_object_t* device) {
    bool result = false;
    amxc_set_t* dev_tags = gmaps_device_get_tags_internal(device);
    uint32_t count;
    bool has_physical;
    bool has_self;
    bool has_lan;

    when_null(dev_tags, exit);

    count = amxc_set_get_count(dev_tags, NULL);
    has_physical = amxc_set_has_flag(dev_tags, "physical");
    has_self = amxc_set_has_flag(dev_tags, "self");
    has_lan = amxc_set_has_flag(dev_tags, "lan");

    amxc_set_reset(dev_tags);
    if(has_physical) {
        amxc_set_add_flag(dev_tags, "physical");
    }
    if(has_self) {
        amxc_set_add_flag(dev_tags, "self");
    }
    if(has_lan) {
        amxc_set_add_flag(dev_tags, "lan");
    }

    if(amxc_set_get_count(dev_tags, NULL) != count) {
        result = gmaps_device_tag_update_dm(device);
    } else {
        result = true;
    }

exit:
    return result;
}

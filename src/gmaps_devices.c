/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <string.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "gmaps_priv.h"

#include <amxd/amxd_object_event.h>
#include <uuid/uuid.h>
#include "gmaps_devices_multilink.h"

#define ME "devices"

#define LLTD_PROBE_START "00:0D:3A:D7:F1:40"
#define LLTD_PROBE_END   "00:0D:3A:FF:FF:FF"

typedef struct _gmap_device {
    amxc_htable_it_t it;
    amxd_object_t* device;
    gmap_device_priv_data_t data;
} gmap_device_t;

typedef struct _gmap_matching_devs_info {
    amxc_var_t* matching_devs;
    amxp_expr_t* expression;
} gmap_matching_devs_info_t;


static void gmaps_init_device_priv(amxd_object_t* device, gmap_device_t* internal) {
    gmap_device_priv_data_t* priv = &internal->data;
    device->priv = internal;

    gmaps_device_tag_device_init(device, priv);
    gmaps_dm_tpb_device_init_priv(device, priv);
    gmaps_traverse_device_init(device, priv);
    gmaps_query_device_init(device, priv);
}

static void gmaps_clean_device_priv(amxd_object_t* device, gmap_device_t* internal) {
    gmap_device_priv_data_t* priv;
    when_null(device, exit);

    priv = &internal->data;
    when_null(priv, exit);

    gmaps_query_device_clean(device, priv);
    gmaps_traverse_device_clean(device, priv);
    gmaps_dm_tpb_device_clean_priv(device, priv);
    gmaps_device_tag_device_clean(device, priv);

    device->priv = NULL;
exit:
    return;
}

void gmaps_store_device(amxd_object_t* device) {
    gmap_device_t* dev = calloc(1, sizeof(gmap_device_t));
    when_null_trace(dev, exit, ERROR, "Out of Memory");

    dev->device = device;
    gmaps_init_device_priv(device, dev);
    amxc_htable_insert(gmap_get_table(),
                       amxd_object_get_name(device, AMXD_OBJECT_NAMED),
                       &dev->it);

exit:
    return;
}

static gmap_device_t* gmaps_find_device_internal(const char* key) {
    gmap_device_t* dev = NULL;
    amxc_htable_it_t* it = amxc_htable_get(gmap_get_table(), key);

    when_null(it, exit);

    dev = amxc_htable_it_get_data(it, gmap_device_t, it);

exit:
    return dev;
}

static void gmaps_free_device(gmap_device_t* dev) {
    when_null(dev, exit);

    amxc_htable_it_clean(&dev->it, gmaps_clear_device);

exit:
    return;
}

/**
 * Utility function to add the @param device to a list of matching devices if it matches
 * the given expression in @param info.
 */
static gmap_status_t gmaps_add_matching_device(amxd_object_t* device,
                                               void* priv) {
    gmap_status_t status = gmap_status_ok;
    const gmap_matching_devs_info_t* info = NULL;

    when_null_status(priv, exit, status = gmap_status_invalid_parameter);

    info = (const gmap_matching_devs_info_t*) priv;

    if(gmaps_device_matches(device, info->expression)) {
        amxc_var_add(cstring_t,
                     info->matching_devs,
                     amxd_object_get_name(device, AMXD_OBJECT_NAMED));
    }

exit:
    return status;
}

/**
 * Returns true if the device with the given @param key is a LLTD probe.
 * LLTD probes should be considered as blocked devices.
 * Bugzilla reference: 78491.
 */
static bool gmaps_devices_is_LLTD_probe(const char* key) {
    if((strcmp(key, LLTD_PROBE_START) >= 0) && (strcmp(key, LLTD_PROBE_END) <= 0)) {
        return true;
    }
    return false;
}

amxd_object_t* gmaps_get_device(const char* key) {
    amxd_object_t* retval = NULL;
    gmap_device_t* dev = NULL;
    amxc_htable_it_t* it = amxc_htable_get(gmap_get_table(), key);
    when_null_status(it, exit, retval = NULL);
    dev = amxc_htable_it_get_data(it, gmap_device_t, it);

    retval = dev->device;

exit:
    return retval;
}

static amxd_object_t* gmaps_follow_link(amxd_object_t* obj, const char* param) {
    char* key = NULL;
    amxd_object_t* dev = NULL;

    key = amxd_object_get_value(cstring_t, obj, param, NULL);
    when_null_status(key, exit, dev = NULL);
    dev = gmaps_get_device(key);

exit:
    free(key);
    return dev;
}

void gmaps_clear_device(GMAPS_UNUSED const char* key,
                        amxc_htable_it_t* it) {
    gmap_device_t* dev = amxc_htable_it_get_data(it, gmap_device_t, it);
    gmaps_clean_device_priv(dev->device, dev);
    free(dev);
}

static void gmaps_new_add_parameters(amxd_object_t* new_dev, const amxc_var_t* parameters) {
    amxc_htable_t* paramtable = amxc_var_get_amxc_htable_t(parameters);
    amxc_htable_for_each(it, paramtable) {
        amxc_var_t* data = amxc_htable_it_get_data(it, amxc_var_t, hit);
        const char* tkey = amxc_htable_it_get_key(it);

        if((strcmp(tkey, "Active") == 0) ||
           ( strcmp(tkey, "Name") == 0) ||
           ( strcmp(tkey, "Type") == 0) ||
           ( strcmp(tkey, "Key") == 0)) {
            continue;
        }

        amxd_object_set_param(new_dev, tkey, data);
        amxc_var_delete(&data);
    }
    amxc_htable_delete(&paramtable, 0);
}

static void call_remove_lease(const char* path, char* mac) {
    amxc_string_t dhcp_path;
    amxc_string_t mac_lower;
    amxc_var_t* temp_element = NULL;
    amxc_var_t ret;
    amxc_var_t args;
    amxc_var_t* macs_list = NULL;
    amxb_invoke_t* invoke_ctx = NULL;
    amxb_bus_ctx_t* dhcp_ctx = NULL;
    int retval = 0;

    amxc_string_init(&dhcp_path, 0);
    amxc_string_init(&mac_lower, 0);
    amxc_var_init(&ret);
    amxc_var_init(&args);

    amxc_string_set(&dhcp_path, path);
    amxc_string_set(&mac_lower, mac);
    amxc_string_to_lower(&mac_lower);

    when_str_empty(path, exit);

    amxc_string_append(&dhcp_path, "^.^.", 4);
    dhcp_ctx = amxb_be_who_has(amxc_string_get(&dhcp_path, 0));
    when_null_trace(dhcp_ctx, exit, ERROR, "The path %s is not found", amxc_string_get(&dhcp_path, 0));

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    macs_list = amxc_var_add_key(amxc_llist_t, &args, "MACs", NULL);
    temp_element = amxc_var_add_new(macs_list);
    amxc_var_push(cstring_t, temp_element, amxc_string_take_buffer(&mac_lower));

    amxb_new_invoke(&invoke_ctx, dhcp_ctx, amxc_string_get(&dhcp_path, 0), NULL, "cleanClientLeases");
    retval = amxb_invoke(invoke_ctx, &args, &ret, NULL, NULL, 5);
    amxb_free_invoke(&invoke_ctx);
    when_failed_trace(retval, exit, ERROR, "Call to 'cleanClientLeases' for device with mac '%s' failed with status %d",
                      mac, retval);

exit:
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    amxc_string_clean(&dhcp_path);
    amxc_string_clean(&mac_lower);
}

static void gmaps_remove_dhcp_leases(const char* key) {
    bool has_tags = false;
    amxd_object_t* device = gmaps_get_device(key);
    amxd_status_t status = amxd_status_ok;
    char* dhcpv4_client_path = NULL;
    char* dhcpv6_client_path = NULL;
    char* mac = NULL;

    gmaps_device_has_tag(key, "dhcp mac", "", gmap_traverse_this, &has_tags);
    when_false(has_tags, exit);

    mac = amxd_object_get_value(cstring_t, device, "PhysAddress", &status);
    if((mac == NULL) || (mac[0] == '\0')) {
        free(mac);
        SAH_TRACEZ_ERROR(ME, "No PhysAddress value found for device %s", key);
        goto exit;
    }

    dhcpv4_client_path = amxd_object_get_value(cstring_t, device, "DHCPv4Client", &status);
    dhcpv6_client_path = amxd_object_get_value(cstring_t, device, "DHCPv6Client", &status);

    call_remove_lease(dhcpv4_client_path, mac);
    call_remove_lease(dhcpv6_client_path, mac);

    free(dhcpv4_client_path);
    free(dhcpv6_client_path);
    free(mac);

exit:
    return;
}

gmap_status_t gmaps_new_device(amxd_object_t* template_obj,
                               const char* key,
                               const char* discovery_source,
                               const ssv_string_t tags,
                               bool persistent,
                               const char* default_name,
                               const amxc_var_t* parameters) {
    gmap_status_t status = gmap_status_unknown_error;
    amxd_object_t* dev = NULL;
    amxc_set_t* parsed_tags = gmaps_device_tag_parse(tags);
    bool physical_lan;
    bool self;

    when_null_status(template_obj, exit, status = gmap_status_invalid_parameter);
    when_str_empty_status(key, exit, status = gmap_status_invalid_key);
    when_null_status(parsed_tags, exit, status = gmap_status_invalid_parameter);

    when_true_status(gmaps_device_is_blocked(key), exit, status = gmap_status_invalid_key);
    dev = gmaps_get_device(key);

    physical_lan = amxc_set_has_flag(parsed_tags, "lan") && amxc_set_has_flag(parsed_tags, "physical");
    self = amxc_set_has_flag(parsed_tags, "self");

    // Checks that tags '(lan && physical)' are never set together with the tag 'self'
    if(physical_lan && self) {
        SAH_TRACEZ_ERROR(ME, "Cannot set the '(lan && physical)' tags together with self");
        status = gmap_status_invalid_parameter;
        goto exit;
    }

    if(dev == NULL) {
        if(physical_lan) {
            when_false_status(check_max_lan_devices_entries(), exit, status = gmap_status_device_limit_reached);
            when_false_status(check_max_device_entries(), exit, status = gmap_status_device_limit_reached);

            increase_lan_device_count();
        } else {
            when_false_status(check_max_device_entries(), exit, status = gmap_status_data_model_error);
        }

        when_failed_status(amxd_object_new_instance(&dev, template_obj, key, 0, NULL),
                           exit,
                           status = gmap_status_unknown_error);
        gmaps_store_device(dev);
        gmaps_device_tag_set_unchecked(dev, parsed_tags);
    } else {
        gmaps_device_tag_set(dev, parsed_tags);
    }

    amxd_object_set_attr(dev, amxd_oattr_persistent, persistent);
    amxd_object_set_value(cstring_t, dev, "Key", key);
    amxd_object_set_value(cstring_t, dev, "DiscoverySource", discovery_source);

    if(gmap_is_ntp_synchronized()) {
        amxc_ts_t now;
        amxc_ts_now(&now);
        amxd_object_set_value(amxc_ts_t, dev, "FirstSeen", &now);
        amxd_object_set_value(amxc_ts_t, dev, "LastConnection", &now);
    }

    amxo_parser_apply_mibs(gmap_get_parser(), dev, gmaps_device_matches);

    if((default_name != NULL) && (*default_name != 0)) {
        if(gmaps_device_set_name(dev, default_name, "default") != gmap_status_ok) {
            SAH_TRACEZ_WARNING(ME, "Failed to add default name [%s] for device [%s]", default_name, key);
        }
    } else {
        char* def_name = gmaps_device_generate_default_name(dev);
        if(gmaps_device_set_name(dev, def_name, "default") != gmap_status_ok) {
            SAH_TRACEZ_WARNING(ME, "Failed to add default name [%s] for device [%s]", def_name, key);
        }
        free(def_name);

    }
    gmaps_device_select_name(dev);

    gmaps_new_add_parameters(dev, parameters);

    amxd_object_emit_add_inst(dev);

    gmaps_devices_merge_execute_rules(key);


    status = gmap_status_ok;

exit:
    amxc_set_delete(&parsed_tags);

    return status;
}

gmap_status_t gmaps_device_cleanup(amxd_object_t* devices, amxd_object_t** dev, const char* key) {
    gmap_status_t status = gmap_status_ok;
    amxd_object_t* dev_master = NULL;
    amxd_object_t* alternatives = NULL;
    amxd_object_t* names = NULL;
    gmap_device_t* dev_internal = NULL;

    when_null_status(dev, exit, status = gmap_status_invalid_parameter);
    *dev = NULL;

    when_str_empty_status(key, exit, status = gmap_status_invalid_key);
    when_null_status(devices, exit, status = gmap_status_invalid_parameter);

    dev_internal = gmaps_find_device_internal(key);
    when_null_status(dev_internal, exit, status = gmap_status_ok);
    *dev = dev_internal->device;
    when_null_status(*dev, exit, status = gmap_status_ok);

    /* Require both 'self' and 'hgw' to still allow deleting bridge devices of self. */
    status = gmap_status_invalid_request;
    if(gmaps_device_tag_has(*dev, "self") && gmaps_device_tag_has(*dev, "hgw")) {
        SAH_TRACEZ_ERROR(ME, "Self devices can not be deleted (%s)", key);
        *dev = NULL;
        goto exit;
    }
    status = gmap_status_ok;

    // remove all upper and lower links
    gmaps_devices_multilink_on_device_delete(key);

    // Remove the dhcp lease in case present in DHCPvx.Server.Pool.x.Client
    gmaps_remove_dhcp_leases(key);

    if(gmaps_device_tag_has(*dev, "lan") && gmaps_device_tag_has(*dev, "physical")) {
        decrease_lan_device_count();
    }

    // Clean-up alternatives
    dev_master = gmaps_follow_link(*dev, "Master");
    if(dev_master != NULL) {
        gmaps_device_remove_alternative(dev_master, *dev);
    }

    alternatives = amxd_object_get_child(*dev, "Alternative");
    amxd_object_for_each(instance, child_it, alternatives) {
        amxd_object_t* adev = amxc_llist_it_get_data(child_it, amxd_object_t, it);
        amxd_object_t* altdev = gmaps_follow_link(adev, "Alias");
        if(altdev != NULL) {
            amxd_object_set_value(cstring_t, altdev, "Master", "");
        } else {
            SAH_TRACEZ_ERROR(ME, "Found broken link to alternative in device %s", key);
        }
    }

    // Remove all names from hash table
    names = amxd_object_get_child(*dev, "Names");
    amxd_object_for_each(instance, child_it, names) {
        amxd_object_t* name = amxc_llist_it_get_data(child_it, amxd_object_t, it);
        char* name_value = amxd_object_get_value(cstring_t, name, "Name", NULL);
        uint32_t name_index = amxd_object_get_value(uint32_t, name, "Suffix", NULL);
        if(!gmaps_device_name_table_remove(name_value, name_index)) {
            SAH_TRACEZ_ERROR(ME, "Failed to remove %s from hashing table", name_value);
        }
        free(name_value);
    }

    gmaps_devices_merge_delete_rules(key);

    gmaps_query_remove_all(*dev);

    gmaps_free_device(dev_internal);

exit:
    return status;
}

gmap_status_t gmaps_delete_device(amxd_object_t* devices, const char* key) {
    gmap_status_t status = gmap_status_ok;
    amxd_object_t* dev = NULL;

    when_null_status(devices, exit, status = gmap_status_invalid_parameter);
    when_str_empty_status(key, exit, status = gmap_status_invalid_key);

    status = gmaps_device_cleanup(devices, &dev, key);
    when_failed(status, exit);

    amxd_object_emit_del_inst(dev);
    amxd_object_delete(&dev);

exit:
    return status;
}

gmap_status_t gmaps_block_device(amxd_object_t* devices, const char* key) {
    gmap_status_t status = gmap_status_unknown_error;
    amxc_htable_it_t* hit = NULL;

    when_null_status(devices, exit, status = gmap_status_invalid_parameter);
    when_str_empty_status(key, exit, status = gmap_status_invalid_key);

    // Check if already blocked.
    when_not_null_status(amxc_htable_get(gmap_get_blocked_devices(), key),
                         exit,
                         status = gmap_status_ok);

    hit = calloc(1, sizeof(amxc_htable_it_t));
    amxc_htable_it_init(hit);
    amxc_htable_insert(gmap_get_blocked_devices(), key, hit);

    // If the blocked device exists, remove it.
    gmaps_delete_device(devices, key);

    status = gmap_status_ok;
exit:
    return status;
}

gmap_status_t gmaps_unblock_device(const char* key) {
    gmap_status_t status = gmap_status_unknown_error;
    amxc_htable_it_t* hit = NULL;

    when_str_empty_status(key, exit, status = gmap_status_invalid_key);

    hit = amxc_htable_take(gmap_get_blocked_devices(), key);

    // Check if already unblocked.
    when_null_status(hit,
                     exit,
                     status = gmap_status_ok);

    amxc_htable_it_clean(hit, NULL);
    free(hit);

    status = gmap_status_ok;
exit:
    return status;
}

bool gmaps_device_is_blocked(const char* key) {
    bool retval = false;

    when_str_empty_status(key, exit, retval = false);

    when_true_status(gmaps_devices_is_LLTD_probe(key),
                     exit,
                     retval = true);

    when_not_null_status(amxc_htable_get(gmap_get_blocked_devices(), key),
                         exit,
                         retval = true);

exit:
    return retval;
}

gmap_status_t gmaps_find_devices(const char* expression,
                                 uint32_t UID,
                                 uint32_t flags,
                                 amxc_var_t* ret) {
    gmap_status_t status = gmap_status_ok;
    amxp_expr_t* expr = NULL;
    amxd_object_t* device_template = gmap_dm_get_devices_device();

    when_failed_status(amxp_expr_new(&expr, expression),
                       exit,
                       status = gmap_status_invalid_expression);

    when_null_status(device_template, exit, status = gmap_status_invalid_parameter);
    when_null_status(ret, exit, status = gmap_status_invalid_parameter);

    amxc_var_set_type(ret, AMXC_VAR_ID_LIST);

    amxd_object_for_each(instance, it, device_template) {
        amxd_object_t* device = amxc_llist_it_get_data(it, amxd_object_t, it);

        //TODO add acl check (object_canRead(device, UID)) when available
        (void) UID;

        if(((flags & GMAP_INCLUDE_ALTERNATIVES) == 0) && gmaps_device_is_alternative(device)) {
            // skip alternative devices
            continue;
        }

        // check that the (master) device matches with the expression
        if(gmaps_device_matches(device, expr)) {
            // match found, add to the result list
            amxc_var_add(cstring_t,
                         ret,
                         amxd_object_get_name(device, AMXD_OBJECT_NAMED));
            // continue with next device
            continue;
        }

        // check alternatives if GMAP_INCLUDE_ALTERNATIVES flag is not set
        if((flags & GMAP_INCLUDE_ALTERNATIVES) == 0) {
            gmap_matching_devs_info_t match_info = {.matching_devs = ret, .expression = expr};
            gmaps_device_for_all_alternatives(device, gmaps_add_matching_device, &match_info);
        }
    }

exit:
    amxp_expr_delete(&expr);
    return status;
}

/**
 * The master device of @param device is returned via the parameter if it exists.
 * @return True if success, false is failed.
 */
bool gmaps_device_get_master(amxd_object_t** device) {
    bool retval = false;
    char* master = amxd_object_get_value(cstring_t, *device, "Master", NULL);
    amxd_object_t* master_device = NULL;

    when_str_empty_status(master, exit, retval = true);
    master_device = gmaps_get_device(master);
    when_null_status(master_device, exit, retval = false);
    *device = master_device;
    retval = true;
exit:
    free(master);
    return retval;
}

/**
 * @brief Get a list of devices matching expression
 *
 * Using an expression this function is searching the data model for device that are matching the expression.
 *
 * A variant list containing the detailed information of the devices is build and returned
 *
 * A master device will match the expression if the device itself matchs or if any of its alternatives are matching.
 *
 * If the @ref GMAP_INCLUDE_ALTERNATIVES flag is specified, a master will only match the provided expression if the master
 *   device itself matches. The alternative devices are added to the returned list if thay are matching the expression.
 *
 * The details of the returned information can be changed by providing the flags bitmask. Supported flags are:
 *
 * @ref GMAP_NO_DETAILS: no parameters are added in the returned result
 * @ref GMAP_NO_ACTIONS: no actions are added in the returned result
 * @ref GMAP_INCLUDE_TOPOLOGY: The topology (down direction) for each device is added in the returned result.
 * @ref GMAP_INCLUE_ALTERNATIVES: match alternative devices individually
 */
amxc_var_t* gmaps_devices_get(const char* expression, uint32_t flags) {
    amxc_var_t* devlist = NULL;
    amxc_var_t* retv = NULL;

    amxc_var_new(&devlist);
    amxc_var_set_type(devlist, AMXC_VAR_ID_LIST);

    amxc_var_new(&retv);
    amxc_var_set_type(retv, AMXC_VAR_ID_LIST);

    gmaps_find_devices(expression, 0, flags, devlist);

    amxc_var_for_each(device, devlist) {
        const char* key = amxc_var_constcast(cstring_t, device);
        amxc_var_t* newinfo = NULL;

        newinfo = amxc_var_add_new(retv);
        amxc_var_set_type(newinfo, AMXC_VAR_ID_HTABLE);

        gmaps_device_get(key, newinfo, flags);
    }


    amxc_var_delete(&devlist);

    return retv;
}

gmap_device_priv_data_t* gmaps_device_private_data(const amxd_object_t* device) {
    gmap_device_t* dev = NULL;
    gmap_device_priv_data_t* value = NULL;

    when_null(device, exit);
    dev = (gmap_device_t*) device->priv;
    when_null(dev, exit);

    value = &dev->data;

exit:
    return value;
}

amxd_object_t* GMAPS_PRIVATE gmaps_device_from_private_data(const gmap_device_priv_data_t* priv) {
    gmap_device_t* gmap_dev = NULL;
    amxd_object_t* dev = NULL;

    when_null(priv, exit);
    gmap_dev = amxc_container_of(priv, gmap_device_t, data);
    dev = gmap_dev->device;

exit:
    return dev;
}

#!/bin/sh
name="gmap-server"
name_datamodel="Devices"

case $1 in
    boot | start)
        ${name} -D
        ;;
    shutdown | stop)

        [ -f "/var/run/${name}.pid" ] && kill $(cat "/var/run/${name}.pid")
        ;;
    debuginfo)
	    ba-cli "${name_datamodel}.?"
        ;;
esac

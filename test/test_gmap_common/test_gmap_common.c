/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdarg.h>
#include <setjmp.h>
#include <cmocka.h>

#include <stdbool.h>
#include <string.h>
#include <stdio.h>

#include "../test_common/test_common.h"
#include "test_gmap_common.h"
#include "gmaps_priv.h"

void test_is_mac(GMAPS_UNUSED void** state) {
    assert_false(gmaps_common_is_mac(NULL));
    assert_false(gmaps_common_is_mac(""));
    assert_false(gmaps_common_is_mac(" 11:22:33:44:55:66"));
    assert_false(gmaps_common_is_mac("11:22:33:44:55:66 "));
    assert_false(gmaps_common_is_mac("11::22::33::44::55::66"));
    assert_false(gmaps_common_is_mac("112233445566"));
    assert_false(gmaps_common_is_mac("11 22 33 44 55 66"));
    assert_false(gmaps_common_is_mac("11:22:33:44:55"));
    assert_false(gmaps_common_is_mac("11:22:33:44:55:66:77"));
    assert_false(gmaps_common_is_mac("ab:cd:ef:ga:bc:de"));
    assert_false(gmaps_common_is_mac(":::::"));

    assert_true(gmaps_common_is_mac("ab:cd:ef:aa:bc:de"));
    assert_true(gmaps_common_is_mac("AB:CD:EF:AA:BC:DE"));
    assert_true(gmaps_common_is_mac("00:00:00:00:00:00"));
    assert_true(gmaps_common_is_mac("11:22:33:44:55:66"));
    assert_true(gmaps_common_is_mac("Ab:Cd:ef:AB:cD:EF"));
}

void test_contains_tag(GMAPS_UNUSED void** state) {
    // negative cases
    assert_false(gmaps_common_contains_tag(NULL, NULL));
    assert_false(gmaps_common_contains_tag(NULL, "hello"));
    assert_false(gmaps_common_contains_tag("hello hi", NULL));
    assert_false(gmaps_common_contains_tag("", "hi"));
    assert_false(gmaps_common_contains_tag("  ", "hi"));
    assert_false(gmaps_common_contains_tag("             ", "hi"));
    assert_false(gmaps_common_contains_tag("    ", "    "));
    assert_false(gmaps_common_contains_tag("    ", " "));
    assert_false(gmaps_common_contains_tag("hello hi", "hii"));
    assert_false(gmaps_common_contains_tag("hello hi", "hello "));
    assert_false(gmaps_common_contains_tag("hello hi", "hello hi"));
    assert_false(gmaps_common_contains_tag("hello hi", "h"));
    assert_false(gmaps_common_contains_tag("hello hi ", "hi "));
    assert_false(gmaps_common_contains_tag("hello hi", " h"));
    assert_false(gmaps_common_contains_tag("hello hi", " hi"));
    assert_false(gmaps_common_contains_tag("hello hi", "ello"));
    assert_false(gmaps_common_contains_tag("hello", "hell"));
    assert_false(gmaps_common_contains_tag("hello", "ello"));
    assert_false(gmaps_common_contains_tag("hello hi", "HI"));
    assert_false(gmaps_common_contains_tag("hello hi", "'"));
    assert_false(gmaps_common_contains_tag("hello hi", "'hi'"));
    assert_false(gmaps_common_contains_tag("hello 'hi'", "hi"));
    assert_false(gmaps_common_contains_tag("hello \"hi\"", "hi"));
    assert_false(gmaps_common_contains_tag("hello hi", "\"hi\""));
    assert_false(gmaps_common_contains_tag("empty value not present", ""));

    // positive normal cases
    assert_true(gmaps_common_contains_tag("something very normal", "something"));
    assert_true(gmaps_common_contains_tag("something very normal", "very"));
    assert_true(gmaps_common_contains_tag("something very normal", "normal"));

    // positive edge cases
    assert_true(gmaps_common_contains_tag(" spacing before and after ", "spacing"));
    assert_true(gmaps_common_contains_tag(" spacing before and after ", "after"));
    assert_true(gmaps_common_contains_tag("multiple      spaces      as     wel", "multiple"));
    assert_true(gmaps_common_contains_tag("multiple      spaces      as     wel", "spaces"));
    assert_true(gmaps_common_contains_tag("we have CaSiNg", "CaSiNg"));
    assert_true(gmaps_common_contains_tag("singleton", "singleton"));
    assert_true(gmaps_common_contains_tag("x", "x"));
}

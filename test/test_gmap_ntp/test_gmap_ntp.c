/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdarg.h>
#include <setjmp.h>
#include <cmocka.h>

#include <stdbool.h>
#include <string.h>
#include <stdio.h>

#include "test_gmap_ntp.h"
#include "../test_common/test_util.h"
#include "../test_common/test_common.h"
#include "gmaps_uuid.h"
#include "gmaps_dm_devices.h"
#include <amxut/amxut_dm.h>
#include <debug/sahtrace.h>
#include <amxut/amxut_timer.h>


int test_gmap_ntp_setup(void** state) {
    amxd_object_t* root_obj = NULL;

    amxut_bus_setup(state);

    root_obj = amxd_dm_get_root(amxut_bus_dm());
    assert_ptr_not_equal(root_obj, NULL);

    util_setup_resolver();
    amxut_dm_load_odl("%s", ODL_DEFS);
    amxut_dm_load_odl("%s", ODL_TEST_CONFIG);
    amxut_dm_load_odl("%s", ODL_NTP_DEFINITION); // Note: ntp not syncronized yet at this point.
    amxut_dm_load_odl("%s", ODL_DHCP_CLIENT);

    assert_non_null(amxb_be_who_has("Time."));
    assert_non_null(amxb_be_who_has("DHCPv4Server."));

    gmap_server_init(amxut_bus_dm(), amxut_bus_parser());

    return 0;
}

int test_gmap_ntp_teardown(void** state) {
    gmap_server_cleanup();

    amxo_parser_clean(amxut_bus_parser());
    amxd_dm_clean(amxut_bus_dm());
    amxut_bus_teardown(state);

    return 0;
}

static void s_assert_firstseen(const char* dev_name, const char* expected_firstseen) {
    amxc_var_t actual_firstseen_var;
    amxc_var_init(&actual_firstseen_var);
    amxd_object_t* dev = gmaps_get_device(dev_name);
    assert_non_null(dev);
    assert_int_equal(amxd_status_ok, amxd_object_get_param(dev, "FirstSeen", &actual_firstseen_var));
    char* actual_firstseen_char = amxc_var_dyncast(cstring_t, &actual_firstseen_var);
    assert_string_equal(actual_firstseen_char, expected_firstseen);
    free(actual_firstseen_char);
    amxc_var_clean(&actual_firstseen_var);
}

static void s_set_firstseen(const char* dev_name, const char* datetime) {
    amxc_var_t values;
    amxc_var_init(&values);
    amxc_var_set_type(&values, AMXC_VAR_ID_HTABLE);
    amxc_var_t* firstseen_param = amxc_var_add_new_key(&values, "FirstSeen");
    amxc_var_set_type(firstseen_param, AMXC_VAR_ID_TIMESTAMP);
    amxc_ts_t firstseen_ts;
    assert_int_equal(0, amxc_ts_parse(&firstseen_ts, datetime, strlen(datetime)));
    assert_int_equal(0, amxc_var_set(amxc_ts_t, firstseen_param, &firstseen_ts));

    assert_int_equal(gmap_status_ok, gmaps_device_set(dev_name, &values));

    amxc_var_clean(&values);
}

void test_firstseen_set_after_ntp_up(UNUSED void** state) {
    // GIVEN a board that has never seen NTP yet,
    //       two devices without known firstseen time (because no ntp)
    //       and a device with known firstseen time.
    amxut_timer_go_to_datetime_str("2019-01-01T11:12:13Z");
    util_create_device(TESTKEY, "default", "self and hgw", false, "selectednamedevice", amxd_status_ok, NULL);
    util_create_device(TEST2KEY, "default", "self and hgw", false, "selectednamedevice", amxd_status_ok, NULL);
    util_create_device(TEST3KEY, "default", "self and hgw", false, "selectednamedevice", amxd_status_ok, NULL);
    s_assert_firstseen(TESTKEY, "0001-01-01T00:00:00Z");
    s_assert_firstseen(TEST2KEY, "0001-01-01T00:00:00Z");
    s_assert_firstseen(TEST3KEY, "0001-01-01T00:00:00Z");

    // WHEN the NTP server comes up
    amxut_timer_go_to_datetime_str("2024-05-06T07:08:09Z");
    amxut_dm_load_odl(ODL_NTP_SYNCRONIZED);
    amxut_bus_handle_events();

    // THEN the device's firstseen date becomes the current time
    s_assert_firstseen(TESTKEY, "2024-05-06T07:08:09Z");
    s_assert_firstseen(TEST2KEY, "2024-05-06T07:08:09Z");
    s_assert_firstseen(TEST3KEY, "2024-05-06T07:08:09Z");
}

void test_firstseen_after_ntp_toggle(UNUSED void** state) {
    // GIVEN a board that has NTP seen, and some devices.
    amxut_timer_go_to_datetime_str("2019-01-01T11:12:01Z");
    amxut_dm_load_odl(ODL_NTP_SYNCRONIZED);
    amxut_timer_go_to_datetime_str("2019-01-01T11:13:01Z");
    util_create_device(TESTKEY, "default", "self and hgw", false, "selectednamedevice", amxd_status_ok, NULL);
    amxut_timer_go_to_datetime_str("2019-01-01T11:14:10Z");
    util_create_device(TEST3KEY, "default", "self and hgw", false, "selectednamedevice", amxd_status_ok, NULL);

    // WHEN NTP status toggles (goes down and up again)
    amxut_dm_load_odl("time-unsynchronized.odl");
    amxut_bus_handle_events();
    amxut_dm_load_odl(ODL_NTP_SYNCRONIZED);
    amxut_bus_handle_events();

    // THEN the devices kept their firstseen time
    s_assert_firstseen(TESTKEY, "2019-01-01T11:13:01Z");
    s_assert_firstseen(TEST3KEY, "2019-01-01T11:14:10Z");
}

void test_firstseen_mix(UNUSED void** state) {
    // GIVEN ntp not syncronized
    // AND GIVEN a device with zero FirstSeen time (because ntp not syncronized)
    amxut_timer_go_to_datetime_str("2019-01-01T11:12:01Z");
    util_create_device(TESTKEY, "default", "self and hgw", false, "selectednamedevice", amxd_status_ok, NULL);

    // AND GIVEN a device with a set FirstSeen time (for example restored from backup)
    amxut_timer_go_to_datetime_str("2019-01-01T11:14:10Z");
    util_create_device(TEST3KEY, "default", "self and hgw", false, "selectednamedevice", amxd_status_ok, NULL);
    s_set_firstseen(TEST3KEY, "2018-01-18T18:18:18Z");

    // WHEN ntp comes up
    amxut_timer_go_to_datetime_str("2026-01-01T11:14:10Z");
    amxut_dm_load_odl(ODL_NTP_SYNCRONIZED);
    amxut_bus_handle_events();

    // THEN the device with zero FirstSeen gets the current time
    s_assert_firstseen(TESTKEY, "2026-01-01T11:14:10Z");
    // AND THEN the device with the "custom" FirstSeen time, keeps that time.
    s_assert_firstseen(TEST3KEY, "2018-01-18T18:18:18Z");
}
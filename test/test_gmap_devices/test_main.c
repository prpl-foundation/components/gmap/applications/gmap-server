/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdarg.h>
#include <setjmp.h>
#include <cmocka.h>

#include "test_devices.h"

int main(void) {
    const struct CMUnitTest tests_shared_setup[] = {
        /**
         * devices_create_destroy tests
         */
        cmocka_unit_test(test_rpc_can_create_device_first),
        cmocka_unit_test(test_rpc_can_create_device_second),
        cmocka_unit_test(test_rpc_can_create_device_third),
        cmocka_unit_test(test_rpc_can_update_tag_of_existing_device),
        cmocka_unit_test(test_rpc_cannot_update_restricted_tag_of_existing_device),
        cmocka_unit_test(test_rpc_can_create_device_temp),
        cmocka_unit_test(test_rpc_can_create_device_extra),
        cmocka_unit_test(test_internal_create_device_input_validation),

        cmocka_unit_test(test_internal_destroy_device),
        cmocka_unit_test(test_rpc_can_destroy_device),
        // test_del_can_destroy_device_on_name is in `tests_separate_setup` below
        // test_del_can_destroy_device_on_index is in `tests_separate_setup` below
        cmocka_unit_test(test_rpc_can_destroy_device_removes_ipv4_lease),
        cmocka_unit_test(test_rpc_can_destroy_device_removes_ipv6_lease),
        cmocka_unit_test(test_rpc_can_destroy_device_removes_both_leases),
        cmocka_unit_test(test_rpc_lease_not_removed_empty_mac),
        cmocka_unit_test(test_internal_destroy_device_input_validation),
        cmocka_unit_test(test_internal_destroy_device_self),
        cmocka_unit_test(test_internal_destroy_device_self_hgw),
        cmocka_unit_test(test_rpc_refuse_create_device_with_invalid_key),
        // For new tests, please add them under `tests_separate_setup` instead of here, and use
        // cmocka_unit_test_setup_teardown to keep tests independent of each other and maintainable.

        /**
         * devices_find tests
         */
        cmocka_unit_test(test_rpc_can_find_device_by_parameter),
        cmocka_unit_test(test_rpc_cannot_find_device_by_parameter),
        cmocka_unit_test(test_rpc_cannot_find_device_by_nonexisting_parameter_true),
        cmocka_unit_test(test_rpc_cannot_find_device_by_nonexisting_parameter_false),
        cmocka_unit_test(test_rpc_can_find_device_by_parameter_htable),
        cmocka_unit_test(test_rpc_can_find_device_by_tag),
        cmocka_unit_test(test_rpc_can_find_devices_by_tag),
        cmocka_unit_test(test_rpc_can_find_device_by_tags),
        cmocka_unit_test(test_internal_find_device_input_validation),
        // For new tests, please add them under `tests_separate_setup` instead of here, and use
        // cmocka_unit_test_setup_teardown to keep tests independent of each other and maintainable.

        /**
         * devices_link tests
         */
        cmocka_unit_test(test_chosenlink_can_link_devices),
        cmocka_unit_test(test_chosenlink_can_link_devices_again_same),
        cmocka_unit_test(test_chosenlink_can_replace_link),
        cmocka_unit_test(test_chosenlink_can_link_devices_using_linkcmd),
        cmocka_unit_test(test_chosenlink_link_devices_input_validation),
        cmocka_unit_test(test_chosenlink_internal_link_devices_input_validation),
        cmocka_unit_test(test_chosenlink_remove_all_upper_links),
        cmocka_unit_test(test_chosenlink_remove_all_lower_links),
        // For new tests, please add them under `tests_separate_setup` instead of here, and use
        // cmocka_unit_test_setup_teardown to keep tests independent of each other and maintainable.

        /**
         * devices_notify tests
         */
        cmocka_unit_test(test_rpc_can_send_notify),
        // For new tests, please add them under `tests_separate_setup` instead of here, and use
        // cmocka_unit_test_setup_teardown to keep tests independent of each other and maintainable.

        /**
         * device_name_table tests
         */
        cmocka_unit_test(test_internal_can_add_names_to_table),
        cmocka_unit_test(test_internal_can_add_names_to_table_existing),
        cmocka_unit_test(test_internal_can_get_first_available_index),
        cmocka_unit_test(test_internal_can_get_index_available),
        cmocka_unit_test(test_internal_can_remove_names_from_table),
        // For new tests, please add them under `tests_separate_setup` instead of here, and use
        // cmocka_unit_test_setup_teardown to keep tests independent of each other and maintainable.

        /**
         * device_name tests
         */
        cmocka_unit_test(test_rpc_can_set_name),
        cmocka_unit_test(test_rpc_cannot_set_duplicate_name),
        cmocka_unit_test(test_rpc_can_set_duplicate_name_in_self_device),
        cmocka_unit_test(test_rpc_can_set_duplicate_name_in_upnp_device),
        cmocka_unit_test(test_internal_set_name_input_validation),
        cmocka_unit_test(test_rpc_can_remove_name),
        // For new tests, please add them under `tests_separate_setup` instead of here, and use
        // cmocka_unit_test_setup_teardown to keep tests independent of each other and maintainable.

        /**
         * device default name generation tests
         */
        cmocka_unit_test(test_rpc_create_devices_with_default_names),
        cmocka_unit_test(test_rpc_create_devices_without_default_names),
        // For new tests, please add them under `tests_separate_setup` instead of here, and use
        // cmocka_unit_test_setup_teardown to keep tests independent of each other and maintainable.

        /**
         * device  name selection tests
         */
        cmocka_unit_test(test_init_select_name),
        cmocka_unit_test(test_rpc_select_name),
        cmocka_unit_test(test_select_name_adds_correct_sufix),
        cmocka_unit_test(test_select_name_cleanup),
        // For new tests, please add them under `tests_separate_setup` instead of here, and use
        // cmocka_unit_test_setup_teardown to keep tests independent of each other and maintainable.

        /**
         * devices_block tests
         */
        cmocka_unit_test(test_rpc_can_block_unblock_device),
        cmocka_unit_test(test_rpc_can_block_device_already_blocked),
        cmocka_unit_test(test_rpc_can_unblock_device_already_unblocked),
        cmocka_unit_test(test_rpc_LLTD_device_is_blocked),
        cmocka_unit_test(test_internal_block_device_input_validation),
        cmocka_unit_test(test_internal_unblock_device_input_validation),
        cmocka_unit_test(test_internal_is_blocked_device_input_validation),
        // For new tests, please add them under `tests_separate_setup` instead of here, and use
        // cmocka_unit_test_setup_teardown to keep tests independent of each other and maintainable.

        /**
         * device_set tests
         */
        cmocka_unit_test(test_rpc_cannot_set_parameter_active_false),
        cmocka_unit_test(test_rpc_cannot_set_parameter_active_true),
        cmocka_unit_test(test_rpc_can_set_parameter_name),
        cmocka_unit_test(test_rpc_can_set_parameter_name_null),
        cmocka_unit_test(test_rpc_cannot_set_parameter_key),
        cmocka_unit_test(test_rpc_can_set_parameters_unfiltered),
        cmocka_unit_test(test_rpc_can_set_parameters_unfiltered_recursive),
        cmocka_unit_test(test_internal_set_input_validation),
        // For new tests, please add them under `tests_separate_setup` instead of here, and use
        // cmocka_unit_test_setup_teardown to keep tests independent of each other and maintainable.

        /**
         *  Run tests for tags
         */
        cmocka_unit_test(test_gmap_can_device_set_tag),
        cmocka_unit_test(test_gmap_cannot_set_tags_after_creation),
        cmocka_unit_test(test_gmap_duplicate_tags_are_ignored),
        cmocka_unit_test(test_gmap_can_invoke_device_set_tag),
        cmocka_unit_test(test_gmap_can_device_has_tag),
        cmocka_unit_test(test_gmap_can_invoke_device_has_tag),
        cmocka_unit_test(test_gmap_can_clear_tag),
        cmocka_unit_test(test_gmap_can_invoke_clear_tag),
        cmocka_unit_test(test_gmap_set_tag__mib_activate),
        // For new tests, please add them under `tests_separate_setup` instead of here, and use
        // cmocka_unit_test_setup_teardown to keep tests independent of each other and maintainable.

        /**
         *  Run tests for functions
         */
        cmocka_unit_test(test_gmap_device_set_function),
        cmocka_unit_test(test_gmap_execute_device_function),
        cmocka_unit_test(test_gmap_execute_device_subobject_function),
        cmocka_unit_test(test_gmap_device_remove_function),
        // For new tests, please add them under `tests_separate_setup` instead of here, and use
        // cmocka_unit_test_setup_teardown to keep tests independent of each other and maintainable.

        /**
         * Tests for devices get
         */
        cmocka_unit_test(test_gmap_can_devices_get),
        cmocka_unit_test(test_gmap_can_devices_get_custom_ops),
        cmocka_unit_test(test_gmap_can_devices_get_result),
        cmocka_unit_test(test_gmap_devices_get_matching_mac),
        cmocka_unit_test(test_gmap_can_invoke_devices_get),
        // For new tests, please add them under `tests_separate_setup` instead of here, and use
        // cmocka_unit_test_setup_teardown to keep tests independent of each other and maintainable.

        /**
         * Tests for device get
         */
        cmocka_unit_test(test_gmap_can_device_get),
        cmocka_unit_test(test_gmap_can_device_get_result),
        cmocka_unit_test(test_gmap_can_invoke_device_get),

        cmocka_unit_test(test_gmap_can_alternative_wrong_parameters),
        cmocka_unit_test(test_gmap_can_device_set_alternative),
        cmocka_unit_test(test_gmap_can_device_remove_alternative),
        cmocka_unit_test(test_gmap_can_invoke_set_alternative),
        cmocka_unit_test(test_gmap_can_invoke_remove_alternatives),
        // For new tests, please add them under `tests_separate_setup` instead of here, and use
        // cmocka_unit_test_setup_teardown to keep tests independent of each other and maintainable.

        /**
         * Query tests
         */
        cmocka_unit_test(test_can_open_query),
        cmocka_unit_test(test_can_match_query),
        cmocka_unit_test(test_can_close_query),
        cmocka_unit_test(test_rpc_query),
        cmocka_unit_test(test_query_cleanup),
        // For new tests, please add them under `tests_separate_setup` instead of here, and use
        // cmocka_unit_test_setup_teardown to keep tests independent of each other and maintainable.

        /**
         * Device topology tests
         */
        cmocka_unit_test(test_topology_traverse_down),
        cmocka_unit_test(test_topology_traverse_up),
        cmocka_unit_test(test_topology_match_expression),
        cmocka_unit_test(test_topology_defaults),
        cmocka_unit_test(test_topology_traverse_failed),
        // For new tests, please add them under `tests_separate_setup` instead of here, and use
        // cmocka_unit_test_setup_teardown to keep tests independent of each other and maintainable.

        /**
         * Device get_parameters, get_first_parameter tests
         */
        cmocka_unit_test(test_first_parameter_traverse),
        cmocka_unit_test(test_first_parameter_subobject_parameter),
        cmocka_unit_test(test_first_parameter_traverse_failed),
        cmocka_unit_test(test_get_parameters_traverse_down),
        cmocka_unit_test(test_get_parameters_traverse_failed),
        // For new tests, please add them under `tests_separate_setup` instead of here, and use
        // cmocka_unit_test_setup_teardown to keep tests independent of each other and maintainable.

        /*
         * Devices Add and Remove actions tests
         */
        cmocka_unit_test(test_add_action),
        cmocka_unit_test(test_add_second_action),
        cmocka_unit_test(test_action_already_exists),
        cmocka_unit_test(test_remove_action),
        cmocka_unit_test(test_remove_second_action),
        // For new tests, please add them under `tests_separate_setup` instead of here, and use
        // cmocka_unit_test_setup_teardown to keep tests independent of each other and maintainable.
    };

    // Tests with setup and teardown per tests instead of for a group of tests.
    //
    // This has the advantage that one test does not change the behaviour of other tests, so:
    // - it's much easier to diagnose problems (you only have to look at one tests, not at all
    //   tests above)
    // - it's much easier to change a test (if you change one test you don't have to adapt all tests
    //   below).
    const struct CMUnitTest tests_separate_setup[] = {
        cmocka_unit_test_setup_teardown(test_alternatives_are_cleaned_for_destroyed_devices_alternative, test_gmap_setup, test_gmap_teardown),
        cmocka_unit_test_setup_teardown(test_alternatives_are_cleaned_for_destroyed_devices_master, test_gmap_setup, test_gmap_teardown),

        /*
         * Remove devices using _del()
         */
        cmocka_unit_test_setup_teardown(test_del_can_destroy_device_on_name, test_gmap_setup, test_gmap_teardown),
        cmocka_unit_test_setup_teardown(test_del_can_destroy_device_on_index, test_gmap_setup, test_gmap_teardown),

        /**
         * Query tests
         */
        cmocka_unit_test_setup_teardown(test_query_notification_after_device_creation, test_gmap_setup, test_gmap_teardown),
        cmocka_unit_test_setup_teardown(test_query_no_notification_after_irrelevant_device_creation, test_gmap_setup, test_gmap_teardown),
        cmocka_unit_test_setup_teardown(test_query_notification_after_device_stops_matching, test_gmap_setup, test_gmap_teardown),
        cmocka_unit_test_setup_teardown(test_query_no_notification_after_irrelevant_device_stops_matching, test_gmap_setup, test_gmap_teardown),
        cmocka_unit_test_setup_teardown(test_query_notification_after_existing_device_starts_matching_in_tag, test_gmap_setup, test_gmap_teardown),
        cmocka_unit_test_setup_teardown(test_query_notification_after_existing_device_starts_matching_in_parameter_via_rpc, test_gmap_setup, test_gmap_teardown),
        cmocka_unit_test_setup_teardown(test_query_notification_after_existing_device_starts_matching_in_parameter_via_transaction, test_gmap_setup, test_gmap_teardown),
        cmocka_unit_test_setup_teardown(test_query_notification_when_device_updated, test_gmap_setup, test_gmap_teardown),
        cmocka_unit_test_setup_teardown(test_query_no_notification_when_ignoring_device_updated, test_gmap_setup, test_gmap_teardown),

        /*
         * Remove inactive devices test
         */
        cmocka_unit_test_setup_teardown(test_remove_inactive_devices, test_gmap_setup, test_gmap_teardown),
        cmocka_unit_test_setup_teardown(test_not_remove_inactive_device_before_time, test_gmap_setup, test_gmap_teardown),
        cmocka_unit_test_setup_teardown(test_remove_all_inactive_devices, test_gmap_setup, test_gmap_teardown),
        cmocka_unit_test_setup_teardown(test_cannot_remove_protected_devices, test_gmap_setup, test_gmap_teardown),

        /*
         * topology
         */
        cmocka_unit_test_setup_teardown(test_topology_alternatives, test_gmap_setup, test_gmap_teardown),

        /*
         * linking while tracking datasource
         */
        cmocka_unit_test_setup_teardown(test_multilink_rpc_linkAdd, test_gmap_setup, test_gmap_teardown),
        cmocka_unit_test_setup_teardown(test_multilink_rpc_linkAdd_fail, test_gmap_setup, test_gmap_teardown),
        cmocka_unit_test_setup_teardown(test_multilink_rpc_linkAdd_invalid_args, test_gmap_setup, test_gmap_teardown),
        cmocka_unit_test_setup_teardown(test_multilink_on_device_delete, test_gmap_setup, test_gmap_teardown),
        cmocka_unit_test_setup_teardown(test_multilink_rpc_linkAdd_twice, test_gmap_setup, test_gmap_teardown),
        cmocka_unit_test_setup_teardown(test_multilink_rpc_linkReplace, test_gmap_setup, test_gmap_teardown),
        cmocka_unit_test_setup_teardown(test_multilink_rpc_linkReplace_fail, test_gmap_setup, test_gmap_teardown),
        cmocka_unit_test_setup_teardown(test_multilink_rpc_linkReplace_invalid_args, test_gmap_setup, test_gmap_teardown),
        cmocka_unit_test_setup_teardown(test_multilink_rpc_linkRemove, test_gmap_setup, test_gmap_teardown),
        cmocka_unit_test_setup_teardown(test_multilink_rpc_linkRemove_twice, test_gmap_setup, test_gmap_teardown),
        cmocka_unit_test_setup_teardown(test_multilink_rpc_linkRemove__multiple_sources, test_gmap_setup, test_gmap_teardown),
        cmocka_unit_test_setup_teardown(test_multilink_rpc_linkRemove_removeAllUpper, test_gmap_setup, test_gmap_teardown),
        cmocka_unit_test_setup_teardown(test_multilink_rpc_linkRemove_removeAllLower, test_gmap_setup, test_gmap_teardown),
        cmocka_unit_test_setup_teardown(test_multilink_rpc_linkRemove_invalid_args, test_gmap_setup, test_gmap_teardown),

        /*
         * Devices merge tests
         */
        cmocka_unit_test_setup_teardown(test_gmap_alternative_wrong_api_parameters, test_gmap_setup, test_gmap_teardown),
        cmocka_unit_test_setup_teardown(test_gmap_add_one_offset_merge_rule, test_gmap_setup, test_gmap_teardown),
        cmocka_unit_test_setup_teardown(test_gmap_add_one_filter_merge_rule, test_gmap_setup, test_gmap_teardown),
        cmocka_unit_test_setup_teardown(test_gmap_add_one_address_merge_rule, test_gmap_setup, test_gmap_teardown),
        cmocka_unit_test_setup_teardown(test_gmap_add_one_filter_offset_merge_rule, test_gmap_setup, test_gmap_teardown),
        cmocka_unit_test_setup_teardown(test_gmap_add_one_filter_righshift_merge_rule, test_gmap_setup, test_gmap_teardown),
        cmocka_unit_test_setup_teardown(test_gmap_add_one_filter_leftshift_merge_rule, test_gmap_setup, test_gmap_teardown),
        cmocka_unit_test_setup_teardown(test_gmap_add_one_offset_merge_rule_alternative_link_swap, test_gmap_setup, test_gmap_teardown),
        cmocka_unit_test_setup_teardown(test_gmap_add_offset_rule_already_alternative, test_gmap_setup, test_gmap_teardown),
        cmocka_unit_test_setup_teardown(test_gmap_add_offset_rule_already_master, test_gmap_setup, test_gmap_teardown),
        cmocka_unit_test_setup_teardown(test_gmap_add_offset_rule_before_alternative, test_gmap_setup, test_gmap_teardown),
        cmocka_unit_test_setup_teardown(test_gmap_destroy_device_ongoing_offset_rule, test_gmap_setup, test_gmap_teardown),
        cmocka_unit_test_setup_teardown(test_gmap_delete_ongoing_offset_rule_2, test_gmap_setup, test_gmap_teardown),
        cmocka_unit_test_setup_teardown(test_gmap_can_invoke_add_alternative_rules, test_gmap_setup, test_gmap_teardown),

        /**
         * devices_traverse tests
         */
        cmocka_unit_test_setup_teardown(test_rpc_can_create_tree0, test_gmap_setup, test_gmap_teardown),
        cmocka_unit_test_setup_teardown(test_rpc_can_create_tree1, test_gmap_setup, test_gmap_teardown),
        cmocka_unit_test_setup_teardown(test_internal_traverse_tree0_dev0_all_match, test_gmap_setup, test_gmap_teardown),
        cmocka_unit_test_setup_teardown(test_internal_traverse_tree0_dev0_all_match_early_stop, test_gmap_setup, test_gmap_teardown),
        cmocka_unit_test_setup_teardown(test_internal_traverse_tree0_dev0_this_early_stop, test_gmap_setup, test_gmap_teardown),
        cmocka_unit_test_setup_teardown(test_internal_traverse_tree0_dev0_early_stop_at_push_cb, test_gmap_setup, test_gmap_teardown),
        cmocka_unit_test_setup_teardown(test_internal_traverse_tree1_recursive, test_gmap_setup, test_gmap_teardown),
        cmocka_unit_test_setup_teardown(test_internal_traverse_input_validation, test_gmap_setup, test_gmap_teardown),
        cmocka_unit_test_setup_teardown(test_internal_traverse_mode_string_conversions, test_gmap_setup, test_gmap_teardown),

        /**
         * devices_query tests
         */
        cmocka_unit_test_setup_teardown(test_query_notification_after_set_alternative, test_gmap_setup, test_gmap_teardown),
        cmocka_unit_test_setup_teardown(test_query_notification_after_remove_alternative, test_gmap_setup, test_gmap_teardown),
    };

    int failed_tests = 0;
    failed_tests += cmocka_run_group_tests(tests_separate_setup, NULL, NULL);
    failed_tests += cmocka_run_group_tests(tests_shared_setup, test_gmap_setup, test_gmap_teardown);
    if(failed_tests != 0) {
        printf("FAILED TESTS: %d\n", failed_tests);
    }


    return failed_tests;


}

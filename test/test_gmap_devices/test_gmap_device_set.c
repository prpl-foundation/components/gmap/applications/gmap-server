/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <setjmp.h>
#include <cmocka.h>

#include "test_devices.h"
#include "gmaps_dm_device.h"
#include "gmaps_device_set.h"


void test_rpc_cannot_set_parameter_active_false(GMAPS_UNUSED void** state) {
    amxc_var_t args;
    amxc_var_t ret;
    amxc_var_t* parameters = NULL;
    amxd_status_t status = amxd_status_ok;
    amxc_var_init(&args);
    amxc_var_init(&ret);

    // GIVEN a device with Active=true
    amxd_object_t* device = gmaps_get_device(TESTKEY);
    assert_non_null(device);
    amxd_object_set_bool(device, "Active", true);
    assert_true(amxd_object_get_value(bool, device, "Active", NULL));

    // WHEN using the "set" rpc to manipulate "Active" field directly (instead of "setActive()")
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    parameters = amxc_var_add_key(amxc_htable_t, &args, "parameters", NULL);
    amxc_var_add_key(bool, parameters, "Active", false);
    status = amxd_object_invoke_function(device,
                                         "set",
                                         &args,
                                         &ret),

    // THEN this was refused.
    assert_int_not_equal(status, amxd_status_ok);
    // readback, verify that it has not changed:
    assert_true(amxd_object_get_value(bool, device, "Active", NULL));

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_rpc_cannot_set_parameter_active_true(GMAPS_UNUSED void** state) {
    amxc_var_t args;
    amxc_var_t ret;
    amxc_var_t* parameters = NULL;
    amxd_status_t status = amxd_status_ok;
    amxc_var_init(&args);
    amxc_var_init(&ret);

    // GIVEN a device with Active=false
    amxd_object_t* device = gmaps_get_device(TESTKEY);
    assert_non_null(device);
    amxd_object_set_bool(device, "Active", false);
    assert_false(amxd_object_get_value(bool, device, "Active", NULL));

    // WHEN using the "set" rpc to manipulate "Active" field directly (instead of "setActive()")
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    parameters = amxc_var_add_key(amxc_htable_t, &args, "parameters", NULL);
    amxc_var_add_key(bool, parameters, "Active", true);
    status = amxd_object_invoke_function(device,
                                         "set",
                                         &args,
                                         &ret),

    // THEN this was refused.
    assert_int_not_equal(status, amxd_status_ok);
    // readback, verify that it has not changed:
    assert_false(amxd_object_get_value(bool, device, "Active", NULL));

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_rpc_can_set_parameter_name(GMAPS_UNUSED void** state) {
    amxd_object_t* devices = amxd_dm_findf(amxut_bus_dm(), "Devices");
    amxd_object_t* device_instance = gmaps_get_device(TEST2KEY);
    amxc_var_t args;
    amxc_var_t ret;
    amxc_var_t* parameters = NULL;

    assert_ptr_not_equal(devices, NULL);
    assert_ptr_not_equal(device_instance, NULL);

    amxc_var_init(&args);
    amxc_var_init(&ret);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    parameters = amxc_var_add_key(amxc_htable_t, &args, "parameters", NULL);
    amxc_var_add_key(cstring_t, parameters, "Name", "myNameViaSet");

    assert_int_equal(amxd_object_invoke_function(device_instance,
                                                 "set",
                                                 &args,
                                                 &ret),
                     amxd_status_ok);

    // readback
    amxd_object_t* source_obj = amxd_object_findf(device_instance, "%s", "Names.webui");
    char* name_read = amxd_object_get_value(cstring_t,
                                            source_obj,
                                            "Name",
                                            NULL);
    char* source_read = amxd_object_get_value(cstring_t,
                                              source_obj,
                                              "Source",
                                              NULL);
    assert_string_equal(name_read, "myNameViaSet");
    assert_string_equal(source_read, "webui");
    char* suffix = amxd_object_get_value(cstring_t,
                                         source_obj,
                                         "Suffix",
                                         NULL);
    assert_string_equal(suffix, "");
    free(suffix);

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    free(name_read);
    free(source_read);
}

void test_rpc_can_set_parameter_name_null(GMAPS_UNUSED void** state) {
    amxd_object_t* devices = amxd_dm_findf(amxut_bus_dm(), "Devices");
    amxd_object_t* device_instance = gmaps_get_device(TEST2KEY);
    amxc_var_t args;
    amxc_var_t ret;
    amxc_var_t* parameters = NULL;
    amxc_var_t* name = NULL;

    assert_ptr_not_equal(devices, NULL);
    assert_ptr_not_equal(device_instance, NULL);

    amxc_var_init(&args);
    amxc_var_init(&ret);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    parameters = amxc_var_add_key(amxc_htable_t, &args, "parameters", NULL);
    name = amxc_var_add_key(cstring_t, parameters, "Name", "madeUpName");
    amxc_var_clean(name); // make it a NULL variant

    assert_int_equal(amxd_object_invoke_function(device_instance,
                                                 "set",
                                                 &args,
                                                 &ret),
                     amxd_status_ok);

    // readback, should read the previously set name
    amxd_object_t* source_obj = amxd_object_findf(device_instance, "%s", "Names.webui");
    char* name_read = amxd_object_get_value(cstring_t,
                                            source_obj,
                                            "Name",
                                            NULL);
    char* source_read = amxd_object_get_value(cstring_t,
                                              source_obj,
                                              "Source",
                                              NULL);
    assert_string_equal(name_read, "myNameViaSet");
    assert_string_equal(source_read, "webui");
    char* suffix = amxd_object_get_value(cstring_t,
                                         source_obj,
                                         "Suffix",
                                         NULL);
    assert_string_equal(suffix, "");
    free(suffix);

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    free(name_read);
    free(source_read);
}

void test_rpc_cannot_set_parameter_key(GMAPS_UNUSED void** state) {
    amxd_object_t* devices = amxd_dm_findf(amxut_bus_dm(), "Devices");
    amxd_object_t* device_instance = gmaps_get_device(TEST2KEY);
    amxc_var_t args;
    amxc_var_t ret;
    amxc_var_t* parameters = NULL;

    assert_ptr_not_equal(devices, NULL);
    assert_ptr_not_equal(device_instance, NULL);

    amxc_var_init(&args);
    amxc_var_init(&ret);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    parameters = amxc_var_add_key(amxc_htable_t, &args, "parameters", NULL);
    amxc_var_add_key(cstring_t, parameters, "Key", "madeUpKey");

    assert_int_equal(amxd_object_invoke_function(device_instance,
                                                 "set",
                                                 &args,
                                                 &ret),
                     amxd_status_ok);

    // readback
    char* key_read = amxd_object_get_value(cstring_t,
                                           device_instance,
                                           "Key",
                                           NULL);
    assert_string_equal(key_read, TEST2KEY);

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    free(key_read);
}

void test_rpc_can_set_parameters_unfiltered(GMAPS_UNUSED void** state) {
    amxd_object_t* devices = amxd_dm_findf(amxut_bus_dm(), "Devices");
    amxd_object_t* device_instance = gmaps_get_device(TEST2KEY);
    amxc_var_t args;
    amxc_var_t ret;
    amxc_var_t* parameters = NULL;

    assert_ptr_not_equal(devices, NULL);
    assert_ptr_not_equal(device_instance, NULL);

    amxc_var_init(&args);
    amxc_var_init(&ret);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    parameters = amxc_var_add_key(amxc_htable_t, &args, "parameters", NULL);
    amxc_var_add_key(cstring_t, parameters, "DiscoverySource", "DSViaSet");
    amxc_var_add_key(ssv_string_t, parameters, "Tags", "tag1ViaSet tag2ViaSet");
    amxc_var_add_key(cstring_t, parameters, "madeUpParameter", "madeUpValue");

    assert_int_equal(amxd_object_invoke_function(device_instance,
                                                 "set",
                                                 &args,
                                                 &ret),
                     amxd_status_ok);

    // readback
    char* p1_read = amxd_object_get_value(cstring_t,
                                          device_instance,
                                          "DiscoverySource",
                                          NULL);
    char* p2_read = amxd_object_get_value(cstring_t,
                                          device_instance,
                                          "Tags",
                                          NULL);
    char* p3_read = amxd_object_get_value(cstring_t,
                                          device_instance,
                                          "madeUpParameter",
                                          NULL);
    assert_string_equal(p1_read, "DSViaSet");
    assert_string_equal(p2_read, "tag1ViaSet tag2ViaSet");
    assert_null(p3_read);

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    free(p1_read);
    free(p2_read);
}

void test_rpc_can_set_parameters_unfiltered_recursive(GMAPS_UNUSED void** state) {
    amxd_object_t* devices = amxd_dm_findf(amxut_bus_dm(), "Devices");
    amxd_object_t* device_instance = gmaps_get_device(TEST2KEY);
    amxc_var_t args;
    amxc_var_t ret;
    amxc_var_t* parameters = NULL;
    amxc_var_t* alternatives = NULL;

    assert_ptr_not_equal(devices, NULL);
    assert_ptr_not_equal(device_instance, NULL);

    amxc_var_init(&args);
    amxc_var_init(&ret);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    parameters = amxc_var_add_key(amxc_htable_t,
                                  &args,
                                  "parameters",
                                  NULL);
    alternatives = amxc_var_add_key(amxc_htable_t,
                                    parameters,
                                    "Alternative",
                                    NULL);
    amxc_var_add_key(amxc_htable_t,
                     parameters,
                     "madeUpTemplate",
                     NULL);
    amxc_var_add_key(amxc_htable_t,
                     alternatives,
                     TESTKEY,
                     NULL);
    //amxc_var_add_key(cstring_t, alternatives, TESTKEY, NULL);

    assert_int_equal(amxd_object_invoke_function(device_instance,
                                                 "set",
                                                 &args,
                                                 &ret),
                     amxd_status_ok);

#if 0
    // readback
    char* p1_read = amxd_object_get_value(cstring_t,
                                          device_instance,
                                          "DiscoverySource",
                                          NULL);
    char* p2_read = amxd_object_get_value(cstring_t,
                                          device_instance,
                                          "Tags",
                                          NULL);
    char* p3_read = amxd_object_get_value(cstring_t,
                                          device_instance,
                                          "madeUpParameter",
                                          NULL);
    assert_string_equal(p1_read, "DSViaSet");
    assert_string_equal(p2_read, "tag1ViaSet tag2ViaSet");
    assert_null(p3_read);

#endif
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_internal_set_input_validation(GMAPS_UNUSED void** state) {
    amxc_var_t values;
    amxc_var_init(&values);
    amxc_var_set_type(&values, AMXC_VAR_ID_HTABLE);

    assert_int_equal(gmaps_device_set(NULL, &values),
                     gmap_status_invalid_key);
    assert_int_equal(gmaps_device_set("", &values),
                     gmap_status_invalid_key);
    assert_int_equal(gmaps_device_set("madeUpKey", &values),
                     gmap_status_device_not_found);
    assert_int_equal(gmaps_device_set("myKey", NULL),
                     gmap_status_invalid_parameter);

    amxc_var_set_type(&values, AMXC_VAR_ID_NULL);
    assert_int_equal(gmaps_device_set("myKey", &values),
                     gmap_status_invalid_parameter);

    amxc_var_clean(&values);
}
/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <setjmp.h>
#include <cmocka.h>

#include "test_devices.h"
#include "gmaps_dm_device.h"
#include "../test_common/test_util.h"

void util_chosenlink_link_devices(const char* upper_key, const char* lower_key, bool new_link);

static char* new_device_or_get_key(const char* mac, const char* tags, bool exist) {
    gmap_status_t status = gmap_status_unknown_error;
    bool already_exists = false;
    char* key = NULL;
    status = gmaps_uuid_new_device_or_get_key(mac,
                                              NULL, tags, false, NULL, NULL, &key, &already_exists);
    assert_true(status == gmap_status_ok);
    assert_true(already_exists == exist);
    assert_non_null(key);
    assert_string_not_equal(key, "");
    return key;
}

void test_gmap_alternative_wrong_api_parameters(GMAPS_UNUSED void** state) {

    amxc_var_t valid_rules;
    amxc_var_t invalid_rules;
    char* valid_key1 = NULL;

    amxc_var_init(&valid_rules);
    amxc_var_set_type(&valid_rules, AMXC_VAR_ID_LIST);
    amxc_var_add(uint8_t, &valid_rules, 1);

    amxc_var_init(&invalid_rules);
    amxc_var_set_type(&invalid_rules, AMXC_VAR_ID_CSTRING);

    valid_key1 = new_device_or_get_key("01:01:02:03:04:05", "mac", false);

    handle_events();

    assert_false(gmaps_devices_merge_add_rules(NULL, &valid_rules));
    assert_false(gmaps_devices_merge_add_rules("test3Key", &valid_rules));

    assert_false(gmaps_devices_merge_add_rules(valid_key1, NULL));
    assert_false(gmaps_devices_merge_add_rules(valid_key1, &invalid_rules));

    assert_false(gmaps_devices_merge_delete_rules(NULL));

    assert_false(gmaps_devices_merge_execute_rules(NULL));

    amxc_var_clean(&valid_rules);
    amxc_var_clean(&invalid_rules);
    free(valid_key1);

}

void test_gmap_add_one_address_merge_rule(GMAPS_UNUSED void** state) {
    char* valid_key1 = NULL;
    char* valid_key2 = NULL;
    amxd_object_t* device1 = NULL;
    amxd_object_t* device2 = NULL;
    amxc_var_t rules;
    amxc_var_t* address_rule = NULL;
    amxc_var_t* address_list = NULL;
    bool has_tag = false;

    valid_key1 = new_device_or_get_key("03:01:02:03:04:05", "mac", false);
    valid_key2 = new_device_or_get_key("03:01:02:03:04:06", "mac edev", false);
    printf("validkey1 %s\n", valid_key1);
    printf("validkey2 %s\n", valid_key2);

    handle_events();

    device1 = gmaps_get_device(valid_key1);
    device2 = gmaps_get_device(valid_key2);

    assert_non_null(device1);
    assert_non_null(device2);

    /* link device2 to device1 */
    util_chosenlink_link_devices(valid_key1, valid_key2, true);

    handle_events();

    assert_true(gmaps_device_is_linked_to(device2, device1, gmap_traverse_up));


    amxc_var_init(&rules);
    amxc_var_set_type(&rules, AMXC_VAR_ID_LIST);

    address_rule = amxc_var_add_new(&rules);
    amxc_var_set_type(address_rule, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, address_rule, "Type", "Address");
    amxc_var_add_key(int64_t, address_rule, "Number", 1);
    address_list = amxc_var_add_new_key(address_rule, "Address");
    amxc_var_set_type(address_list, AMXC_VAR_ID_LIST);
    amxc_var_add(cstring_t, address_list, "03:01:02:03:04:06");

    assert_true(gmaps_devices_merge_add_rules(valid_key1, &rules));

    handle_events();

    assert_true(gmaps_device_is_alternative_from(device1, device2));
    assert_true(amxc_llist_size(gmaps_get_devices_merge_rules()) == 0);

    gmaps_device_has_tag("valid_key2", "edev", "", 0, &has_tag);
    assert_true(&has_tag);

    amxc_var_clean(&rules);
    free(valid_key1);
    free(valid_key2);
}

void test_gmap_add_one_filter_merge_rule(GMAPS_UNUSED void** state) {
    char* valid_key1 = NULL;
    char* valid_key2 = NULL;
    amxd_object_t* device1 = NULL;
    amxd_object_t* device2 = NULL;
    amxc_var_t rules;
    amxc_var_t* filter_rule = NULL;
    bool has_tag = false;

    valid_key1 = new_device_or_get_key("02:01:02:03:04:05", "mac", false);
    valid_key2 = new_device_or_get_key("64:01:02:03:04:05", "mac edev", false);
    printf("validkey1 %s\n", valid_key1);
    printf("validkey2 %s\n", valid_key2);

    handle_events();

    device1 = gmaps_get_device(valid_key1);
    device2 = gmaps_get_device(valid_key2);

    assert_non_null(device1);
    assert_non_null(device2);

    /* link device2 to device1 */
    util_chosenlink_link_devices(valid_key1, valid_key2, true);

    handle_events();

    assert_true(gmaps_device_is_linked_to(device2, device1, gmap_traverse_up));


    amxc_var_init(&rules);
    amxc_var_set_type(&rules, AMXC_VAR_ID_LIST);

    filter_rule = amxc_var_add_new(&rules);
    amxc_var_set_type(filter_rule, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, filter_rule, "Type", "Filter");
    amxc_var_add_key(cstring_t, filter_rule, "Mask", "00FFFFFFFFFF");
    amxc_var_add_key(cstring_t, filter_rule, "Value", "640000000000");

    assert_true(gmaps_devices_merge_add_rules(valid_key1, &rules));

    handle_events();

    assert_true(gmaps_device_is_alternative_from(device1, device2));
    assert_true(amxc_llist_size(gmaps_get_devices_merge_rules()) == 0);

    gmaps_device_has_tag("valid_key2", "edev", "", 0, &has_tag);
    assert_true(&has_tag);

    amxc_var_clean(&rules);
    free(valid_key1);
    free(valid_key2);
}


void test_gmap_add_one_offset_merge_rule(GMAPS_UNUSED void** state) {
    char* valid_key1 = NULL;
    char* valid_key2 = NULL;
    amxd_object_t* device1 = NULL;
    amxd_object_t* device2 = NULL;
    amxc_var_t rules;
    amxc_var_t* offset_rule = NULL;
    amxc_var_t* offset_list = NULL;
    bool has_tag = false;

    valid_key1 = new_device_or_get_key("01:01:02:03:04:05", "mac", false);
    valid_key2 = new_device_or_get_key("01:01:02:03:04:06", "mac edev", false);
    printf("validkey1 %s\n", valid_key1);
    printf("validkey2 %s\n", valid_key2);

    handle_events();

    device1 = gmaps_get_device(valid_key1);
    device2 = gmaps_get_device(valid_key2);

    assert_non_null(device1);
    assert_non_null(device2);

    /* link device2 to device1 */
    util_chosenlink_link_devices(valid_key1, valid_key2, true);

    handle_events();

    assert_true(gmaps_device_is_linked_to(device2, device1, gmap_traverse_up));


    amxc_var_init(&rules);
    amxc_var_set_type(&rules, AMXC_VAR_ID_LIST);

    offset_rule = amxc_var_add_new(&rules);
    amxc_var_set_type(offset_rule, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, offset_rule, "Type", "Offset");
    amxc_var_add_key(int64_t, offset_rule, "Number", 1);
    offset_list = amxc_var_add_new_key(offset_rule, "Offset");
    amxc_var_set_type(offset_list, AMXC_VAR_ID_LIST);
    amxc_var_add(int64_t, offset_list, 1);

    assert_true(gmaps_devices_merge_add_rules(valid_key1, &rules));

    handle_events();

    assert_true(gmaps_device_is_alternative_from(device1, device2));
    assert_true(amxc_llist_size(gmaps_get_devices_merge_rules()) == 0);

    gmaps_device_has_tag("valid_key2", "edev", "", 0, &has_tag);
    assert_true(&has_tag);

    amxc_var_clean(&rules);
    free(valid_key1);
    free(valid_key2);
}

void test_gmap_add_one_filter_offset_merge_rule(GMAPS_UNUSED void** state) {
    char* valid_key1 = NULL;
    char* valid_key2 = NULL;
    amxd_object_t* device1 = NULL;
    amxd_object_t* device2 = NULL;
    amxc_var_t rules;
    amxc_var_t* filter_offset_rule = NULL;
    amxc_var_t* offset_list = NULL;
    bool has_tag = false;

    valid_key1 = new_device_or_get_key("05:01:02:03:04:05", "mac", false);
    valid_key2 = new_device_or_get_key("73:01:02:03:04:06", "mac edev", false);
    printf("validkey1 %s\n", valid_key1);
    printf("validkey2 %s\n", valid_key2);

    handle_events();

    device1 = gmaps_get_device(valid_key1);
    device2 = gmaps_get_device(valid_key2);

    assert_non_null(device1);
    assert_non_null(device2);

    /* link device2 to device1 */
    util_chosenlink_link_devices(valid_key1, valid_key2, true);

    handle_events();

    assert_true(gmaps_device_is_linked_to(device2, device1, gmap_traverse_up));


    amxc_var_init(&rules);
    amxc_var_set_type(&rules, AMXC_VAR_ID_LIST);

    filter_offset_rule = amxc_var_add_new(&rules);
    amxc_var_set_type(filter_offset_rule, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, filter_offset_rule, "Type", "FilterOffset");
    amxc_var_add_key(cstring_t, filter_offset_rule, "Mask", "00FFFFFFFFFF");
    amxc_var_add_key(cstring_t, filter_offset_rule, "Value", "730000000000");
    amxc_var_add_key(int64_t, filter_offset_rule, "Number", 1);
    offset_list = amxc_var_add_new_key(filter_offset_rule, "Offset");
    amxc_var_set_type(offset_list, AMXC_VAR_ID_LIST);
    amxc_var_add(int64_t, offset_list, 1);

    assert_true(gmaps_devices_merge_add_rules(valid_key1, &rules));

    handle_events();

    assert_true(gmaps_device_is_alternative_from(device1, device2));
    assert_true(amxc_llist_size(gmaps_get_devices_merge_rules()) == 0);

    gmaps_device_has_tag("valid_key2", "edev", "", 0, &has_tag);
    assert_true(&has_tag);

    amxc_var_clean(&rules);
    free(valid_key1);
    free(valid_key2);
}

void test_gmap_add_one_filter_righshift_merge_rule(GMAPS_UNUSED void** state) {
    char* valid_key1 = NULL;
    char* valid_key2 = NULL;
    amxd_object_t* device1 = NULL;
    amxd_object_t* device2 = NULL;
    amxc_var_t rules;
    amxc_var_t* filter_rightshift_rule = NULL;
    bool has_tag = false;

    valid_key1 = new_device_or_get_key("06:01:02:03:04:05", "mac", false);
    valid_key2 = new_device_or_get_key("7E:00:81:01:04:05", "mac edev", false);
    printf("validkey1 %s\n", valid_key1);
    printf("validkey2 %s\n", valid_key2);

    handle_events();

    device1 = gmaps_get_device(valid_key1);
    device2 = gmaps_get_device(valid_key2);

    assert_non_null(device1);
    assert_non_null(device2);

    /* link device2 to device1 */
    util_chosenlink_link_devices(valid_key1, valid_key2, true);

    handle_events();

    assert_true(gmaps_device_is_linked_to(device2, device1, gmap_traverse_up));


    amxc_var_init(&rules);
    amxc_var_set_type(&rules, AMXC_VAR_ID_LIST);

    filter_rightshift_rule = amxc_var_add_new(&rules);
    amxc_var_set_type(filter_rightshift_rule, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, filter_rightshift_rule, "Type", "FilterRightShift");
    amxc_var_add_key(cstring_t, filter_rightshift_rule, "Mask", "00FFFFFFFFFF");
    amxc_var_add_key(cstring_t, filter_rightshift_rule, "Value", "7E0000000000");
    amxc_var_add_key(cstring_t, filter_rightshift_rule, "Shift", "00FFFFFF0000");

    assert_true(gmaps_devices_merge_add_rules(valid_key1, &rules));

    handle_events();

    assert_true(gmaps_device_is_alternative_from(device1, device2));
    assert_true(amxc_llist_size(gmaps_get_devices_merge_rules()) == 0);

    gmaps_device_has_tag("valid_key2", "edev", "", 0, &has_tag);
    assert_true(&has_tag);

    amxc_var_clean(&rules);
    free(valid_key1);
    free(valid_key2);
}

void test_gmap_add_one_filter_leftshift_merge_rule(GMAPS_UNUSED void** state) {
    char* valid_key1 = NULL;
    char* valid_key2 = NULL;
    amxd_object_t* device1 = NULL;
    amxd_object_t* device2 = NULL;
    amxc_var_t rules;
    amxc_var_t* filter_leftshift_rule = NULL;
    bool has_tag = false;

    valid_key1 = new_device_or_get_key("07:01:02:03:04:05", "mac", false);
    valid_key2 = new_device_or_get_key("7E:01:04:03:04:05", "mac edev", false);
    printf("validkey1 %s\n", valid_key1);
    printf("validkey2 %s\n", valid_key2);

    handle_events();

    device1 = gmaps_get_device(valid_key1);
    device2 = gmaps_get_device(valid_key2);

    assert_non_null(device1);
    assert_non_null(device2);

    /* link device2 to device1 */
    util_chosenlink_link_devices(valid_key1, valid_key2, true);

    handle_events();

    assert_true(gmaps_device_is_linked_to(device2, device1, gmap_traverse_up));

    amxc_var_init(&rules);
    amxc_var_set_type(&rules, AMXC_VAR_ID_LIST);

    filter_leftshift_rule = amxc_var_add_new(&rules);
    amxc_var_set_type(filter_leftshift_rule, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, filter_leftshift_rule, "Type", "FilterLeftShift");
    amxc_var_add_key(cstring_t, filter_leftshift_rule, "Mask", "00FFFFFFFFFF");
    amxc_var_add_key(cstring_t, filter_leftshift_rule, "Value", "7E0000000000");
    amxc_var_add_key(cstring_t, filter_leftshift_rule, "Shift", "0000FF000000");

    assert_true(gmaps_devices_merge_add_rules(valid_key1, &rules));

    handle_events();

    assert_true(gmaps_device_is_alternative_from(device1, device2));
    assert_true(amxc_llist_size(gmaps_get_devices_merge_rules()) == 0);

    gmaps_device_has_tag("valid_key2", "edev", "", 0, &has_tag);
    assert_true(&has_tag);

    amxc_var_clean(&rules);
    free(valid_key1);
    free(valid_key2);
}


void test_gmap_add_one_offset_merge_rule_alternative_link_swap(GMAPS_UNUSED void** state) {
    // This test creates the following topology:
    //                    dev4
    //                     |
    //                     v
    //    (master)dev1~~~~~dev2(alternative)
    //
    // and verifies if both dev2 and dev1 are reachable from dev4.

    char* valid_key1 = NULL;
    char* valid_key2 = NULL;
    char* valid_key4 = NULL;

    amxd_object_t* device1 = NULL;
    amxd_object_t* device2 = NULL;
    amxd_object_t* device4 = NULL;

    amxc_var_t rules;
    amxc_var_t* offset_rule = NULL;
    amxc_var_t* offset_list = NULL;

    valid_key1 = new_device_or_get_key("01:01:02:03:04:05", "mac", false);
    valid_key2 = new_device_or_get_key("01:01:02:03:04:06", "mac", false);
    valid_key4 = new_device_or_get_key("01:02:02:03:04:06", "mac", false);
    printf("validkey1 %s\n", valid_key1);
    printf("validkey2 %s\n", valid_key2);
    printf("validkey4 %s\n", valid_key4);

    handle_events();

    device1 = gmaps_get_device(valid_key1);
    device2 = gmaps_get_device(valid_key2);
    device4 = gmaps_get_device(valid_key4);

    assert_non_null(device1);
    assert_non_null(device2);
    assert_non_null(device4);

    gmaps_device_set_alternative(device1, device2);
    assert_true(gmaps_device_is_alternative_from(device1, device2));
    gmaps_device_remove_alternative(device1, device2);
    handle_events();

    /* link device2 to device4 */
    util_call_linkAdd(valid_key4, valid_key2, "testdatasource", "default", true);

    handle_events();

    assert_true(gmaps_device_is_linked_to(device4, device2, gmap_traverse_down));
    assert_true(gmaps_device_is_linked_to(device2, device4, gmap_traverse_up));
    assert_false(gmaps_device_is_linked_to(device4, device1, gmap_traverse_down));
    assert_false(gmaps_device_is_linked_to(device1, device4, gmap_traverse_up));

    amxc_var_init(&rules);
    amxc_var_set_type(&rules, AMXC_VAR_ID_LIST);
    offset_rule = amxc_var_add_new(&rules);
    amxc_var_set_type(offset_rule, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, offset_rule, "Type", "Offset");
    amxc_var_add_key(int64_t, offset_rule, "Number", 1);
    offset_list = amxc_var_add_new_key(offset_rule, "Offset");
    amxc_var_set_type(offset_list, AMXC_VAR_ID_LIST);
    amxc_var_add(int64_t, offset_list, 1);

    assert_true(gmaps_devices_merge_add_rules(valid_key1, &rules));

    handle_events();

    assert_true(gmaps_device_is_alternative_from(device1, device2));
    assert_true(amxc_llist_size(gmaps_get_devices_merge_rules()) == 0);

    assert_true(gmaps_device_is_linked_to(device4, device2, gmap_traverse_down));
    assert_true(gmaps_device_is_linked_to(device2, device4, gmap_traverse_up));
    assert_true(gmaps_device_is_linked_to(device4, device1, gmap_traverse_down));
    assert_true(gmaps_device_is_linked_to(device1, device4, gmap_traverse_up));

    amxc_var_clean(&rules);
    free(valid_key1);
    free(valid_key2);
    free(valid_key4);
}


void test_gmap_add_offset_rule_already_alternative(GMAPS_UNUSED void** state) {
    char* valid_key1 = NULL;
    char* valid_key2 = NULL;
    amxd_object_t* device1 = NULL;
    amxd_object_t* device2 = NULL;

    amxc_var_t rules;
    amxc_var_t* offset_rule = NULL;
    amxc_var_t* offset_list = NULL;

    valid_key1 = new_device_or_get_key("01:01:02:03:04:05", "mac", false);
    valid_key2 = new_device_or_get_key("01:01:02:03:04:06", "mac", false);

    device1 = gmaps_get_device(valid_key1);
    device2 = gmaps_get_device(valid_key2);

    assert_non_null(device1);
    assert_non_null(device2);

    gmaps_device_set_alternative(device1, device2);
    assert_true(gmaps_device_is_alternative_from(device1, device2));

    amxc_var_init(&rules);
    amxc_var_set_type(&rules, AMXC_VAR_ID_LIST);
    offset_rule = amxc_var_add_new(&rules);
    amxc_var_set_type(offset_rule, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, offset_rule, "Type", "Offset");
    amxc_var_add_key(int64_t, offset_rule, "Number", 1);
    offset_list = amxc_var_add_new_key(offset_rule, "Offset");
    amxc_var_set_type(offset_list, AMXC_VAR_ID_LIST);
    amxc_var_add(int64_t, offset_list, 1);

    assert_true(gmaps_devices_merge_add_rules(valid_key1, &rules));

    handle_events();

    assert_true(amxc_llist_size(gmaps_get_devices_merge_rules()) == 0);

    amxc_var_clean(&rules);
    free(valid_key1);
    free(valid_key2);

}

void test_gmap_add_offset_rule_already_master(GMAPS_UNUSED void** state) {
    char* valid_key1 = NULL;
    char* valid_key2 = NULL;
    amxd_object_t* device1 = NULL;
    amxd_object_t* device2 = NULL;

    amxc_var_t rules;
    amxc_var_t* offset_rule = NULL;
    amxc_var_t* offset_list = NULL;

    valid_key1 = new_device_or_get_key("01:01:02:03:04:05", "mac", false);
    valid_key2 = new_device_or_get_key("01:01:02:03:04:06", "mac", false);

    device1 = gmaps_get_device(valid_key1);
    device2 = gmaps_get_device(valid_key2);

    assert_non_null(device1);
    assert_non_null(device2);

    gmaps_device_set_alternative(device1, device2);
    assert_true(gmaps_device_is_alternative_from(device1, device2));

    amxc_var_init(&rules);
    amxc_var_set_type(&rules, AMXC_VAR_ID_LIST);
    offset_rule = amxc_var_add_new(&rules);
    amxc_var_set_type(offset_rule, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, offset_rule, "Type", "Offset");
    amxc_var_add_key(int64_t, offset_rule, "Number", 1);
    offset_list = amxc_var_add_new_key(offset_rule, "Offset");
    amxc_var_set_type(offset_list, AMXC_VAR_ID_LIST);
    amxc_var_add(int64_t, offset_list, -1);

    assert_true(gmaps_devices_merge_add_rules(valid_key2, &rules));

    handle_events();

    assert_true(gmaps_device_is_alternative_from(device1, device2));
    assert_true(amxc_llist_size(gmaps_get_devices_merge_rules()) == 0);

    amxc_var_clean(&rules);
    free(valid_key1);
    free(valid_key2);

}

void test_gmap_add_offset_rule_before_alternative(GMAPS_UNUSED void** state) {
    char* valid_key1 = NULL;
    char* valid_key3 = NULL;
    amxd_object_t* device1 = NULL;
    amxd_object_t* device3 = NULL;

    amxc_var_t rules;
    amxc_var_t* offset_rule = NULL;
    amxc_var_t* offset_list = NULL;

    valid_key1 = new_device_or_get_key("01:01:02:03:04:05", "mac", false);

    device1 = gmaps_get_device(valid_key1);
    assert_non_null(device1);

    amxc_var_init(&rules);
    amxc_var_set_type(&rules, AMXC_VAR_ID_LIST);
    offset_rule = amxc_var_add_new(&rules);
    amxc_var_set_type(offset_rule, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, offset_rule, "Type", "Offset");
    amxc_var_add_key(int64_t, offset_rule, "Number", 1);
    offset_list = amxc_var_add_new_key(offset_rule, "Offset");
    amxc_var_set_type(offset_list, AMXC_VAR_ID_LIST);
    amxc_var_add(int64_t, offset_list, 2);

    assert_true(gmaps_devices_merge_add_rules(valid_key1, &rules));

    handle_events();

    assert_true(amxc_llist_size(gmaps_get_devices_merge_rules()) != 0);

    valid_key3 = new_device_or_get_key("01:01:02:03:04:07", "mac", false);

    device3 = gmaps_get_device(valid_key3);
    assert_non_null(device3);

    handle_events();
    /* rule executed when device3 created, rule deleted */
    assert_true(amxc_llist_size(gmaps_get_devices_merge_rules()) == 0);
    assert_true(gmaps_device_is_alternative_from(device1, device3));

    amxc_var_clean(&rules);
    free(valid_key1);
    free(valid_key3);

}

void test_gmap_destroy_device_ongoing_offset_rule(GMAPS_UNUSED void** state) {

    amxd_object_t* device_templ = NULL;
    amxd_object_t* devices = NULL;

    char* valid_key1 = NULL;
    char* valid_key2 = NULL;
    amxd_object_t* device1 = NULL;
    amxd_object_t* device2 = NULL;

    amxc_var_t rules;
    amxc_var_t* offset_rule = NULL;
    amxc_var_t* offset_list = NULL;

    valid_key1 = new_device_or_get_key("01:01:02:03:04:05", "mac", false);
    valid_key2 = new_device_or_get_key("01:01:02:03:04:06", "mac", false);

    device1 = gmaps_get_device(valid_key1);
    device2 = gmaps_get_device(valid_key2);

    assert_non_null(device1);
    assert_non_null(device2);

    /* dev1ce1, device area already alternatives */
    gmaps_device_set_alternative(device1, device2);
    assert_true(gmaps_device_is_alternative_from(device1, device2));

    amxc_var_init(&rules);
    amxc_var_set_type(&rules, AMXC_VAR_ID_LIST);

    offset_rule = amxc_var_add_new(&rules);
    amxc_var_set_type(offset_rule, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, offset_rule, "Type", "Offset");
    // Number =2 then considered as passed, rule deleted
    amxc_var_add_key(int64_t, offset_rule, "Number", 3);
    offset_list = amxc_var_add_new_key(offset_rule, "Offset");
    amxc_var_set_type(offset_list, AMXC_VAR_ID_LIST);
    amxc_var_add(int64_t, offset_list, 1);
    amxc_var_add(int64_t, offset_list, 2);
    amxc_var_add(int64_t, offset_list, 3);
    assert_true(gmaps_devices_merge_add_rules(valid_key1, &rules));

    handle_events();

    assert_true(amxc_llist_size(gmaps_get_devices_merge_rules()) != 0);

    devices = amxd_dm_findf(amxut_bus_dm(), "Devices");
    assert_non_null(devices);
    device_templ = amxd_object_get_child(devices, "Device");
    assert_non_null(device_templ);

    gmaps_delete_device(device_templ, valid_key1);
    /* device deleted, associated rule should be deleted at the same time */

    assert_true(amxc_llist_size(gmaps_get_devices_merge_rules()) == 0);

    amxc_var_clean(&rules);
    free(valid_key1);
    free(valid_key2);
}

void test_gmap_delete_ongoing_offset_rule_2(GMAPS_UNUSED void** state) {

    char* valid_key1 = NULL;
    char* valid_key2 = NULL;
    amxd_object_t* device1 = NULL;
    amxd_object_t* device2 = NULL;

    amxc_var_t rules;
    amxc_var_t* offset_rule = NULL;
    amxc_var_t* offset_list = NULL;

    valid_key1 = new_device_or_get_key("01:01:02:03:04:05", "mac", false);
    valid_key2 = new_device_or_get_key("01:01:02:03:04:06", "mac", false);

    device1 = gmaps_get_device(valid_key1);
    device2 = gmaps_get_device(valid_key2);

    assert_non_null(device1);
    assert_non_null(device2);

    handle_events();

    assert_false(gmaps_device_is_alternative_from(device1, device2));

    amxc_var_init(&rules);
    amxc_var_set_type(&rules, AMXC_VAR_ID_LIST);

    offset_rule = amxc_var_add_new(&rules);
    amxc_var_set_type(offset_rule, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, offset_rule, "Type", "Offset");
    // Number =2 then considered as passed, rule deleted
    amxc_var_add_key(int64_t, offset_rule, "Number", 3);
    offset_list = amxc_var_add_new_key(offset_rule, "Offset");
    amxc_var_set_type(offset_list, AMXC_VAR_ID_LIST);

    amxc_var_add(int64_t, offset_list, 1);
    amxc_var_add(int64_t, offset_list, 2);
    amxc_var_add(int64_t, offset_list, 3);
    assert_true(gmaps_devices_merge_add_rules(valid_key1, &rules));

    handle_events();
    /* offset 1 executed, offset 2 executed, offset 3 non executed: rule not deleted */
    assert_true(amxc_llist_size(gmaps_get_devices_merge_rules()) != 0);

    assert_true(gmaps_devices_merge_delete_rules(valid_key1));

    assert_true(amxc_llist_size(gmaps_get_devices_merge_rules()) == 0);

    assert_true(gmaps_device_is_alternative_from(device1, device2));

    amxc_var_clean(&rules);
    free(valid_key1);
    free(valid_key2);
}


void test_gmap_can_invoke_add_alternative_rules(GMAPS_UNUSED void** state) {
    char* valid_key1 = NULL;
    char* valid_key2 = NULL;
    amxd_object_t* device1 = NULL;
    amxd_object_t* device2 = NULL;
    amxc_var_t args;
    amxc_var_t result;

    amxc_var_t* rules;
    amxc_var_t* offset_rule = NULL;
    amxc_var_t* offset_list = NULL;

    valid_key1 = new_device_or_get_key("01:01:02:03:04:05", "mac", false);
    valid_key2 = new_device_or_get_key("01:01:02:03:04:06", "mac", false);

    device1 = gmaps_get_device(valid_key1);
    device2 = gmaps_get_device(valid_key2);

    assert_non_null(device1);
    assert_non_null(device2);

    /*make sure if not alternatives */
    gmaps_device_set_alternative(device1, device2);
    assert_true(gmaps_device_is_alternative_from(device1, device2));
    gmaps_device_remove_alternative(device1, device2);
    handle_events();
    assert_false(gmaps_device_is_alternative_from(device1, device2));

    amxc_var_init(&args);
    amxc_var_init(&result);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    rules = amxc_var_add_new_key(&args, "rules");

    amxc_var_set_type(rules, AMXC_VAR_ID_LIST);
    offset_rule = amxc_var_add_new(rules);
    amxc_var_set_type(offset_rule, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, offset_rule, "Type", "Offset");
    amxc_var_add_key(int64_t, offset_rule, "Number", 3);
    offset_list = amxc_var_add_new_key(offset_rule, "Offset");
    amxc_var_set_type(offset_list, AMXC_VAR_ID_LIST);
    amxc_var_add(int64_t, offset_list, 1);
    amxc_var_add(int64_t, offset_list, 2);
    amxc_var_add(int64_t, offset_list, 3);

    assert_int_equal(amxd_object_invoke_function(device1, "setAlternativeRules", &args, &result), amxd_status_ok);
    assert_true(amxc_var_constcast(bool, &result));

    amxc_var_clean(&args);
    amxc_var_clean(&result);

    handle_events();

    assert_true(amxc_llist_size(gmaps_get_devices_merge_rules()) != 0);

    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_init(&result);
    assert_int_equal(amxd_object_invoke_function(device1, "removeAlternativeRules", &args, &result), amxd_status_ok);
    assert_true(amxc_var_constcast(bool, &result));

    assert_true(amxc_llist_size(gmaps_get_devices_merge_rules()) == 0);

    amxc_var_clean(&args);
    amxc_var_clean(&result);
    free(valid_key1);
    free(valid_key2);
}

// alternative is already alternative
// proposed alternative is already master

#if 0
void test_gmap_can_invoke_set_alternative(GMAPS_UNUSED void** state) {
    amxd_object_t* device = gmaps_get_device("test2Key");
    amxd_object_t* device_alt = gmaps_get_device("test3Key");
    amxc_var_t args;
    amxc_var_t check_args;
    amxc_var_t result;
    amxc_var_t check_result;

    amxc_var_init(&args);
    amxc_var_init(&check_args);
    amxc_var_init(&result);
    amxc_var_init(&check_result);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_set_type(&check_args, AMXC_VAR_ID_HTABLE);

    amxc_var_add_key(cstring_t, &args, "alternative", "test3Key");
    amxc_var_add_key(cstring_t, &check_args, "alternative", "test3Key");


    amxd_object_invoke_function(device, "setAlternative", &args, &result);

    assert_int_equal(amxd_object_invoke_function(device, "isAlternative", &check_args, &check_result), amxd_status_ok);

    assert_true(amxc_var_get_bool(&check_result));

    assert_true(gmaps_device_are_alternatives(device_alt, device));
    assert_true(gmaps_device_is_alternative(device_alt));
    assert_true(gmaps_device_is_alternative_from(device, device_alt));

    assert_true(gmaps_device_is_linked_to(device, device_alt, gmap_traverse_down));

    amxc_var_clean(&args);
    amxc_var_clean(&result);
    amxc_var_clean(&check_result);
    amxc_var_clean(&check_args);

}

void test_gmap_can_device_remove_alternative(GMAPS_UNUSED void** state) {
    amxd_object_t* device = gmaps_get_device("test2Key");
    amxd_object_t* device_alt = gmaps_get_device("test3Key");

    gmaps_device_remove_alternative(device, device_alt);

    assert_false(gmaps_device_are_alternatives(device, device_alt));
    assert_false(gmaps_device_is_alternative(device_alt));
    assert_false(gmaps_device_is_alternative_from(device, device_alt));

}

void test_gmap_can_invoke_remove_alternatives(GMAPS_UNUSED void** state) {
    amxd_object_t* device = gmaps_get_device("test2Key");
    amxd_object_t* device_alt = gmaps_get_device("test3Key");

    amxc_var_t args;
    amxc_var_t check_args;
    amxc_var_t result;
    amxc_var_t check_result;


    amxc_var_init(&args);
    amxc_var_init(&check_args);
    amxc_var_init(&result);
    amxc_var_init(&check_result);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_set_type(&check_result, AMXC_VAR_ID_HTABLE);

    amxc_var_add_key(cstring_t, &args, "alternative", "test3Key");
    amxc_var_add_key(cstring_t, &check_args, "alternative", "test3Key");


    amxd_object_invoke_function(device, "removeAlternative", &args, &result);
    amxd_object_invoke_function(device, "isAlternative", &check_args, &check_result);

    assert_false(amxc_var_constcast(bool, &check_result));

    assert_false(gmaps_device_are_alternatives(device_alt, device));
    assert_false(gmaps_device_is_alternative(device_alt));

    amxc_var_clean(&args);
    amxc_var_clean(&result);
    amxc_var_clean(&check_result);
    amxc_var_clean(&check_args);
}
#endif

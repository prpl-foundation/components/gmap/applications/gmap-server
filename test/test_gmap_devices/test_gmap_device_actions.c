/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdarg.h>
#include <setjmp.h>
#include <cmocka.h>

#include "test_devices.h"
#include "gmaps_devices.h"
#include "gmaps_dm_devices.h"
#include "../test_common/test_util.h"


static bool function_name_present_in_topology(amxc_var_t* topology, char* func_name, char* action_name) {
    amxc_var_t* actions = amxc_var_get_key(topology, "Actions", AMXC_VAR_FLAG_DEFAULT);

    when_null_status(actions, exit, NULL);

    amxc_var_for_each(var, actions) {
        const char* function = GET_CHAR(var, "Function");
        const char* action = GET_CHAR(var, "Name");

        if((strcmp(function, func_name) == 0) && (strcmp(action, action_name) == 0)) {
            return true;
        }
    }

exit:
    return false;
}


void test_add_action(GMAPS_UNUSED void** state) {
    char* dev_name = "dev_add_action";
    char* func_name = "unimplemented1";
    char* action_name = "chosen_action_name1";
    amxd_object_t* device = NULL;
    amxc_var_t args;
    amxc_var_t ret;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    test_util_scanMibDir("../test_common/mibs");
    util_create_device(dev_name, "", "addaction", false, "", amxd_status_ok, NULL);

    device = amxd_dm_findf(amxut_bus_dm(), "Devices.Device.%s", dev_name);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "traverse", "down");
    amxc_var_add_key(cstring_t, &args, "expression", "");
    amxc_var_add_key(cstring_t, &args, "flags", "");

    assert_int_equal(amxd_object_invoke_function(device,
                                                 "topology",
                                                 &args,
                                                 &ret),
                     amxd_status_ok);

    assert_false(function_name_present_in_topology(&ret, func_name, action_name));

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "function", func_name);
    amxc_var_add_key(cstring_t, &args, "name", action_name);

    assert_int_equal(amxd_object_invoke_function(device,
                                                 "addAction",
                                                 &args,
                                                 &ret),
                     amxd_status_ok);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "traverse", "down");
    amxc_var_add_key(cstring_t, &args, "expression", "");
    amxc_var_add_key(cstring_t, &args, "flags", "");

    assert_int_equal(amxd_object_invoke_function(device,
                                                 "topology",
                                                 &args,
                                                 &ret),
                     amxd_status_ok);

    amxc_var_dump(&ret, 1);
    assert_true(function_name_present_in_topology(&ret, func_name, action_name));

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_add_second_action(GMAPS_UNUSED void** state) {
    char* dev_name = "dev_add_action";
    char* func_name1 = "unimplemented1";
    char* func_name2 = "unimplemented2";
    char* action_name1 = "chosen_action_name1";
    char* action_name2 = "chosen_action_name2";
    amxd_object_t* device = NULL;
    amxc_var_t args;
    amxc_var_t ret;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    device = amxd_dm_findf(amxut_bus_dm(), "Devices.Device.%s", dev_name);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "function", func_name2);
    amxc_var_add_key(cstring_t, &args, "name", action_name2);

    assert_int_equal(amxd_object_invoke_function(device,
                                                 "addAction",
                                                 &args,
                                                 &ret),
                     amxd_status_ok);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "traverse", "down");
    amxc_var_add_key(cstring_t, &args, "expression", "");
    amxc_var_add_key(cstring_t, &args, "flags", "");

    assert_int_equal(amxd_object_invoke_function(device,
                                                 "topology",
                                                 &args,
                                                 &ret),
                     amxd_status_ok);

    amxc_var_dump(&ret, 1);
    assert_true(function_name_present_in_topology(&ret, func_name1, action_name1));
    assert_true(function_name_present_in_topology(&ret, func_name2, action_name2));

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_action_already_exists(GMAPS_UNUSED void** state) {
    char* dev_name = "dev_add_action";
    char* func_name = "unimplemented2";
    char* old_action_name = "chosen_action_name2";
    char* new_action_name = "chosen_action_name_new";
    amxd_object_t* device = NULL;
    amxc_var_t args;
    amxc_var_t ret;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    device = amxd_dm_findf(amxut_bus_dm(), "Devices.Device.%s", dev_name);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "function", func_name);
    amxc_var_add_key(cstring_t, &args, "name", new_action_name);

    assert_int_equal(amxd_object_invoke_function(device,
                                                 "addAction",
                                                 &args,
                                                 &ret),
                     amxd_status_invalid_action);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "traverse", "down");
    amxc_var_add_key(cstring_t, &args, "expression", "");
    amxc_var_add_key(cstring_t, &args, "flags", "");

    assert_int_equal(amxd_object_invoke_function(device,
                                                 "topology",
                                                 &args,
                                                 &ret),
                     amxd_status_ok);

    amxc_var_dump(&ret, 1);
    assert_true(function_name_present_in_topology(&ret, func_name, old_action_name));
    assert_false(function_name_present_in_topology(&ret, func_name, new_action_name));

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_remove_action(GMAPS_UNUSED void** state) {
    char* dev_name = "dev_add_action";
    char* func_name = "unimplemented1";
    char* action_name = "chosen_action_name1";
    amxd_object_t* device = NULL;
    amxc_var_t args;
    amxc_var_t ret;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    device = amxd_dm_findf(amxut_bus_dm(), "Devices.Device.%s", dev_name);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "function", func_name);
    amxc_var_add_key(cstring_t, &args, "name", action_name);

    assert_int_equal(amxd_object_invoke_function(device,
                                                 "removeAction",
                                                 &args,
                                                 &ret),
                     amxd_status_ok);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "traverse", "down");
    amxc_var_add_key(cstring_t, &args, "expression", "");
    amxc_var_add_key(cstring_t, &args, "flags", "");

    assert_int_equal(amxd_object_invoke_function(device,
                                                 "topology",
                                                 &args,
                                                 &ret),
                     amxd_status_ok);

    amxc_var_dump(&ret, 1);
    assert_false(function_name_present_in_topology(&ret, func_name, action_name));

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_remove_second_action(GMAPS_UNUSED void** state) {
    char* dev_name = "dev_add_action";
    char* func_name = "unimplemented2";
    char* action_name = "chosen_action_name2";
    amxd_object_t* device = NULL;
    amxc_var_t args;
    amxc_var_t ret;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    device = amxd_dm_findf(amxut_bus_dm(), "Devices.Device.%s", dev_name);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "function", func_name);
    amxc_var_add_key(cstring_t, &args, "name", action_name);

    assert_int_equal(amxd_object_invoke_function(device,
                                                 "removeAction",
                                                 &args,
                                                 &ret),
                     amxd_status_ok);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "traverse", "down");
    amxc_var_add_key(cstring_t, &args, "expression", "");
    amxc_var_add_key(cstring_t, &args, "flags", "");

    assert_int_equal(amxd_object_invoke_function(device,
                                                 "topology",
                                                 &args,
                                                 &ret),
                     amxd_status_ok);

    amxc_var_dump(&ret, 1);
    assert_false(function_name_present_in_topology(&ret, func_name, action_name));
    assert_false(amxc_var_get_key(&ret, "Actions", AMXC_VAR_FLAG_DEFAULT));


    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}
/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdarg.h>
#include <setjmp.h>
#include <cmocka.h>

#include "test_devices.h"
#include "gmaps_devices.h"
#include "gmaps_dm_devices.h"
#include "../test_common/test_util.h"

#define NBRELEMS(x)  (sizeof(x) / sizeof((x)[0]))

typedef struct _device2paramvalue {
    const char* device_name;
    const char* parameter_value;
} device2paramvalue_t;

typedef struct _first_param_test_instance {
    const char* orig_device_key;
    const char* traverse_mode;
    const char* expression;
    const char* param_rslt_value;
} first_param_test_instance_t;

device2paramvalue_t device_param_config[] =
{
    {TEST_TOPOLOGY_KEY_DEV_1_1_0, "ParamValue1"},
    {TEST_TOPOLOGY_KEY_DEV_2_1_0, "ParamValue3"},
    {TEST_TOPOLOGY_KEY_DEV_2_1_1, "ParamValue2"},
    {TEST_TOPOLOGY_KEY_DEV_2_2_0, "ParamValue4"},
    {TEST_TOPOLOGY_KEY_DEV_3_1_0, "ParamValue5"},
    {TEST_TOPOLOGY_KEY_DEV_3_2_0, "ParamValue6"},
    {TEST_TOPOLOGY_KEY_DEV_3_2_1, "ParamValue7"}
};

first_param_test_instance_t first_param_test_instance[] = {
    {TEST_TOPOLOGY_KEY_DEV_1_1_0, "down", "", "ParamValue1"},                                                            // no expression, down, return value of first matching, so same as this in this case
    {TEST_TOPOLOGY_KEY_DEV_1_1_0, "down", ".Key starts with \"" TEST_TOPOLOGY_KEY_DEV_3_2_1_PREFIX "\"", "ParamValue6"}, // expression matches alternative device returns value of masterdevice
    {TEST_TOPOLOGY_KEY_DEV_1_1_0, "down", ".Key == \"Axel\"", NULL},                                                     // no match should be found
    {TEST_TOPOLOGY_KEY_DEV_3_1_0, "down", ".Key == \"Dev_1_1_0\"", NULL},                                                // no match should be found
    {TEST_TOPOLOGY_KEY_DEV_1_1_0, "down_exclusive", "", "ParamValue3"},                                                  //exclusive: don't take this, first master son taken
    {TEST_TOPOLOGY_KEY_DEV_1_1_0, "down_exclusive", ".Key starts with \"" TEST_TOPOLOGY_KEY_DEV_3_1_0_PREFIX "\"", "ParamValue5"},
    {TEST_TOPOLOGY_KEY_DEV_1_1_0, "down_exclusive", ".Key starts with \"" TEST_TOPOLOGY_KEY_DEV_2_2_0_PREFIX "\"", "ParamValue4"},
    {TEST_TOPOLOGY_KEY_DEV_1_1_0, "down_exclusive", ".Key == \"Axel\"", NULL},                                         // no match should be found
    {TEST_TOPOLOGY_KEY_DEV_3_1_0, "down_exclusive", "", NULL},                                                         // no match should be found
    {TEST_TOPOLOGY_KEY_DEV_1_1_0, "one_down", "", "ParamValue1"},                                                      // same as this as non exlusive
    {TEST_TOPOLOGY_KEY_DEV_1_1_0, "one_down", ".Key starts with \"" TEST_TOPOLOGY_KEY_DEV_3_2_1_PREFIX "\"", NULL},    // only navigaute one level down so no match
    {TEST_TOPOLOGY_KEY_DEV_1_1_0, "one_down_exclusive", "", "ParamValue3"},                                            // exclusive so first master son
    {TEST_TOPOLOGY_KEY_DEV_1_1_0, "one_down_exclusive", ".Key starts with \"" TEST_TOPOLOGY_KEY_DEV_3_2_1_PREFIX "\"", NULL},
    {TEST_TOPOLOGY_KEY_DEV_3_1_0, "one_down_exclusive", "", NULL},                                                     // no match should be found
    {TEST_TOPOLOGY_KEY_DEV_1_1_0, "this", "", "ParamValue1"},                                                          // return this
    {TEST_TOPOLOGY_KEY_DEV_1_1_0, "this", ".Key starts with \"" TEST_TOPOLOGY_KEY_DEV_3_2_1_PREFIX "\"", NULL},        //  no match on this
    {TEST_TOPOLOGY_KEY_DEV_1_1_0, "", "", "ParamValue1"},                                                              // use defaults : down
    {TEST_TOPOLOGY_KEY_DEV_1_1_0, "up", "", "ParamValue1"},                                                            // will return this
    {TEST_TOPOLOGY_KEY_DEV_3_1_0, "up", ".Key starts with \"" TEST_TOPOLOGY_KEY_DEV_1_1_0_PREFIX "\"", "ParamValue1"},
    {TEST_TOPOLOGY_KEY_DEV_3_1_0, "up", ".Key starts with \"" TEST_TOPOLOGY_KEY_DEV_2_2_0_PREFIX "\"", "ParamValue4"}, // no match during traverse
    {TEST_TOPOLOGY_KEY_DEV_3_1_0, "up", ".Key starts with \"" TEST_TOPOLOGY_KEY_DEV_2_1_0_PREFIX "\"", NULL},          // no match during traverse
    {TEST_TOPOLOGY_KEY_DEV_3_1_0, "up_exclusive", "", "ParamValue4"},
    {TEST_TOPOLOGY_KEY_DEV_3_1_0, "up_exclusive", ".Key starts with \"" TEST_TOPOLOGY_KEY_DEV_1_1_0_PREFIX "\"", "ParamValue1"},
    {TEST_TOPOLOGY_KEY_DEV_3_1_0, "up_exclusive", ".Key starts with \"" TEST_TOPOLOGY_KEY_DEV_2_2_0_PREFIX "\"", "ParamValue4"},
    {TEST_TOPOLOGY_KEY_DEV_3_1_0, "up_exclusive", ".Key starts with \"" TEST_TOPOLOGY_KEY_DEV_3_1_0_PREFIX "\"", NULL},
    {TEST_TOPOLOGY_KEY_DEV_1_1_0, "up_exclusive", "", NULL}, //  no match
    {TEST_TOPOLOGY_KEY_DEV_3_1_0, "one_up", "", "ParamValue5"},
    {TEST_TOPOLOGY_KEY_DEV_3_1_0, "one_up", ".Key starts with \"" TEST_TOPOLOGY_KEY_DEV_1_1_0_PREFIX "\"", NULL},
    {TEST_TOPOLOGY_KEY_DEV_3_1_0, "one_up_exclusive", "", "ParamValue4"},
    {TEST_TOPOLOGY_KEY_DEV_3_1_0, "one_up_exclusive", ".Key starts with \"" TEST_TOPOLOGY_KEY_DEV_1_1_0_PREFIX "\"", NULL},
    {TEST_TOPOLOGY_KEY_DEV_1_1_0, "one_up_exclusive", "", NULL}, //  no match
};

/**
 * M = Master
 * A = Alternative
 *                                  __________
 *                                  |         |
 *                           _______|dev1.1.0 |_______
 *                           |      |_________|       |
 *                           |                        |
 *     ___________      _____V_____              _____V_____
 *     |         |A    M|         |              |         |
 *     |dev2.1.1 |<-----|dev2.1.0 |         _____|dev2.2.0 |_____
 *     |_________|      |_________|         |    |_________|     |
 *                                          |                    |
 *                                     _____V_____          _____V_____      ___________
 *                                     |         |          |         |M    A|         |
 *                                     |dev3.1.0 |          |dev3.2.0 |----->|dev3.2.1 |
 *                                     |_________|          |_________|      |_________|
 *
 * This function set params (some are dupplicated) on a device vice tree as depicted above
 */

static void add_obj_and_params_to_devices(void) {

    amxc_var_t devinfo;
    amxd_object_t* dev = NULL;
    uint32_t i;

    amxc_var_init(&devinfo);

    for(i = 0; i < NBRELEMS(device_param_config); i++) {
        dev = gmaps_get_device(device_param_config[i].device_name);
        assert_non_null(dev);
        util_add_string_param_to_obj(dev, "Param", device_param_config[i].parameter_value);
        gmaps_device_get(TEST_TOPOLOGY_KEY_DEV_1_1_0, &devinfo, 0);
        amxc_var_dump(&devinfo, 0);
        amxc_var_clean(&devinfo);
    }
}

void test_first_parameter_traverse(GMAPS_UNUSED void** state) {
    amxd_object_t* device = NULL;
    uint32_t i;
    amxc_var_t args;
    amxc_var_t ret;

    add_obj_and_params_to_devices();

    for(i = 0; i < NBRELEMS(first_param_test_instance); i++) {
        amxc_var_init(&args);
        amxc_var_init(&ret);
        amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
        device = gmaps_get_device(first_param_test_instance[i].orig_device_key);
        assert_non_null(device);
        amxc_var_add_key(cstring_t, &args, "parameter", "Param");
        amxc_var_add_key(cstring_t, &args, "expression", first_param_test_instance[i].expression);
        amxc_var_add_key(cstring_t, &args, "traverse", first_param_test_instance[i].traverse_mode);
        assert_int_equal(amxd_object_invoke_function(device,
                                                     "getFirstParameter",
                                                     &args,
                                                     &ret),
                         amxd_status_ok);
        amxc_var_dump(&ret, 0);
        printf("type_id: %d\n", ret.type_id);
        if(amxc_var_type_of(&ret) != AMXC_VAR_ID_CSTRING) {
            printf("test %d: no  param value found expected result: %s\n", i, first_param_test_instance[i].param_rslt_value);
            assert_null(first_param_test_instance[i].param_rslt_value);
        } else {
            const char* str_result;
            str_result = amxc_var_constcast(cstring_t, &ret);
            printf("test %d: param value result: %s  expected result: %s\n", i, str_result, first_param_test_instance[i].param_rslt_value);
            assert_string_equal(str_result, first_param_test_instance[i].param_rslt_value);
        }
        amxc_var_clean(&args);
        amxc_var_clean(&ret);
    }
}

void test_first_parameter_subobject_parameter(GMAPS_UNUSED void** state) {
    amxd_object_t* device = NULL;
    amxc_var_t args;
    amxc_var_t ret;
    const char* str_result = NULL;

    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);

    device = gmaps_get_device(TEST_TOPOLOGY_KEY_DEV_1_1_0);
    assert_non_null(device);
    amxd_object_t* subobject = NULL;
    amxd_object_new(&subobject, amxd_object_singleton, "MySubObject");
    util_add_string_param_to_obj(subobject, "MyParameter", "MyValue");
    assert_int_equal(amxd_object_add_object(device, subobject), amxd_status_ok);

    amxc_var_add_key(cstring_t, &args, "parameter", "MySubObject.MyParameter");
    amxc_var_add_key(cstring_t, &args, "expression", "");
    amxc_var_add_key(cstring_t, &args, "traverse", "this");
    assert_int_equal(amxd_object_invoke_function(device,
                                                 "getFirstParameter",
                                                 &args,
                                                 &ret),
                     amxd_status_ok);
    amxc_var_dump(&ret, 0);
    assert_true(amxc_var_type_of(&ret) == AMXC_VAR_ID_CSTRING);
    str_result = amxc_var_constcast(cstring_t, &ret);
    assert_non_null(str_result);
    assert_string_equal(str_result, "MyValue");
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_first_parameter_traverse_failed(GMAPS_UNUSED void** state) {
    amxc_var_t args;
    amxc_var_t ret;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "traverse", "down");
    amxc_var_add_key(cstring_t, &args, "expression", NULL);
    amxc_var_add_key(cstring_t, &args, "parameter", "bob");

    assert_int_equal(_Device_getFirstParameter(NULL, NULL, &args, &ret), amxd_status_unknown_error);

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}


void test_get_parameters_traverse_down(GMAPS_UNUSED void** state) {
    amxd_object_t* device = NULL;
    amxc_var_t args;
    amxc_var_t ret;
    amxc_var_t expected_ret;
    uint32_t i = 0;
    int res = 0;

    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);

    device = gmaps_get_device(TEST_TOPOLOGY_KEY_DEV_1_1_0);
    assert_non_null(device);
    amxc_var_add_key(cstring_t, &args, "parameter", "Param");
    amxc_var_add_key(cstring_t, &args, "expression", "");
    amxc_var_add_key(cstring_t, &args, "traverse", "down");
    assert_int_equal(amxd_object_invoke_function(device,
                                                 "getParameters",
                                                 &args,
                                                 &ret),
                     amxd_status_ok);

    amxc_var_init(&expected_ret);
    amxc_var_set_type(&expected_ret, AMXC_VAR_ID_HTABLE);
    for(i = 0; i < NBRELEMS(device_param_config); i++) {
        amxc_var_t var_value;
        amxc_var_t* var;
        var = amxc_var_add_new_key(&expected_ret, device_param_config[i].device_name);
        amxc_var_init(&var_value);
        amxc_var_set(cstring_t, &var_value, device_param_config[i].parameter_value);
        amxc_var_copy(var, &var_value);
        amxc_var_clean(&var_value);
    }
    amxc_var_dump(&expected_ret, 0);
    assert_int_equal(amxc_var_compare(&ret,
                                      &expected_ret,
                                      &res), 0);
    amxc_var_dump(&ret, 0);
    amxc_var_dump(&ret, 0);
    amxc_var_dump(&ret, 0);
    amxc_var_dump(&expected_ret, 0);
    assert_int_equal(res, 0);
    amxc_var_dump(&ret, 0);

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    amxc_var_clean(&expected_ret);
}

void test_get_parameters_traverse_failed(GMAPS_UNUSED void** state) {
    amxc_var_t args;
    amxc_var_t ret;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "traverse", "down");
    amxc_var_add_key(cstring_t, &args, "expression", NULL);
    amxc_var_add_key(cstring_t, &args, "parameter", "bob");

    assert_int_equal(_Device_getParameters(NULL, NULL, &args, &ret), amxd_status_unknown_error);

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}




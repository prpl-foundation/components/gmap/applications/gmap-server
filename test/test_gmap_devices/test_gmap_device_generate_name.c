/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <setjmp.h>
#include <cmocka.h>

#include "test_devices.h"
#include "../test_common/test_util.h"

static void check_generated_default_name(const char* device_key, const char* expected_default_name) {

    amxd_object_t* devices = amxd_dm_findf(amxut_bus_dm(), "Devices");
    amxd_object_t* device_instance = gmaps_get_device(device_key);

    assert_ptr_not_equal(devices, NULL);
    assert_ptr_not_equal(device_instance, NULL);

    amxd_object_t* source_obj = amxd_object_findf(device_instance, "Names.default");
    assert_ptr_not_equal(source_obj, NULL);
    char* name_read = amxd_object_get_value(cstring_t,
                                            source_obj,
                                            "Name",
                                            NULL);
    assert_string_equal(name_read, expected_default_name);
    free(name_read);

}

void test_rpc_create_devices_with_default_names(GMAPS_UNUSED void** state) {
    util_create_device("default1", "default", "self and hgw", false, "defaultname1", amxd_status_ok, NULL);
    check_generated_default_name("default1", "defaultname1");

    util_create_device("default2", "default", "self", false, "defaultname2", amxd_status_ok, NULL);
    check_generated_default_name("default2", "defaultname2");

    util_create_device("default3", "default", "usb", false, "defaultname3", amxd_status_ok, NULL);
    check_generated_default_name("default3", "defaultname3");

    util_create_device("default4", "default", "lan", false, "defaultname4", amxd_status_ok, NULL);
    check_generated_default_name("default4", "defaultname4");

    util_create_device("default5", "default", "whatever", false, "defaultname5", amxd_status_ok, NULL);
    check_generated_default_name("default5", "defaultname5");

    /* MULL parameter for tags not supported ? */
    /* util_create_device("default6", "default", NULL, false, "defaultname6", amxd_status_ok, NULL);
       check_generated_default_name("default6","defaultname6");
     */
    util_create_device("default7", "default", "", false, "defaultname7", amxd_status_ok, NULL);
    check_generated_default_name("default7", "defaultname7");

}

void test_rpc_create_devices_without_default_names(GMAPS_UNUSED void** state) {
    util_create_device("nodefault1", "default", "self and hgw", false, NULL, amxd_status_ok, NULL);
    check_generated_default_name("nodefault1", "HGW");

    util_create_device("nodefault8", "default", "self and hgw", false, "", amxd_status_ok, NULL);
    check_generated_default_name("nodefault8", "HGW");

    util_create_device("nodefault2", "default", "self", false, NULL, amxd_status_ok, NULL);
    check_generated_default_name("nodefault2", "nodefault2");

    util_create_device("nodefault3", "default", "usb", false, NULL, amxd_status_ok, NULL);
    check_generated_default_name("nodefault3", "usb-nodefault3");

    util_create_device("nodefault4", "default", "lan", false, NULL, amxd_status_ok, NULL);
    check_generated_default_name("nodefault4", "PC-nodefault4");

    util_create_device("nodefault5", "default", "whatever", false, NULL, amxd_status_ok, NULL);
    check_generated_default_name("nodefault5", "device nodefault5");
    /* no tags
       util_create_device("nodefault6", "default", NULL, false, NULL, amxd_status_ok, NULL);
       check_generated_default_name("nodefault6","device nodefault6");
     */
    util_create_device("nodefault7", "default", "", false, NULL, amxd_status_ok, NULL);
    check_generated_default_name("nodefault7", "device nodefault7");

}



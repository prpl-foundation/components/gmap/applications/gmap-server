/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <setjmp.h>
#include <cmocka.h>

#include "gmaps_priv.h"
#include "test_devices.h"
#include "../test_common/test_util.h"

#define GMAP_EVENT 120
static uint32_t event_rcvd = 0;
static uint32_t done_rcvd = 0;

static void util_test_remove_function(const char* key, const char* sub_object, const char* function) {

    amxd_object_t* device = NULL;
    amxc_var_t args;
    amxc_var_t ret;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    device = gmaps_get_device(key);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "function", function);

    if(sub_object && *sub_object) {
        amxc_var_add_key(cstring_t, &args, "subObject", sub_object);
    }

    assert_int_equal(amxd_object_invoke_function(device,
                                                 "removeFunction",
                                                 &args,
                                                 &ret),
                     amxd_status_ok);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);

}

static void util_test_set_function(const char* key, const char* sub_object, const char* function, amxc_var_t* data) {

    amxd_object_t* device = NULL;
    amxc_var_t args;
    amxc_var_t ret;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);

    device = gmaps_get_device(key);

    amxc_var_add_key(cstring_t, &args, "function", function);

    if(sub_object && *sub_object) {
        amxc_var_add_key(cstring_t, &args, "subObject", sub_object);
    }

    if(data) {
        amxc_var_t* data_var = amxc_var_add_new_key(&args, "data");
        amxc_var_copy(data_var, data);
    }

    assert_int_equal(amxd_object_invoke_function(device,
                                                 "setFunction",
                                                 &args,
                                                 &ret),
                     amxd_status_ok);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

static bool util_test_function_is_implemented(const char* key, const char* sub_object, const char* function) {
    amxd_object_t* device = NULL;
    amxc_var_t args;
    amxc_var_t ret;
    bool res = false;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);

    device = gmaps_get_device(key);

    amxc_var_add_key(cstring_t, &args, "function", function);

    if(sub_object && *sub_object) {
        amxc_var_add_key(cstring_t, &args, "subObject", sub_object);
    }

    assert_int_equal(amxd_object_invoke_function(device,
                                                 "isImplemented",
                                                 &args,
                                                 &ret),
                     amxd_status_ok);

    res = amxc_var_get_bool(&ret);

    amxc_var_clean(&args);
    amxc_var_clean(&ret);

    return res;
}

static void util_test_function_done(uint64_t id, amxd_status_t state, amxc_var_t* retval) {

    amxc_var_t args;
    amxc_var_t ret;
    amxd_object_t* devices = NULL;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(uint64_t, &args, "ID", id);
    amxc_var_add_key(uint32_t, &args, "status", state);
    amxc_var_t* retval_var = amxc_var_add_new_key(&args, "retval");
    amxc_var_copy(retval_var, retval);

    devices = amxd_dm_findf(amxut_bus_dm(), "Devices");

    assert_int_equal(amxd_object_invoke_function(devices,
                                                 "csiFinished",
                                                 &args,
                                                 &ret),
                     amxd_status_ok);

    amxc_var_clean(&args);
    amxc_var_clean(&ret);

    return;
}

static void gmap_deferred_cb(const amxc_var_t* const data,
                             GMAPS_UNUSED void* const priv) {
    printf("done rcvd\n");
    done_rcvd += 1;
    amxc_var_dump(data, 0);
}

static void gmap_event_slot(const char* const sig_name,
                            const amxc_var_t* const data,
                            GMAPS_UNUSED void* const priv) {
    if(!strcmp(sig_name, "gmap_event")) {
        amxc_var_dump(data, 0);
        uint32_t eventid = GET_UINT32(data, "EventId");
        uint64_t id;
        amxc_var_t retval;
        if(eventid == GMAP_EVENT) {
            event_rcvd += 1;

            amxc_var_dump(data, 0);
            amxc_var_t* event_data = GET_ARG(data, "Data");
            assert_non_null(event_data);
            amxc_var_init(&retval);
            amxc_var_set(uint32_t, &retval, 12);
            id = amxc_var_dyncast(uint64_t, event_data == NULL ? event_data : GET_ARG(event_data, "ID"));
            assert_int_equal(amxd_function_set_deferred_cb(id,
                                                           gmap_deferred_cb,
                                                           NULL), amxd_status_ok);
            util_test_function_done(id, amxd_status_ok, &retval);
            amxc_var_clean(&retval);
            _print_event(sig_name,
                         data,
                         NULL);
        }
    }
}
void test_gmap_device_set_function(GMAPS_UNUSED void** state) {

    amxd_object_t* test2 = NULL;
    const char* dev_key = "dev_with_function";
    util_create_device("dev_with_function", "", "", false, "", amxd_status_ok, NULL);
    amxc_var_t user_data;
    assert_int_equal(gmaps_device_set_tag(dev_key, "testfunction", "", 0), gmap_status_ok);
    // check mib is loaded
    test2 = amxd_object_get(gmaps_get_device(dev_key), "test2");
    assert_ptr_not_equal(test2, NULL);
    // check mib functions status

    handle_events();
    assert_false(util_test_function_is_implemented(dev_key, "", "dotest"));
    assert_false(util_test_function_is_implemented(dev_key, "test2", "dotest2"));

    // set functions
    amxc_var_init(&user_data);
    amxc_var_set(uint32_t, &user_data, 12);
    util_test_set_function(dev_key, NULL, "dotest", &user_data);
    util_test_set_function(dev_key, "test2", "dotest2", NULL);

    handle_events();

    // check mib functions status
    assert_true(util_test_function_is_implemented(dev_key, "", "dotest"));
    assert_true(util_test_function_is_implemented(dev_key, "test2", "dotest2"));

    amxc_var_clean(&user_data);
}

void test_gmap_execute_device_function(GMAPS_UNUSED void** state) {
    amxc_string_t expression;
    char* path = NULL;
    amxd_object_t* test_obj = NULL;
    amxd_object_t* device = NULL;
    amxc_var_t args;
    amxc_var_t ret;
    const char* dev_key = "dev_with_function";

    amxc_string_init(&expression, 0);
    device = gmaps_get_device(dev_key);
    path = amxd_object_get_path(device, AMXD_OBJECT_NAMED | AMXD_OBJECT_TERMINATE);
    amxc_string_setf(&expression, "object == \"%s\"", path);
    amxp_slot_connect(&(amxut_bus_dm()->sigmngr), "gmap_event", amxc_string_get(&expression, 0), gmap_event_slot, NULL);
    free(path);
    amxc_string_clean(&expression);

    handle_events();

    test_obj = gmaps_get_device(dev_key);
    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_init(&ret);
    assert_int_equal(amxd_object_invoke_function(test_obj,
                                                 "dotest",
                                                 &args,
                                                 &ret), amxd_status_deferred);
    handle_events();

    assert_int_equal(event_rcvd, 1);
    assert_int_equal(done_rcvd, 1);
    amxp_slot_disconnect(&(amxut_bus_dm()->sigmngr), "gmap_event", gmap_event_slot);

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_gmap_execute_device_subobject_function(GMAPS_UNUSED void** state) {
    amxc_string_t expression;
    char* path = NULL;
    amxd_object_t* test2_obj = NULL;
    amxd_object_t* device = NULL;
    amxc_var_t args;
    amxc_var_t ret;
    const char* dev_key = "dev_with_function";

    amxc_string_init(&expression, 0);
    device = gmaps_get_device(dev_key);
    path = amxd_object_get_path(device, AMXD_OBJECT_NAMED | AMXD_OBJECT_TERMINATE);
    amxc_string_setf(&expression, "object == \"%s\"", path);
    amxp_slot_connect(&(amxut_bus_dm()->sigmngr), "gmap_event", amxc_string_get(&expression, 0), gmap_event_slot, NULL);
    free(path);
    amxc_string_clean(&expression);

    handle_events();

    test2_obj = amxd_object_get(gmaps_get_device(dev_key), "test2");
    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "reason", "bob");
    amxc_var_add_key(uint32_t, &args, "value", 32);

    amxc_var_init(&ret);
    assert_int_equal(amxd_object_invoke_function(test2_obj,
                                                 "dotest2",
                                                 &args,
                                                 &ret), amxd_status_deferred);
    handle_events();

    assert_int_equal(event_rcvd, 2);
    assert_int_equal(done_rcvd, 2);
    amxp_slot_disconnect(&(amxut_bus_dm()->sigmngr), "gmap_event", gmap_event_slot);

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}


void test_gmap_device_remove_function(GMAPS_UNUSED void** state) {

    assert_true(util_test_function_is_implemented("dev_with_function", "", "dotest"));
    assert_true(util_test_function_is_implemented("dev_with_function", "test2", "dotest2"));

    // remove functions
    util_test_remove_function("dev_with_function", NULL, "dotest");
    util_test_remove_function("dev_with_function", "test2", "dotest2");

    handle_events();

    // check mib functions status
    assert_false(util_test_function_is_implemented("dev_with_function", "", "dotest"));
    assert_false(util_test_function_is_implemented("dev_with_function", "test2", "dotest2"));
}


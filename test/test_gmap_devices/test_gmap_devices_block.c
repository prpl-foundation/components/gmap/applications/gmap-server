/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdarg.h>
#include <setjmp.h>
#include <cmocka.h>

#include "test_devices.h"
#include "../test_common/test_util.h"
#include "gmaps_devices.h"
#include "gmaps_dm_devices.h"


void util_block_device(const char* key) {
    amxd_object_t* devices = amxd_dm_findf(amxut_bus_dm(), "Devices");
    amxc_var_t args;
    amxc_var_t ret;
    amxc_var_t params;

    assert_ptr_not_equal(devices, NULL);

    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxc_var_init(&params);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "key", key);

    assert_int_equal(amxd_object_invoke_function(devices,
                                                 "block",
                                                 &args,
                                                 &ret),
                     amxd_status_ok);

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    amxc_var_clean(&params);
}

void util_unblock_device(const char* key) {
    amxd_object_t* devices = amxd_dm_findf(amxut_bus_dm(), "Devices");
    amxc_var_t args;
    amxc_var_t ret;
    amxc_var_t params;

    assert_ptr_not_equal(devices, NULL);

    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxc_var_init(&params);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "key", key);

    assert_int_equal(amxd_object_invoke_function(devices,
                                                 "unblock",
                                                 &args,
                                                 &ret),
                     amxd_status_ok);

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    amxc_var_clean(&params);
}

bool util_is_blocked_device(const char* key) {
    amxd_object_t* devices = amxd_dm_findf(amxut_bus_dm(), "Devices");
    amxc_var_t args;
    amxc_var_t ret;
    amxc_var_t params;
    bool retval = false;

    assert_ptr_not_equal(devices, NULL);

    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxc_var_init(&params);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "key", key);

    assert_int_equal(amxd_object_invoke_function(devices,
                                                 "isBlocked",
                                                 &args,
                                                 &ret),
                     amxd_status_ok);

    retval = amxc_var_constcast(bool, &ret);

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    amxc_var_clean(&params);

    return retval;
}

void test_rpc_can_block_unblock_device(GMAPS_UNUSED void** state) {
    const char* key = "badDevice";

    // device should not exist yet
    assert_null(gmaps_get_device(key));

    // device should not be blocked yet
    assert_false(util_is_blocked_device(key));

    util_create_device(key, "wifi", "wifi suspicious",
                       false, "badDevice", amxd_status_ok, NULL);
    util_block_device(key);
    assert_true(util_is_blocked_device(key));

    // device should be removed
    assert_null(gmaps_get_device(key));

    // should not be able to create device
    util_create_device(key, "wifi", "wifi suspicious",
                       false, "badDevice", amxd_status_unknown_error, NULL);
    assert_null(gmaps_get_device(key));

    util_unblock_device(key);
    assert_false(util_is_blocked_device(key));

    // should again be able to create device
    util_create_device(key, "wifi", "wifi suspicious",
                       false, "badDevice", amxd_status_ok, NULL);
    assert_non_null(gmaps_get_device(key));

    util_block_device(key);
    assert_true(util_is_blocked_device(key));

    // device should be removed
    assert_null(gmaps_get_device(key));

    util_unblock_device(key);
    assert_false(util_is_blocked_device(key));
}

void test_rpc_can_block_device_already_blocked(GMAPS_UNUSED void** state) {
    const char* key = "badDevice";

    // device should not exist yet
    assert_null(gmaps_get_device(key));

    // device should not be blocked yet
    assert_false(util_is_blocked_device(key));

    util_create_device(key, "wifi", "wifi suspicious",
                       false, "badDevice", amxd_status_ok, NULL);
    util_block_device(key);
    assert_true(util_is_blocked_device(key));

    // device should be removed
    assert_null(gmaps_get_device(key));

    // block again
    util_block_device(key);
    assert_true(util_is_blocked_device(key));

    // device should still be removed
    assert_null(gmaps_get_device(key));

    // should not be able to create device
    util_create_device(key, "wifi", "wifi suspicious",
                       false, "badDevice", amxd_status_unknown_error, NULL);
    assert_null(gmaps_get_device(key));

    util_unblock_device(key);
    assert_false(util_is_blocked_device(key));
}

void test_rpc_LLTD_device_is_blocked(GMAPS_UNUSED void** state) {
    const char* key = "00:0D:3A:D7:F5:45";

    assert_true(util_is_blocked_device(key));
}

void test_rpc_can_unblock_device_already_unblocked(GMAPS_UNUSED void** state) {
    const char* key = "badDevice";

    // device should not exist yet
    assert_null(gmaps_get_device(key));

    // device should not be blocked yet
    assert_false(util_is_blocked_device(key));

    // unblock again
    util_unblock_device(key);
    assert_false(util_is_blocked_device(key));
}

void test_internal_block_device_input_validation(GMAPS_UNUSED void** state) {
    amxd_object_t* devices = amxd_dm_findf(amxut_bus_dm(), "Devices");
    amxc_var_t params;

    assert_ptr_not_equal(devices, NULL);
    amxc_var_init(&params);

    assert_int_equal(gmaps_block_device(NULL,
                                        "madeUpKey"),
                     gmap_status_invalid_parameter);
    assert_int_equal(gmaps_block_device(devices,
                                        NULL),
                     gmap_status_invalid_key);
    assert_int_equal(gmaps_block_device(devices,
                                        ""),
                     gmap_status_invalid_key);

    amxc_var_clean(&params);
}

void test_internal_unblock_device_input_validation(GMAPS_UNUSED void** state) {
    amxd_object_t* devices = amxd_dm_findf(amxut_bus_dm(), "Devices");
    amxc_var_t params;

    assert_ptr_not_equal(devices, NULL);
    amxc_var_init(&params);

    assert_int_equal(gmaps_unblock_device(NULL),
                     gmap_status_invalid_key);
    assert_int_equal(gmaps_unblock_device(""),
                     gmap_status_invalid_key);

    amxc_var_clean(&params);
}

void test_internal_is_blocked_device_input_validation(GMAPS_UNUSED void** state) {
    amxd_object_t* devices = amxd_dm_findf(amxut_bus_dm(), "Devices");
    amxc_var_t params;

    assert_ptr_not_equal(devices, NULL);
    amxc_var_init(&params);

    assert_false(gmaps_device_is_blocked(NULL));
    assert_false(gmaps_device_is_blocked(""));

    amxc_var_clean(&params);
}

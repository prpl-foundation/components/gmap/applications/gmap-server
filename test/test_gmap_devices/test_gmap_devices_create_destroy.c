/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdarg.h>
#include <setjmp.h>
#include <cmocka.h>

#include "test_devices.h"
#include "../test_common/test_util.h"
#include "gmaps_devices.h"
#include "gmaps_dm_devices.h"


void test_rpc_can_create_device_first(GMAPS_UNUSED void** state) {
    util_create_device(TESTKEY, "testDiscoverySource", "", false, "testDefaultName", amxd_status_ok, NULL);
}

void test_rpc_can_create_device_second(GMAPS_UNUSED void** state) {
    util_create_device(TEST2KEY, "test2DiscoverySource", "wifi", false, "test2DefaultName", amxd_status_ok, NULL);

}

void test_rpc_can_create_device_third(GMAPS_UNUSED void** state) {
    util_create_device(TEST3KEY, "test3DiscoverySource",
                       "test3Tag1 eth test3Tag2 wifi", false, "test3DefaultName", amxd_status_ok, NULL);
}

void test_rpc_can_update_tag_of_existing_device(GMAPS_UNUSED void** state) {
    bool tag = false;

    util_create_device(TEST3KEY, "test3DiscoverySource",
                       "test3Tag1 eth test3Tag2 wifi hgw", false, "test3DefaultName", amxd_status_ok, NULL);
    gmaps_device_has_tag(TEST3KEY, "hgw", "", gmap_traverse_this, &tag);
    assert_true(tag);
}

void test_rpc_cannot_update_restricted_tag_of_existing_device(GMAPS_UNUSED void** state) {
    /* The tags 'lan', 'physical' and 'self' cannot be set when the device already exists */
    bool tag = false;
    util_create_device_no_tag_comparison(TEST3KEY, "test3DiscoverySource",
                                         "test3Tag1 eth test3Tag2 wifi hgw physical", false, "test3DefaultName", amxd_status_ok, NULL);
    gmaps_device_has_tag(TEST3KEY, "physical", "", gmap_traverse_this, &tag);
    assert_false(tag);
}

void test_rpc_can_create_device_extra(GMAPS_UNUSED void** state) {
    amxc_var_t params;
    amxc_var_init(&params);
    amxc_var_set_type(&params, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &params, "Master", TEST2KEY);
    util_create_device("ID-00:11:13:0A:0B:10", "TEST", "lan protected edev mac physical eth ipv4 ipv6", false, "PC-01", amxd_status_ok, &params);
    amxc_var_clean(&params);

}

void test_rpc_can_create_device_temp(GMAPS_UNUSED void** state) {
    util_create_device("tempKey1", "tempDiscoverySource",
                       "tempTag1 tempTag2 wifi", false, "tempDefaultName", amxd_status_ok, NULL);
}

void test_internal_create_device_input_validation(GMAPS_UNUSED void** state) {
    amxd_object_t* devices = amxd_dm_findf(amxut_bus_dm(), "Devices");
    amxc_var_t params;

    assert_ptr_not_equal(devices, NULL);
    amxc_var_init(&params);

    assert_int_equal(gmaps_new_device(NULL,
                                      TESTKEY,
                                      "testDiscoverySource",
                                      "testTag",
                                      true,
                                      "testDefaultName",
                                      &params),
                     gmap_status_invalid_parameter);
    assert_int_equal(gmaps_new_device(devices,
                                      NULL,
                                      "testDiscoverySource",
                                      "testTag",
                                      true,
                                      "testDefaultName",
                                      &params),
                     gmap_status_invalid_key);
    assert_int_equal(gmaps_new_device(devices,
                                      "",
                                      "testDiscoverySource",
                                      "testTag",
                                      true,
                                      "testDefaultName",
                                      &params),
                     gmap_status_invalid_key);

    amxc_var_clean(&params);
}

void test_internal_destroy_device(GMAPS_UNUSED void** state) {
    amxd_object_t* devices = amxd_dm_findf(amxut_bus_dm(), "Devices");
    const char* dev_key = "tempTestDevice";
    const char* dev_name = "tempTestDeviceName";

    util_create_device(dev_key, "tempDiscoverySource",
                       "", false, dev_name, amxd_status_ok, NULL);

    assert_false(gmaps_device_name_table_is_index_available(dev_name, 0));
    assert_int_equal(gmaps_delete_device(devices, "tempTestDevice"),
                     gmap_status_ok);

    assert_null(gmaps_get_device(dev_key));
    assert_true(gmaps_device_name_table_is_index_available(dev_name, 0));
}

void test_rpc_can_destroy_device(GMAPS_UNUSED void** state) {
    amxd_object_t* devices = amxd_dm_findf(amxut_bus_dm(), "Devices");
    amxc_var_t args;
    amxc_var_t ret;

    assert_ptr_not_equal(devices, NULL);

    amxc_var_init(&args);
    amxc_var_init(&ret);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "key", "tempKey1");
    assert_int_equal(amxd_object_invoke_function(devices, "destroyDevice", &args, &ret), amxd_status_ok);
    assert_true(amxc_var_get_bool(&ret));

    amxd_object_t* instance = amxd_object_get_instance(amxd_object_get_child(devices, "Device"),
                                                       "tempKey1",
                                                       0);
    assert_null(instance);

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_del_can_destroy_device_on_name(GMAPS_UNUSED void** state) {
    /* This test runs with tests_separate_setup */
    amxd_object_t* device = amxd_dm_findf(amxut_bus_dm(), "Devices.Device.");
    amxd_object_t* instance = NULL;
    amxc_var_t args;
    amxc_var_t ret;

    assert_non_null(device);

    amxc_var_init(&args);
    amxc_var_init(&ret);

    util_create_device("tempKey2", "tempDiscoverySource",
                       "tempTag1 tempTag2 wifi", false, "tempDefaultName", amxd_status_ok, NULL);
    instance = amxd_object_get_instance(device, "tempKey2", 0);
    assert_non_null(instance);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "name", "tempKey2");
    assert_int_equal(amxd_object_invoke_function(device, "_del", &args, &ret), amxd_status_ok);
    assert_true(amxc_var_get_bool(&ret));

    instance = amxd_object_get_instance(amxd_object_get_child(device, "Device"),
                                        "tempKey2",
                                        0);
    assert_null(instance);

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_del_can_destroy_device_on_index(GMAPS_UNUSED void** state) {
    /* This test runs with tests_separate_setup */
    amxd_object_t* device = amxd_dm_findf(amxut_bus_dm(), "Devices.Device.");
    amxd_object_t* instance = NULL;
    amxc_var_t args;
    amxc_var_t ret;

    assert_non_null(device);

    amxc_var_init(&args);
    amxc_var_init(&ret);

    util_create_device("tempKey2", "tempDiscoverySource",
                       "tempTag1 tempTag2 wifi", false, "tempDefaultName", amxd_status_ok, NULL);
    instance = amxd_object_get_instance(device, "tempKey2", 0);
    assert_non_null(instance);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(uint32_t, &args, "index", amxd_object_get_index(instance));
    assert_int_equal(amxd_object_invoke_function(device, "_del", &args, &ret), amxd_status_ok);
    assert_true(amxc_var_get_bool(&ret));

    instance = amxd_object_get_instance(device, "tempKey2", 0);
    assert_null(instance);

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_rpc_can_destroy_device_removes_ipv4_lease(GMAPS_UNUSED void** state) {
    amxd_object_t* devices = amxd_dm_findf(amxut_bus_dm(), "Devices");
    amxd_object_t* instance = NULL;
    amxc_var_t args;
    amxc_var_t ret;
    amxc_var_t pathv4;
    amxc_var_t mac;
    const char* dev_key = "tempLeaseDevice";
    char* mac_address = "11:AA:22:BB:33:CC";
    char* mac_address_lower = "11:aa:22:bb:33:cc";

    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxc_var_init(&pathv4);
    amxc_var_init(&mac);

    assert_ptr_not_equal(devices, NULL);
    util_create_device(dev_key, "tempDiscoverySource",
                       "ipv4 ipv6 lan physical dhcp mac", false, NULL, amxd_status_ok, NULL);

    instance = amxd_object_get_instance(amxd_object_get_child(devices, "Device"),
                                        dev_key,
                                        0);
    assert_non_null(instance);

    amxc_var_set(cstring_t, &pathv4, "DHCPv4Server.Pool.1.Client.1.");
    assert_int_equal(amxd_object_set_param(instance, "DHCPv4Client", &pathv4), amxd_status_ok);
    amxc_var_set(cstring_t, &mac, mac_address);
    assert_int_equal(amxd_object_set_param(instance, "PhysAddress", &mac), amxd_status_ok);

    expect_check(__wrap_cleanClientLeases, MACs, test_util_check_mac_list, mac_address_lower);
    expect_string(__wrap_cleanClientLeases, object_path, "DHCPv4Server.Pool.1");

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "key", dev_key);
    assert_int_equal(amxd_object_invoke_function(devices, "destroyDevice", &args, &ret), amxd_status_ok);
    assert_true(amxc_var_get_bool(&ret));

    instance = amxd_object_get_instance(amxd_object_get_child(devices, "Device"),
                                        dev_key,
                                        0);
    assert_null(instance);

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    amxc_var_clean(&pathv4);
    amxc_var_clean(&mac);
}

void test_rpc_can_destroy_device_removes_ipv6_lease(GMAPS_UNUSED void** state) {
    amxd_object_t* devices = amxd_dm_findf(amxut_bus_dm(), "Devices");
    amxd_object_t* instance = NULL;
    amxc_var_t args;
    amxc_var_t ret;
    amxc_var_t pathv6;
    amxc_var_t mac;
    const char* dev_key = "tempLeaseDevice2";
    char* mac_address = "11:AA:22:BB:33:CC";
    char* mac_address_lower = "11:aa:22:bb:33:cc";

    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxc_var_init(&pathv6);
    amxc_var_init(&mac);

    assert_ptr_not_equal(devices, NULL);
    util_create_device(dev_key, "tempDiscoverySource",
                       "ipv4 ipv6 lan physical dhcp mac", false, NULL, amxd_status_ok, NULL);

    instance = amxd_object_get_instance(amxd_object_get_child(devices, "Device"),
                                        dev_key,
                                        0);
    assert_non_null(instance);

    amxc_var_set(cstring_t, &pathv6, "DHCPv6Server.Pool.1.Client.1.");
    assert_int_equal(amxd_object_set_param(instance, "DHCPv6Client", &pathv6), amxd_status_ok);
    amxc_var_set(cstring_t, &mac, mac_address);
    assert_int_equal(amxd_object_set_param(instance, "PhysAddress", &mac), amxd_status_ok);

    expect_check(__wrap_cleanClientLeases, MACs, test_util_check_mac_list, mac_address_lower);
    expect_string(__wrap_cleanClientLeases, object_path, "DHCPv6Server.Pool.1");

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "key", dev_key);
    assert_int_equal(amxd_object_invoke_function(devices, "destroyDevice", &args, &ret), amxd_status_ok);
    assert_true(amxc_var_get_bool(&ret));

    instance = amxd_object_get_instance(amxd_object_get_child(devices, "Device"),
                                        dev_key,
                                        0);
    assert_null(instance);

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    amxc_var_clean(&pathv6);
    amxc_var_clean(&mac);
}

void test_rpc_can_destroy_device_removes_both_leases(GMAPS_UNUSED void** state) {
    amxd_object_t* devices = amxd_dm_findf(amxut_bus_dm(), "Devices");
    amxd_object_t* instance = NULL;
    amxc_var_t args;
    amxc_var_t ret;
    amxc_var_t pathv4;
    amxc_var_t pathv6;
    amxc_var_t mac;
    const char* dev_key = "tempLeaseDevice3";
    char* mac_address = "11:AA:22:BB:33:CC";
    char* mac_address_lower = "11:aa:22:bb:33:cc";

    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxc_var_init(&pathv4);
    amxc_var_init(&pathv6);
    amxc_var_init(&mac);

    assert_ptr_not_equal(devices, NULL);
    util_create_device(dev_key, "tempDiscoverySource",
                       "ipv4 ipv6 lan physical dhcp mac", false, NULL, amxd_status_ok, NULL);

    instance = amxd_object_get_instance(amxd_object_get_child(devices, "Device"),
                                        dev_key,
                                        0);
    assert_non_null(instance);

    amxc_var_set(cstring_t, &pathv4, "DHCPv4Server.Pool.1.Client.1.");
    assert_int_equal(amxd_object_set_param(instance, "DHCPv4Client", &pathv4), amxd_status_ok);
    amxc_var_set(cstring_t, &pathv6, "DHCPv6Server.Pool.1.Client.1.");
    assert_int_equal(amxd_object_set_param(instance, "DHCPv6Client", &pathv6), amxd_status_ok);
    amxc_var_set(cstring_t, &mac, mac_address);
    assert_int_equal(amxd_object_set_param(instance, "PhysAddress", &mac), amxd_status_ok);

    expect_check(__wrap_cleanClientLeases, MACs, test_util_check_mac_list, mac_address_lower);
    expect_string(__wrap_cleanClientLeases, object_path, "DHCPv4Server.Pool.1");
    expect_check(__wrap_cleanClientLeases, MACs, test_util_check_mac_list, mac_address_lower);
    expect_string(__wrap_cleanClientLeases, object_path, "DHCPv6Server.Pool.1");

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "key", dev_key);
    assert_int_equal(amxd_object_invoke_function(devices, "destroyDevice", &args, &ret), amxd_status_ok);
    assert_true(amxc_var_get_bool(&ret));

    instance = amxd_object_get_instance(amxd_object_get_child(devices, "Device"),
                                        dev_key,
                                        0);
    assert_null(instance);

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    amxc_var_clean(&pathv4);
    amxc_var_clean(&pathv6);
    amxc_var_clean(&mac);
}

void test_rpc_lease_not_removed_empty_mac(GMAPS_UNUSED void** state) {
    amxd_object_t* devices = amxd_dm_findf(amxut_bus_dm(), "Devices");
    amxd_object_t* instance = NULL;
    amxc_var_t args;
    amxc_var_t ret;
    amxc_var_t pathv4;
    amxc_var_t mac;
    const char* dev_key = "tempLeaseDevice4";
    char* mac_address = "";

    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxc_var_init(&pathv4);
    amxc_var_init(&mac);

    assert_ptr_not_equal(devices, NULL);
    util_create_device(dev_key, "tempDiscoverySource",
                       "ipv4 ipv6 lan physical dhcp mac", false, NULL, amxd_status_ok, NULL);

    instance = amxd_object_get_instance(amxd_object_get_child(devices, "Device"),
                                        dev_key,
                                        0);
    assert_non_null(instance);

    amxc_var_set(cstring_t, &pathv4, "DHCPv4Server.Pool.1.Client.2.");
    assert_int_equal(amxd_object_set_param(instance, "DHCPv4Client", &pathv4), amxd_status_ok);
    amxc_var_set(cstring_t, &mac, mac_address);
    assert_int_equal(amxd_object_set_param(instance, "PhysAddress", &mac), amxd_status_ok);

    /* cleanClientLeases should not be called as no mac was passed  */
    // expect_check(__wrap_cleanClientLeases, MACs, test_util_check_mac_list, mac_address);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "key", dev_key);
    assert_int_equal(amxd_object_invoke_function(devices, "destroyDevice", &args, &ret), amxd_status_ok);
    assert_true(amxc_var_get_bool(&ret));

    instance = amxd_object_get_instance(amxd_object_get_child(devices, "Device"),
                                        dev_key,
                                        0);
    assert_null(instance);

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    amxc_var_clean(&pathv4);
    amxc_var_clean(&mac);
}

void test_internal_destroy_device_input_validation(GMAPS_UNUSED void** state) {
    amxd_object_t* devices = amxd_dm_findf(amxut_bus_dm(), "Devices");
    amxc_var_t params;

    assert_ptr_not_equal(devices, NULL);
    amxc_var_init(&params);

    assert_int_equal(gmaps_delete_device(NULL,
                                         TESTKEY),
                     gmap_status_invalid_parameter);
    assert_int_equal(gmaps_delete_device(devices,
                                         NULL),
                     gmap_status_invalid_key);
    assert_int_equal(gmaps_delete_device(devices,
                                         ""),
                     gmap_status_invalid_key);

    amxc_var_clean(&params);
}

void test_internal_destroy_device_self(GMAPS_UNUSED void** state) {
    amxd_object_t* devices = amxd_dm_findf(amxut_bus_dm(), "Devices");
    const char* dev_name = "destroytest_self_device";
    amxd_object_t* dev = NULL;

    util_create_device(dev_name, "test", "self", false, NULL, amxd_status_ok, NULL);
    dev = gmaps_get_device(dev_name);

    assert_non_null(dev);

    assert_int_equal(gmaps_delete_device(devices,
                                         dev_name),
                     gmap_status_ok);

    assert_null(gmaps_get_device(dev_name));
}

void test_internal_destroy_device_self_hgw(GMAPS_UNUSED void** state) {
    amxd_object_t* devices = amxd_dm_findf(amxut_bus_dm(), "Devices");
    const char* dev_name = "destroytest_self_hgw_device";
    amxd_object_t* dev = NULL;

    util_create_device(dev_name, "test", "self hgw", false, NULL, amxd_status_ok, NULL);
    dev = gmaps_get_device(dev_name);

    assert_non_null(dev);

    assert_int_equal(gmaps_delete_device(devices,
                                         dev_name),
                     gmap_status_invalid_request);

    assert_ptr_equal(dev, gmaps_get_device(dev_name));
}

void test_rpc_refuse_create_device_with_invalid_key(GMAPS_UNUSED void** state) {
    util_create_device("123startwithnumber", "testDiscoverySource", "", false, "testDefaultName", amxd_status_unknown_error, NULL);
}
/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <setjmp.h>
#include <cmocka.h>

#include "test_devices.h"
#include "../test_common/test_util.h"
#include "gmaps_dm_device.h"

/**
 * Helper function to add the given @param name from @param source to @param device.
 * After adding the name, it will readback the data model to check correctness.
 * The name, source and suffix are checked.
 * If @param expected_suffix_char is not null, it is used as the expected suffix.
 * Else, @param expected_suffix_uint is used.
 */
void util_set_name(const char* const key,
                   const char* const name,
                   const char* const source,
                   const char* const expected_name,
                   const char* const expected_source,
                   const char* const expected_suffix_char,
                   uint32_t expected_suffix_uint) {
    amxd_object_t* devices = amxd_dm_findf(amxut_bus_dm(), "Devices");
    amxd_object_t* device_instance = gmaps_get_device(key);
    amxc_var_t args;
    amxc_var_t ret;

    assert_ptr_not_equal(devices, NULL);
    assert_ptr_not_equal(device_instance, NULL);
    assert_false((expected_suffix_char != NULL) && (expected_suffix_uint != 0));

    amxc_var_init(&args);
    amxc_var_init(&ret);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "name", name);
    amxc_var_add_key(cstring_t, &args, "source", source);

    assert_int_equal(amxd_object_invoke_function(device_instance,
                                                 "setName",
                                                 &args,
                                                 &ret),
                     amxd_status_ok);

    // readback
    amxd_object_t* source_obj = amxd_object_findf(device_instance, "Names.%s", expected_source);
    char* name_read = amxd_object_get_value(cstring_t,
                                            source_obj,
                                            "Name",
                                            NULL);
    char* source_read = amxd_object_get_value(cstring_t,
                                              source_obj,
                                              "Source",
                                              NULL);
    assert_string_equal(name_read, expected_name);
    assert_string_equal(source_read, expected_source);
    if(expected_suffix_char == NULL) {
        uint32_t suffix = amxd_object_get_value(uint32_t,
                                                source_obj,
                                                "Suffix",
                                                NULL);
        assert_int_equal(suffix, expected_suffix_uint);
    } else {
        char* suffix = amxd_object_get_value(cstring_t,
                                             source_obj,
                                             "Suffix",
                                             NULL);
        assert_string_equal(suffix, expected_suffix_char);
        free(suffix);
    }

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    free(name_read);
    free(source_read);
}

void test_rpc_can_set_name(GMAPS_UNUSED void** state) {
    // Without index given.
    util_set_name(TESTKEY, "deviceName1", "wifi",
                  "deviceName1", "wifi", "", 0);
    // Same, using default source "webui".
    util_set_name(TESTKEY, "deviceName2-my-pc", "",
                  "deviceName2-my-pc", "webui", "", 0);
    // Same, using default source "webui".
    util_set_name(TESTKEY, "deviceName2-my-pc", NULL,
                  "deviceName2-my-pc", "webui", "", 0);

    // With index given, valid.
    util_set_name(TESTKEY, "indexedName-2", "bluetooth",
                  "indexedName", "bluetooth", NULL, 2);

    // With index given, invalid because in use by first device --> should apply first available index!
    // This is zero.
    // Note: we are using another device here.
    util_set_name(TEST2KEY, "indexedName-2", "bluetooth",
                  "indexedName", "bluetooth", "", 0);
    // Same, should use next available index, which is 1.
    util_set_name(TEST3KEY, "indexedName-2", "bluetooth",
                  "indexedName", "bluetooth", NULL, 1);


    // Change name from source "wifi".
    util_set_name(TESTKEY, "deviceName1Wifi", "wifi",
                  "deviceName1Wifi", "wifi", "", 0);

    // Add device name that already exists for the device under a different source.
    // Without index given.
    // So it should re-use the existing index, which is 2.
    util_set_name(TESTKEY, "indexedName", "ethernet",
                  "indexedName", "ethernet", NULL, 2);

    // New name and old name the same --> should take index of old name.
    // Without index given.
    util_set_name(TESTKEY, "deviceName1Wifi", "wifi",
                  "deviceName1Wifi", "wifi", "", 0);

    // New name and old name the same --> should take index of old name.
    util_set_name(TESTKEY, "deviceName1Wifi", "wifi",
                  "deviceName1Wifi", "wifi", "", 0);
    // Check that name with suffix 1 has not been created.
    assert_true(gmaps_device_name_table_is_index_available("deviceName1Wifi", 1));
    assert_false(gmaps_device_name_table_is_index_available("deviceName1Wifi", 0));

    // Check that existing same names as the new name, within the same device of
    // different sources get the same index number as the one that's added.
    util_set_name(TESTKEY, "deviceName3-5", "zigbee",
                  "deviceName3", "zigbee", NULL, 5);
    assert_false(gmaps_device_name_table_is_index_available("deviceName3", 5));
    assert_true(gmaps_device_name_table_is_index_available("deviceName3", 0));
    util_set_name(TESTKEY, "deviceName3", "LoRaWAN",
                  "deviceName3", "LoRaWAN", "", 0);
    assert_true(gmaps_device_name_table_is_index_available("deviceName3", 5));
    assert_false(gmaps_device_name_table_is_index_available("deviceName3", 0));

    // TODO: extend this test with a check of generated events after implementing event handling.
}

void test_rpc_cannot_set_duplicate_name(GMAPS_UNUSED void** state) {
    amxd_object_t* device_instance = gmaps_get_device(TESTKEY);
    amxd_object_t* source_obj = amxd_object_findf(device_instance, "Names.wifi");
    GMAPS_UNUSED uint32_t index;

    assert_true(gmaps_device_name_table_is_index_available("deviceName1", 0));
    assert_true(gmaps_device_name_table_is_index_available("deviceName1", 1));

    assert_non_null(source_obj);
    index = amxd_object_get_index(source_obj);

    util_set_name(TEST2KEY, "deviceName1", "wifi",
                  "deviceName1", "wifi", NULL, 0);

    assert_false(gmaps_device_name_table_is_index_available("deviceName1", 0));

    util_set_name(TEST3KEY, "deviceName1", "wifi",
                  "deviceName1", "wifi", NULL, 1);

    assert_false(gmaps_device_name_table_is_index_available("deviceName1", 1));

}

void test_rpc_can_set_duplicate_name_in_self_device(GMAPS_UNUSED void** state) {
    amxd_object_t* device_instance = gmaps_get_device(TESTKEY);
    amxd_object_t* source_obj = amxd_object_findf(device_instance, "Names.wifi");
    GMAPS_UNUSED uint32_t index;
    char* self_dev = "selfDevice";

    util_create_device(self_dev, "testDiscoverySource", "self", false, "testSelfName", amxd_status_ok, NULL);


    assert_false(gmaps_device_name_table_is_index_available("deviceName1", 1));
    assert_true(gmaps_device_name_table_is_index_available("deviceName1", 2));

    assert_non_null(source_obj);
    index = amxd_object_get_index(source_obj);

    util_set_name(self_dev, "deviceName1", "wifi",
                  "deviceName1", "wifi", NULL, 0);

    assert_true(gmaps_device_name_table_is_index_available("deviceName1", 2));

}

void test_rpc_can_set_duplicate_name_in_upnp_device(GMAPS_UNUSED void** state) {
    amxd_object_t* device_instance = gmaps_get_device(TESTKEY);
    amxd_object_t* source_obj = amxd_object_findf(device_instance, "Names.wifi");
    GMAPS_UNUSED uint32_t index;
    char* upnp_dev = "upnpDevice";

    util_create_device(upnp_dev, "testDiscoverySource", "upnp logical", false, "testSelfName", amxd_status_ok, NULL);


    assert_false(gmaps_device_name_table_is_index_available("deviceName1", 1));
    assert_true(gmaps_device_name_table_is_index_available("deviceName1", 2));

    assert_non_null(source_obj);
    index = amxd_object_get_index(source_obj);

    util_set_name(upnp_dev, "deviceName1", "wifi",
                  "deviceName1", "wifi", NULL, 0);

    assert_true(gmaps_device_name_table_is_index_available("deviceName1", 2));

}

void test_internal_set_name_input_validation(GMAPS_UNUSED void** state) {
    amxd_object_t* device_instance = gmaps_get_device(TESTKEY);

    assert_int_equal(gmaps_device_set_name(NULL, "madeUpName", "madeUpSource"),
                     gmap_status_invalid_parameter);
    assert_int_equal(gmaps_device_set_name(device_instance, "", "madeUpSource"),
                     gmap_status_invalid_parameter);
    assert_int_equal(gmaps_device_set_name(device_instance, NULL, "madeUpSource"),
                     gmap_status_invalid_parameter);
    assert_int_equal(gmaps_device_set_name(device_instance, "madeUpName", ""),
                     gmap_status_invalid_parameter);
    assert_int_equal(gmaps_device_set_name(device_instance, "madeUpName", NULL),
                     gmap_status_invalid_parameter);
}

void test_rpc_can_remove_name(GMAPS_UNUSED void** state) {

    amxc_var_t args;
    amxc_var_t ret;

    amxd_object_t* source_obj = NULL;
    amxd_object_t* devices = amxd_dm_findf(amxut_bus_dm(), "Devices");
    amxd_object_t* device_instance = gmaps_get_device(TESTKEY);
    assert_ptr_not_equal(devices, NULL);
    assert_ptr_not_equal(device_instance, NULL);

    amxc_var_init(&args);
    amxc_var_init(&ret);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "source", "LoRaWAN");

    assert_int_equal(amxd_object_invoke_function(device_instance,
                                                 "removeName",
                                                 &args,
                                                 &ret),
                     amxd_status_ok);
    source_obj = amxd_object_findf(device_instance, "Names.%s", "LoRaWAN");
    assert_ptr_equal(source_obj, NULL);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <setjmp.h>
#include <cmocka.h>

#include "test_devices.h"
#include "gmaps_devices.h"
#include "gmaps_dm_devices.h"
#include "../test_common/test_util.h"

/**
 * Links devices with the given parameters and performs some sanity checks on the result.
 * The provided parameters should be valid.
 * @param new_link  If true, the function will check the new link. Else, it will check
 *                  that no new link has been created.
 */
static void util_chosenlink_link_devices_ext(bool replace, const char* upper_key, const char* lower_key, bool new_link) {
    amxc_var_t params;
    amxd_object_t* devices = amxd_dm_findf(amxut_bus_dm(), "Devices");
    amxd_object_t* upper = amxd_object_get_instance(amxd_object_get_child(devices, "Device"),
                                                    upper_key, 0);
    amxd_object_t* upper_ldevice_template = amxd_object_findf(upper, "LDevice");
    amxd_object_t* upper_udevice_template = amxd_object_findf(upper, "UDevice");
    uint32_t udevice_llink_count = amxd_object_get_instance_count(upper_ldevice_template);
    uint32_t udevice_ulink_count = amxd_object_get_instance_count(upper_udevice_template);

    amxd_object_t* lower = amxd_object_get_instance(amxd_object_get_child(devices, "Device"),
                                                    lower_key, 0);
    amxd_object_t* lower_ldevice_template = amxd_object_findf(lower, "LDevice");
    amxd_object_t* lower_udevice_template = amxd_object_findf(lower, "UDevice");
    uint32_t ldevice_llink_count = amxd_object_get_instance_count(lower_ldevice_template);
    uint32_t ldevice_ulink_count = amxd_object_get_instance_count(lower_udevice_template);
    gmap_status_t status = gmap_status_unknown_error;

    amxc_var_init(&params);

    assert_ptr_not_equal(devices, NULL);

    status = gmaps_devices_chosenlink_make(upper_key, lower_key, "default", replace);
    assert_int_equal(status, gmap_status_ok);

    // readback, verify, dump of upper device
    assert_int_equal(amxd_object_get_instance_count(upper_ldevice_template),
                     udevice_llink_count + (uint32_t) new_link);
    assert_int_equal(amxd_object_get_instance_count(upper_udevice_template),
                     udevice_ulink_count);

    amxd_object_t* upper_ldevice = amxd_object_get_instance(upper_ldevice_template,
                                                            lower_key, 0);
    const char* upper_ldevice_name = amxd_object_get_name(upper_ldevice, AMXD_OBJECT_NAMED);
    assert_string_equal(upper_ldevice_name, lower_key);
    amxd_object_get_params(upper_ldevice, &params, amxd_dm_access_protected);
    amxc_var_dump(&params, STDOUT_FILENO);

    // readback, verify, dump of lower device
    assert_int_equal(amxd_object_get_instance_count(lower_ldevice_template),
                     ldevice_llink_count);
    if(!replace) {
        assert_int_equal(amxd_object_get_instance_count(lower_udevice_template),
                         ldevice_ulink_count + (uint32_t) new_link);
    } else {
        assert_int_equal(amxd_object_get_instance_count(lower_udevice_template),
                         1);
    }
    amxd_object_t* lower_udevice = amxd_object_get_instance(lower_udevice_template,
                                                            upper_key, 0);
    assert_non_null(lower_udevice);
    const char* lower_udevice_name = amxd_object_get_name(lower_udevice, AMXD_OBJECT_NAMED);
    assert_string_equal(lower_udevice_name, upper_key);
    amxd_object_get_params(lower_udevice, &params, amxd_dm_access_protected);
    amxc_var_dump(&params, STDOUT_FILENO);
    printf("UDevice [%s] has LDevice [%s]\n", lower_udevice_name, upper_ldevice_name);

    amxc_var_clean(&params);
}

/**
 * This sets the chosenlink, bypasses the highlevel API of multilink, so use with care.
 *
 * Normally you want to use gmaps_devices_multilink_add() instead.
 * Mostly exists to test lower-level parts (that are underneath multilink), or to test
 * behavior for inconsistent states that cannot be obtained by using the multilink API.
 */
void util_chosenlink_link_devices(const char* upper_key, const char* lower_key, bool new_link) {
    util_chosenlink_link_devices_ext(false, upper_key, lower_key, new_link);
}

static void util_chosenlink_replace(const char* upper_key, const char* lower_key, bool new_link) {
    util_chosenlink_link_devices_ext(true, upper_key, lower_key, new_link);
}

void test_chosenlink_can_link_devices(GMAPS_UNUSED void** state) {
    util_chosenlink_link_devices(TESTKEY, TEST2KEY, true);
    amxd_object_t* dev1 = gmaps_get_device(TESTKEY);
    amxd_object_t* dev2 = gmaps_get_device(TEST2KEY);
    assert_true(gmaps_device_is_linked_to(dev1, dev2, gmap_traverse_down));
    assert_true(gmaps_device_is_linked_to(dev2, dev1, gmap_traverse_up));

    amxc_var_t args;
    amxc_var_t ret;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "device", TEST2KEY);
    amxc_var_add_key(cstring_t, &args, "traverse", "down");

    assert_int_equal(amxd_object_invoke_function(dev1,
                                                 "isLinkedTo",
                                                 &args,
                                                 &ret),
                     amxd_status_ok);

    // check return value
    assert_true(amxc_var_get_bool(&ret));

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_chosenlink_can_link_devices_again_same(GMAPS_UNUSED void** state) {
    util_chosenlink_link_devices(TESTKEY, TEST2KEY, false);

    amxd_object_t* dev1 = gmaps_get_device(TESTKEY);
    amxd_object_t* dev2 = gmaps_get_device(TEST2KEY);

    assert_true(gmaps_device_is_linked_to(dev1, dev2, gmap_traverse_down));
    assert_true(gmaps_device_is_linked_to(dev2, dev1, gmap_traverse_up));

    gmaps_devices_chosenlink_remove(TESTKEY, TEST2KEY);

    assert_false(gmaps_device_is_linked_to(dev1, dev2, gmap_traverse_down));
    assert_false(gmaps_device_is_linked_to(dev2, dev1, gmap_traverse_up));

    util_chosenlink_link_devices(TESTKEY, TEST2KEY, true);

    gmaps_devices_chosenlink_removeAllLLink(dev1, NULL);
    gmaps_devices_chosenlink_removeAllULink(dev2, NULL);

    assert_false(gmaps_device_is_linked_to(dev1, dev2, gmap_traverse_down));
    assert_false(gmaps_device_is_linked_to(dev2, dev1, gmap_traverse_up));
}

void test_chosenlink_can_replace_link(GMAPS_UNUSED void** state) {
    const char* dev1name = "linktest_dev1\" || \"a\" == \"b.dot.";
    const char* dev2name = "linktest_dev2\" || \"a\" == \"b.dot.";
    const char* dev3name = "linktest_dev3\" || \"a\" == \"b.dot.";
    // GIVEN three devices and device1 is linked to device2
    util_create_device(dev1name, "test", "", false, NULL, amxd_status_ok, NULL);
    util_create_device(dev2name, "test", "", false, NULL, amxd_status_ok, NULL);
    util_create_device(dev3name, "test", "", false, NULL, amxd_status_ok, NULL);
    amxd_object_t* dev1 = gmaps_get_device(dev1name);
    amxd_object_t* dev2 = gmaps_get_device(dev2name);
    amxd_object_t* dev3 = gmaps_get_device(dev3name);

    util_chosenlink_replace(dev1name, dev2name, true);
    handle_events();

    assert_true(gmaps_device_is_linked_to(dev1, dev2, gmap_traverse_down));
    assert_false(gmaps_device_is_linked_to(dev1, dev3, gmap_traverse_down));
    assert_false(gmaps_device_is_linked_to(dev2, dev1, gmap_traverse_down));
    assert_false(gmaps_device_is_linked_to(dev2, dev3, gmap_traverse_down));
    assert_false(gmaps_device_is_linked_to(dev3, dev1, gmap_traverse_down));
    assert_false(gmaps_device_is_linked_to(dev3, dev2, gmap_traverse_down));

    assert_false(gmaps_device_is_linked_to(dev1, dev2, gmap_traverse_up));
    assert_false(gmaps_device_is_linked_to(dev1, dev3, gmap_traverse_up));
    assert_true(gmaps_device_is_linked_to(dev2, dev1, gmap_traverse_up));
    assert_false(gmaps_device_is_linked_to(dev2, dev3, gmap_traverse_up));
    assert_false(gmaps_device_is_linked_to(dev3, dev1, gmap_traverse_up));
    assert_false(gmaps_device_is_linked_to(dev3, dev2, gmap_traverse_up));

    // WHEN linking the lower device to another upper device, replacing the existing link
    util_chosenlink_replace(dev3name, dev2name, true);

    // THEN the old link is gone (dev1<=>dev2) and the new link is introduced (dev3<=>dev2)
    assert_false(gmaps_device_is_linked_to(dev1, dev2, gmap_traverse_down)); // <-- link gone
    assert_false(gmaps_device_is_linked_to(dev1, dev3, gmap_traverse_down));
    assert_false(gmaps_device_is_linked_to(dev2, dev1, gmap_traverse_down));
    assert_false(gmaps_device_is_linked_to(dev2, dev3, gmap_traverse_down));
    assert_false(gmaps_device_is_linked_to(dev3, dev1, gmap_traverse_down));
    assert_true(gmaps_device_is_linked_to(dev3, dev2, gmap_traverse_down)); // <-- new link

    assert_false(gmaps_device_is_linked_to(dev1, dev2, gmap_traverse_up));
    assert_false(gmaps_device_is_linked_to(dev1, dev3, gmap_traverse_up));
    assert_false(gmaps_device_is_linked_to(dev2, dev1, gmap_traverse_up)); // <-- link gone
    assert_true(gmaps_device_is_linked_to(dev2, dev3, gmap_traverse_up));  // <-- new link
    assert_false(gmaps_device_is_linked_to(dev3, dev1, gmap_traverse_up));
    assert_false(gmaps_device_is_linked_to(dev3, dev2, gmap_traverse_up));

    amxd_object_t* devices = amxd_dm_findf(amxut_bus_dm(), "Devices");
    assert_int_equal(gmaps_delete_device(devices, dev1name), gmap_status_ok);
    assert_int_equal(gmaps_delete_device(devices, dev2name), gmap_status_ok);
    assert_int_equal(gmaps_delete_device(devices, dev3name), gmap_status_ok);
}

void test_chosenlink_can_link_devices_using_linkcmd(GMAPS_UNUSED void** state) {

    amxd_object_t* dev1 = gmaps_get_device(TESTKEY);
    amxd_object_t* dev2 = gmaps_get_device(TEST2KEY);
    amxd_object_t* dev3 = gmaps_get_device(TEST3KEY);

    util_chosenlink_replace(TESTKEY, TEST2KEY, true);
    assert_true(gmaps_device_is_linked_to(dev1, dev2, gmap_traverse_down));
    assert_true(gmaps_device_is_linked_to(dev2, dev1, gmap_traverse_up));
    util_chosenlink_replace(TESTKEY, TEST3KEY, true);
    assert_true(gmaps_device_is_linked_to(dev1, dev3, gmap_traverse_down));
    assert_true(gmaps_device_is_linked_to(dev3, dev1, gmap_traverse_up));

    gmaps_devices_chosenlink_removeAllULink(dev2, NULL);
    gmaps_devices_chosenlink_removeAllULink(dev3, NULL);
    gmaps_devices_chosenlink_removeAllLLink(dev1, NULL);

    assert_false(gmaps_device_is_linked_to(dev1, dev2, gmap_traverse_down));
    assert_false(gmaps_device_is_linked_to(dev1, dev3, gmap_traverse_down));
    assert_false(gmaps_device_is_linked_to(dev2, dev1, gmap_traverse_up));
    assert_false(gmaps_device_is_linked_to(dev3, dev1, gmap_traverse_up));
}

void test_chosenlink_link_devices_input_validation(GMAPS_UNUSED void** state) {
    assert_non_null(gmaps_get_device(TEST2KEY));
    assert_null(gmaps_get_device("madeUpKey"));
    assert_int_not_equal(gmap_status_ok, gmaps_devices_chosenlink_make("madeUpKey", TEST2KEY, "default", false));
}

void test_chosenlink_internal_link_devices_input_validation(GMAPS_UNUSED void** state) {
    assert_int_equal(gmaps_devices_chosenlink_make(NULL,
                                                   TEST2KEY,
                                                   "testType",
                                                   false),
                     gmap_status_device_not_found);
    assert_int_equal(gmaps_devices_chosenlink_make(TESTKEY,
                                                   NULL,
                                                   "testType",
                                                   false),
                     gmap_status_device_not_found);
    assert_int_equal(gmaps_devices_chosenlink_make(TESTKEY,
                                                   TEST2KEY,
                                                   NULL,
                                                   false),
                     gmap_status_invalid_parameter);
}

void test_chosenlink_remove_all_upper_links(GMAPS_UNUSED void** state) {
    const char* dev1name = "linktest_dev1\" || \"a\" == \"b.dot.";
    const char* dev2name = "linktest_dev2\" || \"a\" == \"b.dot.";
    const char* dev3name = "linktest_dev3\" || \"a\" == \"b.dot.";
    const char* dev4name = "linktest_dev4\" || \"a\" == \"b.dot.";
    amxd_object_t* dev1 = NULL;
    amxd_object_t* dev3 = NULL;
    amxd_object_t* dev4 = NULL;

    util_create_device(dev1name, "test", "", false, NULL, amxd_status_ok, NULL);
    util_create_device(dev2name, "test", "", false, NULL, amxd_status_ok, NULL);
    util_create_device(dev3name, "test", "", false, NULL, amxd_status_ok, NULL);
    util_create_device(dev4name, "test", "", false, NULL, amxd_status_ok, NULL);

    handle_events();

    util_chosenlink_replace(dev1name, dev2name, true);
    util_chosenlink_replace(dev2name, dev3name, true);
    util_chosenlink_replace(dev2name, dev4name, true);
    handle_events();

    dev1 = gmaps_get_device(dev1name);
    dev3 = gmaps_get_device(dev3name);
    dev4 = gmaps_get_device(dev4name);

    assert_non_null(amxd_object_findf(dev1, "LDevice.[Alias=='%s']", dev2name));
    assert_non_null(amxd_object_findf(dev3, "UDevice.[Alias=='%s']", dev2name));
    assert_non_null(amxd_object_findf(dev4, "UDevice.[Alias=='%s']", dev2name));

    gmaps_devices_chosenlink_remove("", dev2name);
    handle_events();

    assert_null(amxd_object_findf(dev1, "LDevice.[Alias=='%s']", dev2name));
    assert_non_null(amxd_object_findf(dev3, "UDevice.[Alias=='%s']", dev2name));
    assert_non_null(amxd_object_findf(dev4, "UDevice.[Alias=='%s']", dev2name));
}

void test_chosenlink_remove_all_lower_links(GMAPS_UNUSED void** state) {
    const char* dev1name = "linktest_dev1\" || \"a\" == \"b.dot.";
    const char* dev2name = "linktest_dev2\" || \"a\" == \"b.dot.";
    const char* dev3name = "linktest_dev3\" || \"a\" == \"b.dot.";
    const char* dev4name = "linktest_dev4\" || \"a\" == \"b.dot.";
    amxd_object_t* dev1 = NULL;
    amxd_object_t* dev3 = NULL;
    amxd_object_t* dev4 = NULL;
    amxd_object_t* devices = NULL;

    util_chosenlink_replace(dev1name, dev2name, true);
    handle_events();

    dev1 = gmaps_get_device(dev1name);
    dev3 = gmaps_get_device(dev3name);
    dev4 = gmaps_get_device(dev4name);

    assert_non_null(amxd_object_findf(dev1, "LDevice.[Alias=='%s']", dev2name));
    assert_non_null(amxd_object_findf(dev3, "UDevice.[Alias=='%s']", dev2name));
    assert_non_null(amxd_object_findf(dev4, "UDevice.[Alias=='%s']", dev2name));

    gmaps_devices_chosenlink_remove(dev2name, "");
    handle_events();

    assert_non_null(amxd_object_findf(dev1, "LDevice.[Alias=='%s']", dev2name));
    assert_null(amxd_object_findf(dev3, "UDevice.[Alias=='%s']", dev2name));
    assert_null(amxd_object_findf(dev4, "UDevice.[Alias=='%s']", dev2name));

    devices = amxd_dm_findf(amxut_bus_dm(), "Devices");
    assert_int_equal(gmaps_delete_device(devices, dev1name), gmap_status_ok);
    assert_int_equal(gmaps_delete_device(devices, dev2name), gmap_status_ok);
    assert_int_equal(gmaps_delete_device(devices, dev3name), gmap_status_ok);
    assert_int_equal(gmaps_delete_device(devices, dev4name), gmap_status_ok);
    handle_events();
}

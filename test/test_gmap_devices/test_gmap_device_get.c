/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <setjmp.h>
#include <cmocka.h>

#include "test_devices.h"
#include "../test_common/test_util.h"
#include "gmaps_dm_device.h"
#include "gmaps_device_get.h"


GMAPS_UNUSED static void test_gmap_can_device_get_flags(uint32_t flags) {
    amxc_var_t devinfo;
    amxc_var_init(&devinfo);

    assert_int_equal(gmaps_device_get(TEST2KEY, &devinfo, flags), gmap_status_ok);

    amxc_var_clean(&devinfo);
}


void test_gmap_can_device_get(GMAPS_UNUSED void** state) {
    const char* dev_name = TEST2KEY;
    amxd_object_t* device = NULL;
    amxc_var_t args;
    amxc_var_t args2;
    amxc_var_t ret;
    amxc_var_t ret2;
    amxc_var_t devinfo;
    amxc_var_t* llinks = NULL;

    amxc_var_init(&args);
    amxc_var_init(&args2);
    amxc_var_init(&ret);
    amxc_var_init(&ret2);
    amxc_var_init(&devinfo);

    util_chosenlink_link_devices(dev_name, TEST3KEY, true);

    // Test invalid paraameters
    assert_int_equal(gmaps_device_get("", &devinfo, 0), gmap_status_invalid_key);
    assert_int_equal(gmaps_device_get(NULL, &devinfo, 0), gmap_status_invalid_key);

    // Test invalid device
    assert_int_equal(gmaps_device_get("testNonExistingKey", &devinfo, 0), gmap_status_device_not_found);

    // Add an action so that it appears in the get
    test_util_scanMibDir("../test_common/mibs");
    assert_int_equal(gmaps_device_set_tag(dev_name, "addaction", "", 0), gmap_status_ok);
    device = gmaps_get_device(dev_name);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "function", "unimplemented3");
    amxc_var_add_key(cstring_t, &args, "name", "action_name");

    assert_int_equal(amxd_object_invoke_function(device,
                                                 "addAction",
                                                 &args,
                                                 &ret),
                     amxd_status_ok);


    // Now test the correct test device
    assert_int_equal(gmaps_device_get(dev_name, &devinfo, 0), gmap_status_ok);
    assert_non_null(amxc_var_get_key(&devinfo, "Childs", 0));
    assert_non_null(amxc_var_get_key(&devinfo, "Names", 0));
    assert_non_null(amxc_var_get_key(&devinfo, "Actions", 0));
    amxc_var_clean(&devinfo);

    amxc_var_init(&devinfo);
    assert_int_equal(gmaps_device_get(dev_name, &devinfo, GMAP_NO_DETAILS), gmap_status_ok);
    assert_true(amxc_var_is_null(amxc_var_get_key(&devinfo, "Alias", 0)));
    amxc_var_clean(&devinfo);

    amxc_var_init(&devinfo);
    assert_int_equal(gmaps_device_get(dev_name, &devinfo, GMAP_NO_ACTIONS), gmap_status_ok);
    assert_null(amxc_var_get_key(&devinfo, "Actions", 0));
    amxc_var_clean(&devinfo);

    amxc_var_init(&devinfo);
    assert_int_equal(gmaps_device_get(dev_name, &devinfo, GMAP_INCLUDE_LINKS), gmap_status_ok);
    llinks = amxc_var_get_key(&devinfo, "LLinks", 0);
    assert_non_null(llinks);
    assert_true(amxc_var_is_null(amxc_var_get_key(amxc_var_get_first(llinks), "Alias", 0)));
    amxc_var_clean(&devinfo);

    amxc_var_init(&devinfo);
    assert_int_equal(gmaps_device_get(dev_name, &devinfo, GMAP_INCLUDE_FULL_LINKS), gmap_status_ok);
    llinks = amxc_var_get_key(&devinfo, "LLinks", 0);
    assert_non_null(llinks);
    assert_false(amxc_var_is_null(amxc_var_get_key(amxc_var_get_first(llinks), "Alias", 0)));

    /* The flag GMAP_INCLUDE_ALTERNATIVES is tested in the topology unit tests.
       Otherwise an extra device was needed. But it has the same functionality there */

    amxc_var_set_type(&args2, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args2, "function", "unimplemented3");
    amxc_var_add_key(cstring_t, &args2, "name", "action_name");

    assert_int_equal(amxd_object_invoke_function(device,
                                                 "removeAction",
                                                 &args2,
                                                 &ret2),
                     amxd_status_ok);

    amxc_var_clean(&devinfo);
    amxc_var_clean(&args);
    amxc_var_clean(&args2);
    amxc_var_clean(&ret);
    amxc_var_clean(&ret2);
}

void test_gmap_can_device_get_result(GMAPS_UNUSED void** state) {

    amxc_var_t devinfo;
    amxc_var_init(&devinfo);

    assert_int_equal(gmaps_device_get(TEST2KEY, &devinfo, 0), gmap_status_ok);

    // Check that the variant type is a hashtable
    assert_int_equal(devinfo.type_id, AMXC_VAR_ID_HTABLE);

    // Check the contents of the htable
    assert_string_equal(amxc_var_constcast(cstring_t, amxc_var_get_key(&devinfo, "Id", 0)), TEST2KEY);

    amxc_var_clean(&devinfo);

}

/**
 *  Test if we correctly can invoke the get function
 */
void test_gmap_can_invoke_device_get(GMAPS_UNUSED void** state) {
    amxd_object_t* device = gmaps_get_device(TEST2KEY);
    amxc_var_t args;
    amxc_var_t result;

    amxc_var_init(&args);
    amxc_var_init(&result);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);

    amxc_var_add_key(cstring_t, &args, "tag", "myTag");
    amxc_var_add_key(cstring_t, &args, "expression", "");
    amxc_var_add_key(cstring_t, &args, "traverse", "this");

    assert_non_null(device);

    amxd_object_invoke_function(device, "get", &args, &result);


    amxc_var_clean(&args);
    amxc_var_clean(&result);


}
MACHINE = $(shell $(CC) -dumpmachine)

SRCDIR = $(realpath ../../src)
OBJDIR = $(realpath ../../output/$(MACHINE)/coverage)
INCDIR = $(realpath ../../include ../../include_priv ../include)
MOCK_SRCDIR = $(realpath ../test_common/)

SOURCES = $(wildcard $(SRCDIR)/gmaps_*.c)
SOURCES += $(wildcard $(MOCK_SRCDIR)/*.c)

CFLAGS += -Werror -Wall -Wextra -Wno-attributes \
          --std=gnu99 -g3 -Wmissing-declarations \
		  $(addprefix -I ,$(INCDIR)) \
		  -fprofile-arcs -ftest-coverage \
		  -fkeep-inline-functions -fkeep-static-functions \
		  $(shell pkg-config --cflags cmocka) 
LDFLAGS += -fprofile-arcs -ftest-coverage \
           -fkeep-inline-functions -fkeep-static-functions \
		   $(shell pkg-config --libs cmocka) -lamxc -lamxp -lamxd -lamxo -lamxut -lamxs -lgmap-client -lamxb -lsahtrace -luuid \
		   -lamxut

WRAP_FUNC=-Wl,--wrap=

LDFLAGS += -g $(addprefix $(WRAP_FUNC),$(MOCK_WRAP))

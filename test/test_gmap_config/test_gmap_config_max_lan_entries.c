/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <setjmp.h>
#include <cmocka.h>

#include "test_config.h"
#include "gmaps_dm_config.h"
#include "gmaps_config.h"
#include "../test_common/test_util.h"

#include "gmaps_devices.h"
#include "gmaps_dm_devices.h"
#include <amxut/amxut_bus.h>
#include <amxut/amxut_timer.h>

static void init_devices() {
    amxc_var_t args;
    amxc_var_t ret;
    amxd_object_t* config = amxd_dm_findf(amxut_bus_dm(), "Devices.Config");

    amxc_var_init(&args);
    amxc_var_init(&ret);

    amxc_var_set(uint32_t, &args, 3);
    gmaps_set_config(config, "global", "MaxLanDevices", &args);

    amxc_var_clean(&args);
    amxc_var_init(&args);
    amxc_var_set(uint32_t, &args, 2);
    gmaps_set_config(config, "global", "MaxInactiveTimeTreshold", &args);
    handle_events();

    util_create_device("lanDev1", "", "lan physical", false, "", amxd_status_ok, NULL);
    assert_int_equal(gmaps_device_set_active("lanDev1", true, "mysource", 100), gmap_status_ok);
    util_create_device("lanDev2", "", "lan physical", false, "", amxd_status_ok, NULL);
    assert_int_equal(gmaps_device_set_active("lanDev2", true, "mysource", 100), gmap_status_ok);
    util_create_device("lanDev3", "", "lan physical protected", false, "", amxd_status_ok, NULL);
    assert_int_equal(gmaps_device_set_active("lanDev3", true, "mysource", 100), gmap_status_ok);

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_max_lan_devices_removes_inactive_lan_devices(GMAPS_UNUSED void** state) {
    amxc_var_t args;
    amxc_var_t ret;
    amxd_object_t* dev = NULL;

    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);

    init_devices();

    dev = gmaps_get_device("lanDev1");
    assert_non_null(dev);
    amxc_var_add_key(bool, &args, "active", 0);
    amxc_var_add_key(cstring_t, &args, "source", "mysource");
    amxc_var_add_key(uint32_t, &args, "priority", 100);
    assert_int_equal(amxd_object_invoke_function(dev,
                                                 "setActive",
                                                 &args,
                                                 &ret),
                     amxd_status_ok);

    dev = gmaps_get_device("lanDev2");
    assert_non_null(dev);
    amxc_var_add_key(bool, &args, "active", 0);
    amxc_var_add_key(cstring_t, &args, "source", "mysource");
    amxc_var_add_key(uint32_t, &args, "priority", 100);
    assert_int_equal(amxd_object_invoke_function(dev,
                                                 "setActive",
                                                 &args,
                                                 &ret),
                     amxd_status_ok);
    amxut_timer_go_to_future_ms(3000);

    util_create_device("lanDev4", "", "lan physical", false, "", amxd_status_ok, NULL);
    dev = gmaps_get_device("lanDev4");
    amxc_var_add_key(bool, &args, "active", true);
    amxc_var_add_key(cstring_t, &args, "source", "mysource");
    amxc_var_add_key(uint32_t, &args, "priority", 100);
    assert_int_equal(amxd_object_invoke_function(dev,
                                                 "setActive",
                                                 &args,
                                                 &ret),
                     amxd_status_ok);

    dev = gmaps_get_device("lanDev1");
    assert_null(dev);
    dev = gmaps_get_device("lanDev2");
    assert_null(dev);
    dev = gmaps_get_device("lanDev4");
    assert_non_null(dev);

    util_create_device("lanDev5", "", "lan physical", false, "", amxd_status_ok, NULL);
    dev = gmaps_get_device("lanDev5");
    amxc_var_add_key(bool, &args, "active", true);
    amxc_var_add_key(cstring_t, &args, "source", "mysource");
    amxc_var_add_key(uint32_t, &args, "priority", 100);
    assert_int_equal(amxd_object_invoke_function(dev,
                                                 "setActive",
                                                 &args,
                                                 &ret),
                     amxd_status_ok);

    dev = gmaps_get_device("lanDev5");
    assert_non_null(dev);

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void test_max_lan_devices_limits_nr_of_devices(GMAPS_UNUSED void** state) {
    amxd_object_t* dev = NULL;

    util_create_device("lanDev6", "", "lan physical", false, "", amxd_status_unknown_error, NULL);

    dev = gmaps_get_device("lanDev6");
    assert_null(dev);
}

void test_max_lan_devices_doesnt_remove_protected(GMAPS_UNUSED void** state) {
    amxd_object_t* dev = NULL;

    assert_int_equal(gmaps_device_set_active("lanDev3", false, "mysource", 100), gmap_status_ok);
    amxut_timer_go_to_future_ms(3000);

    util_create_device("lanDev6", "", "lan physical", false, "", amxd_status_unknown_error, NULL);

    dev = gmaps_get_device("lanDev6");
    assert_null(dev);

    dev = gmaps_get_device("lanDev3");
    assert_non_null(dev);
}

void test_only_lan_tag_is_no_lan_device(GMAPS_UNUSED void** state) {
    amxd_object_t* dev = NULL;
    amxd_object_t* devices = amxd_dm_findf(amxut_bus_dm(), "Devices.");
    amxc_var_t lan_value;

    amxc_var_init(&lan_value);

    amxd_object_get_param(devices, "LANDeviceNumberOfEntries", &lan_value);
    assert_int_equal(amxc_var_dyncast(uint32_t, &lan_value), 3);

    util_create_device("lanDev7", "", "lan", false, "", amxd_status_ok, NULL);

    dev = gmaps_get_device("lanDev7");
    assert_non_null(dev);

    amxc_var_clean(&lan_value);
    amxc_var_init(&lan_value);
    devices = amxd_dm_findf(amxut_bus_dm(), "Devices.");
    amxd_object_get_param(devices, "LANDeviceNumberOfEntries", &lan_value);
    assert_int_equal(amxc_var_dyncast(uint32_t, &lan_value), 3);

    amxc_var_clean(&lan_value);
}

void test_younger_device_not_removed(GMAPS_UNUSED void** state) {
    amxd_object_t* dev = NULL;

    assert_int_equal(gmaps_device_set_active("lanDev5", false, "mysource", 100), gmap_status_ok);

    amxut_timer_go_to_future_ms(1000);

    util_create_device("lanDev8", "", "lan physical", false, "", amxd_status_unknown_error, NULL);

    dev = gmaps_get_device("lanDev8");
    assert_null(dev);
}

void test_interval_removes_inactive_lan_devices_treshold(GMAPS_UNUSED void** state) {
    /*
     * As lanDev5 is already inactive at this time and should thus be deleted over time.
     * Also the treshold value of 3 is reached and thus MaxInactiveTimeTreshold should be used.
     * While MaxInactiveTime is zero and thus should not be used.
     * lanDev3 and lanDev4 should not be deleted as lanDev3 is protected and lanDev4 is still active.
     */
    amxc_var_t args;
    amxd_object_t* dev = NULL;
    amxd_object_t* config_obj = amxd_object_findf(&gmap_get_dm()->object, "Devices.Config");

    amxc_var_init(&args);
    amxc_var_set(uint32_t, &args, 600);
    gmaps_set_config(config_obj, "global", "InactiveCheckInterval", &args);
    amxc_var_clean(&args);

    amxc_var_init(&args);
    amxc_var_set(uint32_t, &args, 500);
    gmaps_set_config(config_obj, "global", "MaxInactiveTimeTreshold", &args);
    amxc_var_clean(&args);

    amxc_var_init(&args);
    amxc_var_set(uint32_t, &args, 3);
    gmaps_set_config(config_obj, "global", "InactiveCheckThreshold", &args);
    amxc_var_clean(&args);

    handle_events();

    amxut_timer_go_to_future_ms(600 * 1000);
    amxut_bus_handle_events();

    dev = gmaps_get_device("lanDev5");
    assert_null(dev);
    dev = gmaps_get_device("lanDev3");
    assert_non_null(dev);
    dev = gmaps_get_device("lanDev4");
    assert_non_null(dev);

    amxc_var_clean(&args);
}

void test_interval_removes_inactive_lan_devices(GMAPS_UNUSED void** state) {
    /*
     * As lanDev4 is already inactive at this time and should thus be deleted over time.
     * However, the threshold is not yet reached and MaxInactiveTime is not zero, so MaxInactiveTime should be used.
     * lanDev3 should not be deleted as lanDev3 is protected.
     */
    amxc_var_t args;
    amxd_object_t* dev = NULL;
    amxd_object_t* config_obj = amxd_object_findf(&gmap_get_dm()->object, "Devices.Config");

    amxc_var_init(&args);
    amxc_var_set(uint32_t, &args, 800);
    gmaps_set_config(config_obj, "global", "MaxInactiveTime", &args);
    amxc_var_clean(&args);

    assert_int_equal(gmaps_device_set_active("lanDev4", false, "mysource", 100), gmap_status_ok);
    handle_events();

    amxut_timer_go_to_future_ms(600 * 1000);
    amxut_bus_handle_events();

    // If the MaxInactiveTimeTreshold was used, lanDev4 would already be removed after 600 seconds.
    // But as the treshold is not reached yet, the lanDev4 is not removed yet.
    dev = gmaps_get_device("lanDev3");
    assert_non_null(dev);
    dev = gmaps_get_device("lanDev4");
    assert_non_null(dev);

    amxut_timer_go_to_future_ms(600 * 1000);
    amxut_bus_handle_events();

    dev = gmaps_get_device("lanDev3");
    assert_non_null(dev);
    dev = gmaps_get_device("lanDev4");
    assert_null(dev);

    amxc_var_clean(&args);
}

void test_interval_removes_no_inactive_lan_devices(GMAPS_UNUSED void** state) {
    /*
     * This test checks the cases were the inactive devices should not be removed.
     */
    amxc_var_t args;
    amxd_object_t* dev = NULL;
    amxd_object_t* config_obj = amxd_object_findf(&gmap_get_dm()->object, "Devices.Config");

    util_create_device("lanDev9", "", "lan physical", false, "", amxd_status_ok, NULL);
    dev = gmaps_get_device("lanDev9");
    handle_events();
    assert_non_null(dev);

    // Threshold is not reached and MaxInactiveTime is '0', so the device should not be deleted.
    amxc_var_init(&args);
    amxc_var_set(uint32_t, &args, 0);
    gmaps_set_config(config_obj, "global", "MaxInactiveTime", &args);
    amxc_var_clean(&args);

    amxut_timer_go_to_future_ms(1200 * 1000);
    amxut_bus_handle_events();

    dev = gmaps_get_device("lanDev9");
    assert_non_null(dev);

    // Threshold is reached, but MaxInactiveTimeTreshold is '0'. So the device should not be deleted.
    amxc_var_init(&args);
    amxc_var_set(uint32_t, &args, 0);
    gmaps_set_config(config_obj, "global", "MaxInactiveTimeTreshold", &args);
    amxc_var_clean(&args);

    util_create_device("lanDev10", "", "lan physical", false, "", amxd_status_ok, NULL);
    dev = gmaps_get_device("lanDev10");
    assert_non_null(dev);
    handle_events();

    amxut_timer_go_to_future_ms(1200 * 1000);
    amxut_bus_handle_events();

    dev = gmaps_get_device("lanDev9");
    assert_non_null(dev);
    dev = gmaps_get_device("lanDev10");
    assert_non_null(dev);

    assert_int_equal(gmaps_device_set_active("lanDev9", true, "mysource", 100), gmap_status_ok);
    assert_int_equal(gmaps_device_set_active("lanDev10", true, "mysource", 100), gmap_status_ok);
    handle_events();

    amxc_var_clean(&args);
}

void test_interval_removes_inactive_lan_devices_combo(GMAPS_UNUSED void** state) {
    /*
     * As lanDev4 is already inactive at this time and should thus be deleted over time.
     * However, the threshold is not yet reached and MaxInactiveTime is not zero, so MaxInactiveTime should be used.
     * lanDev3 should not be deleted as lanDev3 is protected.
     */
    amxc_var_t args;
    amxd_object_t* dev = NULL;
    amxd_object_t* config_obj = amxd_object_findf(&gmap_get_dm()->object, "Devices.Config");

    amxc_var_init(&args);
    amxc_var_set(uint32_t, &args, 800);
    gmaps_set_config(config_obj, "global", "InactiveCheckInterval", &args);
    amxc_var_clean(&args);

    amxc_var_init(&args);
    amxc_var_set(uint32_t, &args, 800);
    gmaps_set_config(config_obj, "global", "MaxInactiveTime", &args);
    amxc_var_clean(&args);

    amxc_var_init(&args);
    amxc_var_set(uint32_t, &args, 600);
    gmaps_set_config(config_obj, "global", "MaxInactiveTimeTreshold", &args);
    amxc_var_clean(&args);

    assert_int_equal(gmaps_device_set_active("lanDev9", false, "mysource", 100), gmap_status_ok);
    handle_events();
    amxut_timer_go_to_future_ms(200 * 1000);
    amxut_bus_handle_events();
    assert_int_equal(gmaps_device_set_active("lanDev10", false, "mysource", 100), gmap_status_ok);
    handle_events();

    amxut_timer_go_to_future_ms(700 * 1000);
    amxut_bus_handle_events();

    // In this case the MaxInactiveTime should remove landev9, as it will be 900 sec inactive.
    // But it should not remove landev10 as it will only be inactive for 700 sec.
    // However, it should remove landev10 as MaxInactiveTimeTreshold equals 600.
    dev = gmaps_get_device("lanDev9");
    assert_null(dev);
    dev = gmaps_get_device("lanDev10");
    assert_null(dev);

    amxc_var_clean(&args);
}

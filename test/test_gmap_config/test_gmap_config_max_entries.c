/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <setjmp.h>
#include <cmocka.h>

#include "test_config.h"
#include "gmaps_dm_config.h"
#include "gmaps_config.h"
#include "../test_common/test_util.h"

#include "gmaps_devices.h"
#include "gmaps_dm_devices.h"
#include "../test_common/test_util.h"
#include <amxut/amxut_timer.h>
#include <amxut/amxut_bus.h>

static void init_devices() {
    amxc_var_t args;
    amxc_var_t ret;
    amxd_object_t* config = amxd_dm_findf(amxut_bus_dm(), "Devices.Config");

    amxc_var_init(&args);
    amxc_var_init(&ret);

    amxc_var_set(uint32_t, &args, 5);

    gmaps_set_config(config, "global", "MaxDevices", &args);

    util_create_device("Dev1", "", "", false, "", amxd_status_ok, NULL);
    assert_int_equal(gmaps_device_set_active("Dev1", true, "mysource", 100), gmap_status_ok);
    amxut_timer_go_to_future_ms(2000);
    util_create_device("Dev2", "", "", false, "", amxd_status_ok, NULL);
    assert_int_equal(gmaps_device_set_active("Dev2", true, "mysource", 100), gmap_status_ok);
    amxut_timer_go_to_future_ms(2000);
    util_create_device("Dev3", "", "protected", false, "", amxd_status_ok, NULL);
    assert_int_equal(gmaps_device_set_active("Dev3", true, "mysource", 100), gmap_status_ok);
    amxut_timer_go_to_future_ms(2000);
    util_create_device("Dev4", "", "", false, "", amxd_status_ok, NULL);
    assert_int_equal(gmaps_device_set_active("Dev4", true, "mysource", 100), gmap_status_ok);
    amxut_timer_go_to_future_ms(2000);
    util_create_device("Dev5", "", "", false, "", amxd_status_ok, NULL);
    assert_int_equal(gmaps_device_set_active("Dev5", true, "mysource", 100), gmap_status_ok);
    amxut_timer_go_to_future_ms(2000);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}


void test_max_devices_removes_inactive_device(GMAPS_UNUSED void** state) {
    amxc_var_t args;
    amxc_var_t ret;
    amxd_object_t* dev = NULL;

    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);

    init_devices();

    dev = gmaps_get_device("Dev1");
    assert_non_null(dev);
    amxc_var_add_key(bool, &args, "active", 0);
    amxc_var_add_key(cstring_t, &args, "source", "mysource");
    amxc_var_add_key(uint32_t, &args, "priority", 100);
    assert_int_equal(amxd_object_invoke_function(dev,
                                                 "setActive",
                                                 &args,
                                                 &ret),
                     amxd_status_ok);

    util_create_device("Dev6", "", "", false, "", amxd_status_ok, NULL);
    dev = gmaps_get_device("Dev6");
    amxc_var_add_key(bool, &args, "active", true);
    amxc_var_add_key(cstring_t, &args, "source", "mysource");
    amxc_var_add_key(uint32_t, &args, "priority", 100);
    assert_int_equal(amxd_object_invoke_function(dev,
                                                 "setActive",
                                                 &args,
                                                 &ret),
                     amxd_status_ok);

    dev = gmaps_get_device("Dev1");
    assert_null(dev);

    dev = gmaps_get_device("Dev6");
    assert_non_null(dev);

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}


void test_max_devices_limits_nr_of_active_devices(GMAPS_UNUSED void** state) {
    amxd_object_t* dev = NULL;

    util_create_device("Dev7", "", "", false, "", amxd_status_unknown_error, NULL);

    dev = gmaps_get_device("Dev7");
    assert_null(dev);
}


void test_max_devices_doesnt_remove_protected(GMAPS_UNUSED void** state) {
    amxd_object_t* dev = NULL;

    assert_int_equal(gmaps_device_set_active("Dev3", false, "mysource", 100), gmap_status_ok);
    util_create_device("Dev7", "", "", false, "", amxd_status_unknown_error, NULL);

    dev = gmaps_get_device("Dev7");
    assert_null(dev);

    dev = gmaps_get_device("Dev3");
    assert_non_null(dev);
}


void test_max_devices_removes_oldest_inactive_device(GMAPS_UNUSED void** state) {
    amxc_var_t args;
    amxc_var_t ret;
    amxd_object_t* dev = NULL;

    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);

    dev = gmaps_get_device("Dev4");
    amxc_var_add_key(bool, &args, "active", 0);
    amxc_var_add_key(cstring_t, &args, "source", "mysource");
    amxc_var_add_key(uint32_t, &args, "priority", 100);
    assert_int_equal(amxd_object_invoke_function(dev,
                                                 "setActive",
                                                 &args,
                                                 &ret),
                     amxd_status_ok);
    amxut_timer_go_to_future_ms(2000);


    dev = gmaps_get_device("Dev2");
    // args cannot be reused, so re-init:
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(bool, &args, "active", 0);
    amxc_var_add_key(cstring_t, &args, "source", "mysource");
    amxc_var_add_key(uint32_t, &args, "priority", 100);
    assert_int_equal(amxd_object_invoke_function(dev,
                                                 "setActive",
                                                 &args,
                                                 &ret),
                     amxd_status_ok);

    util_create_device("Dev7", "", "", false, "", amxd_status_ok, NULL);

    dev = gmaps_get_device("Dev4");
    assert_null(dev);

    dev = gmaps_get_device("Dev7");
    assert_non_null(dev);

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}
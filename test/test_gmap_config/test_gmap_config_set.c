/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdarg.h>
#include <setjmp.h>
#include <cmocka.h>

#include "test_config.h"
#include "gmaps_config.h"
#include "gmaps_dm_config.h"
#include <amxut/amxut_bus.h>


void test_rpc_can_set_config_existing(GMAPS_UNUSED void** state) {
    amxd_object_t* config = amxd_dm_findf(amxut_bus_dm(), "Devices.Config");
    amxc_var_t args;
    amxc_var_t ret;
    amxc_var_t params;
    const amxc_htable_t* table = NULL;
    amxc_htable_it_t* hit;
    const char* value_read = NULL;

    assert_ptr_not_equal(config, NULL);
    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxc_var_init(&params);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "module", "testModule");
    amxc_var_add_key(cstring_t, &args, "option", "testOption");
    amxc_var_add_key(cstring_t, &args, "value", "testValueString");

    assert_int_equal(amxd_object_invoke_function(config, "set", &args, &ret), amxd_status_ok);

    // Readback
    amxd_object_t* test_config = amxd_object_get_child(config, "testModule");
    assert_non_null(test_config);
    assert_int_equal(amxd_object_get_params(test_config, &params, amxd_dm_access_protected), amxd_status_ok);
    assert_int_equal(amxc_var_type_of(&params), AMXC_VAR_ID_HTABLE);
    table = amxc_var_constcast(amxc_htable_t, &params);
    assert_non_null(table);
    hit = amxc_htable_get(table, "testOption");
    assert_non_null(hit);
    value_read = amxc_var_constcast(cstring_t,
                                    amxc_htable_it_get_data(hit, amxc_var_t, hit));
    assert_string_equal(value_read, "testValueString");

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    amxc_var_clean(&params);
}

void test_rpc_can_set_config_nonexisting(GMAPS_UNUSED void** state) {
    amxd_object_t* config = amxd_dm_findf(amxut_bus_dm(), "Devices.Config");
    amxc_var_t args;
    amxc_var_t ret;
    amxc_var_t params;
    const amxc_htable_t* table = NULL;
    amxc_htable_it_t* hit;
    const char* value_read = NULL;

    assert_ptr_not_equal(config, NULL);
    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxc_var_init(&params);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "module", "testModule3");
    amxc_var_add_key(cstring_t, &args, "option", "testOption3");
    amxc_var_add_key(cstring_t, &args, "value", "testValueString3");

    assert_int_equal(amxd_object_invoke_function(config, "set", &args, &ret), amxd_status_ok);

    // Readback
    amxd_object_t* test_config = amxd_object_get_child(config, "testModule3");
    assert_non_null(test_config);
    assert_int_equal(amxd_object_get_params(test_config, &params, amxd_dm_access_protected), amxd_status_ok);
    assert_int_equal(amxc_var_type_of(&params), AMXC_VAR_ID_HTABLE);
    table = amxc_var_constcast(amxc_htable_t, &params);
    assert_non_null(table);
    hit = amxc_htable_get(table, "testOption3");
    assert_non_null(hit);
    value_read = amxc_var_constcast(cstring_t,
                                    amxc_htable_it_get_data(hit, amxc_var_t, hit));
    assert_string_equal(value_read, "testValueString3");

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    amxc_var_clean(&params);
}

void test_internal_set_config_input_validation(GMAPS_UNUSED void** state) {
    amxd_object_t* config = amxd_dm_findf(amxut_bus_dm(), "Devices.Config");
    amxc_var_t val;

    assert_ptr_not_equal(config, NULL);
    amxc_var_init(&val);

    assert_int_equal(gmaps_set_config(NULL,
                                      "myModule",
                                      "myOption",
                                      &val),
                     gmap_status_invalid_parameter);
    assert_int_equal(gmaps_set_config(config,
                                      NULL,
                                      "myOption",
                                      &val),
                     gmap_status_invalid_parameter);
    assert_int_equal(gmaps_set_config(config,
                                      "",
                                      "myOption",
                                      &val),
                     gmap_status_invalid_parameter);
    assert_int_equal(gmaps_set_config(config,
                                      "myModule",
                                      NULL,
                                      &val),
                     gmap_status_invalid_parameter);
    assert_int_equal(gmaps_set_config(config,
                                      "myModule",
                                      "",
                                      &val),
                     gmap_status_invalid_parameter);
    assert_int_equal(gmaps_set_config(config,
                                      "myModule",
                                      "myOption",
                                      NULL),
                     gmap_status_invalid_parameter);

    amxc_var_clean(&val);
}
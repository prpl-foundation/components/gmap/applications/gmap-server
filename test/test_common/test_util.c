/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "test_util.h"
#include "test_common.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <setjmp.h>
#include <cmocka.h>
#include <amxut/amxut_bus.h>
#include <amxut/amxut_dm.h>
#include <amxut/amxut_timer.h>
#include "gmaps_dm_topologybuilder.h"

static void set_args_create_device(amxc_var_t* args,
                                   const char* const key,
                                   const char* const discovery_source,
                                   const char* const tags,
                                   bool persistent,
                                   const char* const default_name,
                                   amxc_var_t* parameters) {
    amxc_var_set_type(args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, args, "key", key);
    amxc_var_add_key(cstring_t, args, "discovery_source", discovery_source);
    amxc_var_add_key(cstring_t, args, "tags", tags);
    amxc_var_add_key(bool, args, "persistent", persistent);
    amxc_var_add_key(cstring_t, args, "default_name", default_name);
    if(parameters != NULL) {
        amxc_var_t* values = amxc_var_add_new_key(args, "values");
        amxc_var_copy(values, parameters);
    }
}

/**
 * Creates a devices with the given parameters and performs some sanity checks on the result.
 * The provided parameters should be valid.
 */
void util_create_device(const char* const key,
                        const char* const discovery_source,
                        const char* const tags,
                        bool persistent,
                        const char* const default_name,
                        amxd_status_t expected_status,
                        amxc_var_t* values) {
    amxd_object_t* devices = amxd_dm_findf(amxut_bus_dm(), "Devices");
    amxc_var_t args;
    amxc_var_t ret;
    amxc_var_t params;
    amxd_object_t* instance = NULL;
    amxd_status_t actual_status = amxd_status_unknown_error;
    assert_non_null(devices);

    // Set up arguments:
    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxc_var_init(&params);
    if(values != NULL) {
        amxc_var_copy(&params, values);
    }
    set_args_create_device(&args, key, discovery_source,
                           tags, persistent, default_name, &params);

    // Perform call:
    actual_status = amxd_object_invoke_function(devices,
                                                "createDevice",
                                                &args,
                                                &ret);

    // Verify result
    assert_int_equal(actual_status, expected_status);
    when_failed(expected_status, exit);
    assert_true(amxc_var_get_bool(&ret));
    instance = amxd_object_get_instance(amxd_object_get_child(devices, "Device"), key, 0);
    assert_non_null(instance);
    assert_int_equal(amxd_object_get_params(instance, &params, amxd_dm_access_protected), amxd_status_ok);
    assert_string_equal(amxc_var_constcast(cstring_t, VARIANT_GET(&params, "Key")),
                        key);
    assert_string_equal(amxc_var_constcast(cstring_t, VARIANT_GET(&params, "DiscoverySource")),
                        discovery_source);
    assert_string_equal(amxc_var_constcast(cstring_t,
                                           amxc_var_get_key(&params,
                                                            "Tags",
                                                            AMXC_VAR_FLAG_DEFAULT)),
                        tags);

exit:
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    amxc_var_clean(&params);
}

void util_create_device_no_tag_comparison(const char* const key,
                                          const char* const discovery_source,
                                          const char* const tags,
                                          bool persistent,
                                          const char* const default_name,
                                          amxd_status_t expected_status,
                                          amxc_var_t* values) {
    amxd_object_t* devices = amxd_dm_findf(amxut_bus_dm(), "Devices");
    amxc_var_t args;
    amxc_var_t ret;
    amxc_var_t params;
    amxd_object_t* instance = NULL;
    amxd_status_t actual_status = amxd_status_unknown_error;
    assert_non_null(devices);

    // Set up arguments:
    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxc_var_init(&params);
    if(values != NULL) {
        amxc_var_copy(&params, values);
    }
    set_args_create_device(&args, key, discovery_source,
                           tags, persistent, default_name, &params);

    // Perform call:
    actual_status = amxd_object_invoke_function(devices,
                                                "createDevice",
                                                &args,
                                                &ret);

    // Verify result
    assert_int_equal(actual_status, expected_status);
    when_failed(expected_status, exit);
    assert_true(amxc_var_get_bool(&ret));
    instance = amxd_object_get_instance(amxd_object_get_child(devices, "Device"), key, 0);
    assert_non_null(instance);
    assert_int_equal(amxd_object_get_params(instance, &params, amxd_dm_access_protected), amxd_status_ok);
    assert_string_equal(amxc_var_constcast(cstring_t, VARIANT_GET(&params, "Key")),
                        key);
    assert_string_equal(amxc_var_constcast(cstring_t, VARIANT_GET(&params, "DiscoverySource")),
                        discovery_source);

exit:
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    amxc_var_clean(&params);
}

void util_destroy_device(const char* const key) {
    amxd_object_t* devices = amxd_dm_findf(amxut_bus_dm(), "Devices");
    amxc_var_t args;
    amxc_var_t ret;
    amxd_status_t actual_status = amxd_status_unknown_error;
    assert_non_null(devices);
    amxc_var_init(&ret);
    amxc_var_init(&args);

    // Set up arguments:
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "key", key);

    // Perform call:
    actual_status = amxd_object_invoke_function(devices,
                                                "destroyDevice",
                                                &args,
                                                &ret);

    assert_int_equal(actual_status, amxd_status_ok);

    amxc_var_clean(&ret);
    amxc_var_clean(&args);

    assert_null(gmaps_get_device(key));
}

void test_util_scanMibDir(const char* path) {
    amxc_var_t args;
    amxc_var_t ret;
    amxd_object_t* config = amxd_dm_findf(amxut_bus_dm(), "Devices.Config");

    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "path", path);
    assert_int_equal(amxd_object_invoke_function(config, "scanMibDir", &args, &ret), amxd_status_ok);

    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void handle_events(void) {
    amxut_bus_handle_events();
}

void util_add_string_param_to_obj(amxd_object_t* object, const char* param_name, const char* value) {
    amxd_param_t* param = NULL;
    amxc_var_t value_var;

    assert_int_equal(amxd_param_new(&param, param_name, AMXC_VAR_ID_CSTRING), 0);
    assert_int_equal(amxd_object_add_param(object, param), 0);

    amxc_var_init(&value_var);
    amxc_var_set_type(&value_var, AMXC_VAR_ID_CSTRING);
    amxc_var_set(cstring_t, &value_var, value);
    assert_int_equal(amxd_object_set_param(object, param_name, &value_var), 0);
    amxc_var_clean(&value_var);
}

amxd_status_t __wrap_cleanClientLeases(amxd_object_t* object,
                                       GMAPS_UNUSED amxd_function_t* func,
                                       amxc_var_t* args,
                                       GMAPS_UNUSED amxc_var_t* ret) {
    amxc_var_t* MACs = amxc_var_get_key(args, "MACs", AMXC_VAR_FLAG_DEFAULT);
    char* object_path = amxd_object_get_path(object, AMXD_OBJECT_INDEXED);

    check_expected(MACs);
    check_expected(object_path);

    free(object_path);

    return amxd_status_ok;
}

int test_util_check_mac_list(LargestIntegralType value, LargestIntegralType check_value_data) {
    amxc_var_t* values = (void*) value;
    const char* mac = amxc_var_constcast(cstring_t, amxc_var_get_first(values));
    const char* expected_value = (void*) check_value_data;

    when_str_empty(mac, exit);
    when_str_empty(expected_value, exit);

    if((strcmp(mac, expected_value) == 0) &&
       (amxc_var_get_next(values) == NULL)) {
        return true;
    }

exit:
    return false;
}

bool util_is_linked_to_for_datasource(const char* upper_dev, const char* lower_dev, const char* datasource) {
    bool linked_dir1 = false;
    bool linked_dir2 = false;

    // Note: no amxd_dm_findf for the whole thing, to be safe against names with a dot, square brackets, etc.
    amxd_object_t* dev_templ = amxd_dm_findf(amxut_bus_dm(), "Devices.Device");
    amxd_object_t* udev_obj = amxd_object_get(dev_templ, upper_dev);
    amxd_object_t* ldev_obj = amxd_object_get(dev_templ, lower_dev);
    amxd_object_t* udev_link_templ = amxd_object_get(udev_obj, "Link");
    amxd_object_t* ldev_link_templ = amxd_object_get(ldev_obj, "Link");
    assert_non_null(udev_link_templ);
    assert_non_null(ldev_link_templ);
    amxd_object_t* ldev_link_obj = amxd_object_get(ldev_link_templ, datasource);
    amxd_object_t* udev_link_obj = amxd_object_get(udev_link_templ, datasource);
    if(ldev_link_obj != NULL) {
        amxd_object_t* ldev_udevice = amxd_object_get(ldev_link_obj, "UDevice");
        assert_non_null(ldev_udevice);
        linked_dir1 = NULL != amxd_object_get(ldev_udevice, upper_dev);
    } else {
        linked_dir1 = false;
    }
    if(udev_link_obj != NULL) {
        amxd_object_t* udev_ldevice = amxd_object_get(udev_link_obj, "LDevice");
        assert_non_null(udev_ldevice);
        linked_dir2 = NULL != amxd_object_get(udev_ldevice, lower_dev);
    } else {
        linked_dir2 = false;
    }

    assert_true(linked_dir1 == linked_dir2);
    return linked_dir1;
}

bool util_is_link_selected(const char* upper_dev, const char* lower_dev) {
    amxd_object_t* upper_dev_obj = gmaps_get_device(upper_dev);
    amxd_object_t* lower_dev_obj = gmaps_get_device(lower_dev);
    bool linked_dir1 = gmaps_device_is_linked_to(upper_dev_obj, lower_dev_obj, gmap_traverse_down);
    bool linked_dir2 = gmaps_device_is_linked_to(lower_dev_obj, upper_dev_obj, gmap_traverse_up);
    assert_non_null(upper_dev_obj);
    assert_non_null(lower_dev_obj);
    assert_true(linked_dir1 == linked_dir2);
    return linked_dir1;
}


bool util_is_linked_to(const char* upper_dev, const char* lower_dev, const char* datasource) {
    bool is_link_selected = util_is_link_selected(upper_dev, lower_dev);
    bool linked_for_source = util_is_linked_to_for_datasource(upper_dev, lower_dev, datasource);

    assert_true(is_link_selected == linked_for_source);
    return is_link_selected;
}

// Avoid "Process terminating with default action of signal 14 (SIGALRM)"
static void setup_timer(void) {
    sigset_t mask;
    sigemptyset(&mask);
    sigaddset(&mask, SIGALRM);

    sigprocmask(SIG_BLOCK, &mask, NULL);
}

void util_setup_resolver(void) {
    amxo_resolver_ftab_add(amxut_bus_parser(), "createDevice", AMXO_FUNC(_Devices_createDevice));
    amxo_resolver_ftab_add(amxut_bus_parser(), "destroyDevice", AMXO_FUNC(_Devices_destroyDevice));
    amxo_resolver_ftab_add(amxut_bus_parser(), "createDeviceOrGetKey", AMXO_FUNC(_Devices_createDeviceOrGetKey));
    amxo_resolver_ftab_add(amxut_bus_parser(), "find", AMXO_FUNC(_Devices_find));
    amxo_resolver_ftab_add(amxut_bus_parser(), "linkAdd", AMXO_FUNC(_Devices_linkAdd));
    amxo_resolver_ftab_add(amxut_bus_parser(), "linkReplace", AMXO_FUNC(_Devices_linkReplace));
    amxo_resolver_ftab_add(amxut_bus_parser(), "linkRemove", AMXO_FUNC(_Devices_linkRemove));
    amxo_resolver_ftab_add(amxut_bus_parser(), "notify", AMXO_FUNC(_Devices_notify));
    amxo_resolver_ftab_add(amxut_bus_parser(), "block", AMXO_FUNC(_Devices_block));
    amxo_resolver_ftab_add(amxut_bus_parser(), "unblock", AMXO_FUNC(_Devices_unblock));
    amxo_resolver_ftab_add(amxut_bus_parser(), "isBlocked", AMXO_FUNC(_Devices_isBlocked));
    amxo_resolver_ftab_add(amxut_bus_parser(), "get", AMXO_FUNC(_Devices_get));
    amxo_resolver_ftab_add(amxut_bus_parser(), "removeInactiveDevices", AMXO_FUNC(_Devices_removeInactiveDevices));

    amxo_resolver_ftab_add(amxut_bus_parser(), "setName", AMXO_FUNC(_Device_setName));
    amxo_resolver_ftab_add(amxut_bus_parser(), "removeName", AMXO_FUNC(_Device_removeName));

    amxo_resolver_ftab_add(amxut_bus_parser(), "setActive", AMXO_FUNC(_Device_setActive));
    amxo_resolver_ftab_add(amxut_bus_parser(), "set", AMXO_FUNC(_Device_set));
    amxo_resolver_ftab_add(amxut_bus_parser(), "get", AMXO_FUNC(_Device_get));
    amxo_resolver_ftab_add(amxut_bus_parser(), "setTag", AMXO_FUNC(_Device_setTag));
    amxo_resolver_ftab_add(amxut_bus_parser(), "hasTag", AMXO_FUNC(_Device_hasTag));
    amxo_resolver_ftab_add(amxut_bus_parser(), "clearTag", AMXO_FUNC(_Device_clearTag));
    amxo_resolver_ftab_add(amxut_bus_parser(), "isImplemented", AMXO_FUNC(_Device_isImplemented));
    amxo_resolver_ftab_add(amxut_bus_parser(), "setFunction", AMXO_FUNC(_Device_setFunction));
    amxo_resolver_ftab_add(amxut_bus_parser(), "csiFinished", AMXO_FUNC(_Devices_csiFinished));
    amxo_resolver_ftab_add(amxut_bus_parser(), "removeFunction", AMXO_FUNC(_Device_removeFunction));
    amxo_resolver_ftab_add(amxut_bus_parser(), "setAlternative", AMXO_FUNC(_Device_setAlternative));
    amxo_resolver_ftab_add(amxut_bus_parser(), "removeAlternative", AMXO_FUNC(_Device_removeAlternative));
    amxo_resolver_ftab_add(amxut_bus_parser(), "isAlternative", AMXO_FUNC(_Device_isAlternative));
    amxo_resolver_ftab_add(amxut_bus_parser(), "setAlternativeRules", AMXO_FUNC(_Device_setAlternativeRules));
    amxo_resolver_ftab_add(amxut_bus_parser(), "removeAlternativeRules", AMXO_FUNC(_Device_removeAlternativeRules));
    amxo_resolver_ftab_add(amxut_bus_parser(), "isLinkedTo", AMXO_FUNC(_Device_isLinkedTo));
    amxo_resolver_ftab_add(amxut_bus_parser(), "topology", AMXO_FUNC(_Device_topology));
    amxo_resolver_ftab_add(amxut_bus_parser(), "getFirstParameter", AMXO_FUNC(_Device_getFirstParameter));
    amxo_resolver_ftab_add(amxut_bus_parser(), "getParameters", AMXO_FUNC(_Device_getParameters));
    amxo_resolver_ftab_add(amxut_bus_parser(), "addAction", AMXO_FUNC(_Device_addAction));
    amxo_resolver_ftab_add(amxut_bus_parser(), "removeAction", AMXO_FUNC(_Device_removeAction));

    amxo_resolver_ftab_add(amxut_bus_parser(), "openQuery", AMXO_FUNC(_openQuery));
    amxo_resolver_ftab_add(amxut_bus_parser(), "closeQuery", AMXO_FUNC(_closeQuery));
    amxo_resolver_ftab_add(amxut_bus_parser(), "matchingDevices", AMXO_FUNC(_Query_matchingDevices));
    amxo_resolver_ftab_add(amxut_bus_parser(), "device_added", AMXO_FUNC(_device_added));
    amxo_resolver_ftab_add(amxut_bus_parser(), "device_changed", AMXO_FUNC(_device_changed));
    amxo_resolver_ftab_add(amxut_bus_parser(), "device_subobject_update", AMXO_FUNC(_device_subobject_update));
    amxo_resolver_ftab_add(amxut_bus_parser(), "device_names_subobject_update", AMXO_FUNC(_device_names_subobject_update));
    amxo_resolver_ftab_add(amxut_bus_parser(), "remove_device_instance", AMXO_FUNC(_remove_device_instance));

    amxo_resolver_ftab_add(amxut_bus_parser(), "scanMibDir", AMXO_FUNC(_Config_scanMibDir));

    amxo_resolver_ftab_add(amxut_bus_parser(), "cleanClientLeases", AMXO_FUNC(__wrap_cleanClientLeases));

    amxo_resolver_ftab_add(amxut_bus_parser(), "load", AMXO_FUNC(_Config_load));
}

int util_setup(void** state) {

    amxd_object_t* root_obj = NULL;

    amxut_bus_setup(state);

    root_obj = amxd_dm_get_root(amxut_bus_dm());
    assert_ptr_not_equal(root_obj, NULL);

    util_setup_resolver();
    assert_int_equal(system("mkdir -p /tmp/usr/lib/gmap-server/config/"), 0);
    assert_int_equal(system("cp ../test_common/gmap_conf_testglobal.odl /tmp/usr/lib/gmap-server/config/gmap_conf_global.odl"), 0);

    amxut_dm_load_odl("%s", ODL_DEFS);
    amxut_dm_load_odl("%s", ODL_TEST_CONFIG);
    amxut_dm_load_odl("%s", ODL_NTP_DEFINITION);
    amxut_dm_load_odl("%s", ODL_NTP_SYNCRONIZED);
    amxut_dm_load_odl("%s", ODL_DHCP_CLIENT);

    assert_non_null(amxb_be_who_has("Time."));
    assert_non_null(amxb_be_who_has("DHCPv4Server."));

    setup_timer();

    gmap_server_init(amxut_bus_dm(), amxut_bus_parser());

    test_util_scanMibDir("../test_common/mibs");

    handle_events();
    /* Allow NTP time synchronization to settle. */
    amxut_timer_go_to_future_ms(30000);

    return 0;
}

int util_teardown(void** state) {
    gmap_server_cleanup();

    amxo_parser_clean(amxut_bus_parser());
    amxd_dm_clean(amxut_bus_dm());
    amxut_bus_teardown(state);

    return 0;
}


static void s_call_helper(const char* func_name, const char* upper_device, const char* lower_device, const char* datasource, const char* type, bool expect_success) {
    // init:
    amxc_var_t args;
    amxc_var_t ret;
    amxd_status_t status = amxd_status_unknown_error;
    amxd_object_t* devices = amxd_dm_findf(amxut_bus_dm(), "Devices");
    assert_ptr_not_equal(devices, NULL);
    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    if(upper_device != NULL) {
        amxc_var_add_key(cstring_t, &args, "upper_device", upper_device);
    }
    if(lower_device != NULL) {
        amxc_var_add_key(cstring_t, &args, "lower_device", lower_device);
    }
    if(datasource != NULL) {
        amxc_var_add_key(cstring_t, &args, "datasource", datasource);
    }
    if(type != NULL) {
        amxc_var_add_key(cstring_t, &args, "type", type);
    }

    // call:
    status = amxd_object_invoke_function(devices,
                                         func_name,
                                         &args,
                                         &ret);
    handle_events();

    // check:
    assert_true((status == amxd_status_ok) == expect_success);
    assert_true(amxc_var_get_bool(&ret) == expect_success);

    // cleanup:
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

void util_call_linkAdd(const char* upper_device, const char* lower_device, const char* datasource, const char* type, bool expect_success) {
    s_call_helper("linkAdd", upper_device, lower_device, datasource, type, expect_success);
}

void util_call_linkReplace(const char* upper_device, const char* lower_device, const char* datasource, const char* type, bool expect_success) {
    s_call_helper("linkReplace", upper_device, lower_device, datasource, type, expect_success);
}

void util_call_linkRemove(const char* upper_device, const char* lower_device, const char* datasource, bool expect_success) {
    s_call_helper("linkRemove", upper_device, lower_device, datasource, NULL, expect_success);
}
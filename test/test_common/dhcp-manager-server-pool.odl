%define {
    object DHCPv4Server {
        /**
         * DHCPv4.Server.Pool from TR-181 datamodel 2.11
         *
         * @version 1.0
         */
        %persistent object Pool[] {
            /**
             * RPC method to remove the states of the leases given in the list for MAC addresses.
             * If no MAC addresses is given then all leases are removed.
             * @param MACs : list containing all the MAC of the static ips to remove.
             * @return bool : true on success
             */
            bool cleanClientLeases(%in list MACs);

            /**
             * DHCPv4.Server.Pool.Client from TR-181 datamodel 2.11
             *
             * @version 1.0
             */
            %persistent object Client[] {
                /**
                 * Alias for the instance
                 *
                 * @version 1.0
                 * type: string(64)
                 */
                %unique %key string Alias {
                    on action validate call check_maximum_length 64;
                }

                /**
                 * MAC Address associated to the client
                 *
                 * @version 1.0
                 * type: string(17)
                 */
                %persistent string Chaddr = "";
            }
        }
    }
    object DHCPv6Server {
        /**
         * DHCPv6.Server.Pool from TR-181 datamodel 2.11
         *
         * @version 1.0
         */
        %persistent object Pool[] {
            /**
             * RPC method to remove the states of the leases given in the list for MAC addresses.
             * If no MAC addresses is given then all leases are removed.
             * @param MACs : list containing all the MAC of the static ips to remove.
             * @return bool : true on success
             */
            bool cleanClientLeases(%in list MACs);

            /**
             * DHCPv6.Server.Pool.Client from TR-181 datamodel 2.11
             *
             * @version 1.0
             */
            %persistent object Client[] {
                /**
                 * Alias for the instance
                 *
                 * @version 1.0
                 * type: string(64)
                 */
                %unique %key string Alias {
                    on action validate call check_maximum_length 64;
                }

                /**
                 * MAC Address associated to the client
                 *
                 * @version 1.0
                 * type: string(17)
                 */
                %persistent string Chaddr = "";
            }
        }
    }
}

%populate {
    object DHCPv4Server.Pool {
        instance add("lan");
    }

    object DHCPv4Server.Pool.lan.Client {
        instance add("clientv4") {
            parameter Chaddr = "11:aa:22:bb:33:cc";
        }
    }

    object DHCPv6Server.Pool {
        instance add("lan");
    }

    object DHCPv6Server.Pool.lan.Client {
        instance add("clientv6") {
            parameter Chaddr = "11:aa:22:bb:33:cc";
        }
    }
}
/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <setjmp.h>
#include <cmocka.h>

#include "test_gmap_import_pcm.h"
#include "../test_common/test_util.h"
#include "gmaps_devices.h"
#include <gmap/gmap.h>

#include <amxc/amxc.h>
#include <amxp/amxp_signal.h>
#include <amxp/amxp_slot.h>

#include <amxd/amxd_common.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_object_event.h>
#include <amxut/amxut_bus.h>
#include <amxut/amxut_dm.h>
#include <amxut/amxut_util.h>

#include "gmaps_devices.h"
#include "gmaps_import_pcm.h"
#include "test_gmap_import_pcm_setup_teardown.h"

static int32_t s_pcmimport_empty_callcount = -1;
static int32_t s_pcmimport_check_self_callcount = -1;

/** @implements amxd_object_fn_t */
static amxd_status_t s_mock_mod_pcm_pcmimport_check_self (amxd_object_t* object UNUSED,
                                                          amxd_function_t* func UNUSED,
                                                          amxc_var_t* args,
                                                          amxc_var_t* ret UNUSED) {

    // Check that the identifier is set:
    amxut_dm_param_equals_cstring_t("Devices.Device.self", "Alias", "self");
    amxut_dm_param_equals_cstring_t("Devices.Device.self", "Key", "self");

    // Check that tags are set:
    amxut_dm_param_equals_ssv_string_t("Devices.Device.self", "Tags", "self protected physical hgw mac ssw_local");

    // Check parameter that only exists after "mac" is set in Tags exists:
    // Note that that filling in the value of the parameter is the respensibility of mod-pcm's
    // PcmImport implementation, so the parameter's value is still empty here.
    // But it's important that we check here that the parameter exists, because otherwise
    // mod-pcm won't be able to fill in its value:
    amxut_dm_param_equals_cstring_t("Devices.Device.self", "PhysAddress", "");

    // Check the device is registered in gmap's bookkeeping:
    assert_non_null(gmaps_get_device("self"));
    assert_ptr_equal(amxd_dm_findf(amxut_bus_dm(), "Devices.Device.self"), gmaps_get_device("self"));

    // Check the arguments are passed to mod_pcm's RPC function:
    assert_string_equal(GETP_CHAR(args, "data.type"), "usersetting");
    assert_string_equal(GETP_CHAR(args, "data.type"), "usersetting");
    assert_string_equal(GETP_CHAR(args, "data.set.hgwconfig.Devices.Device.0.self.Names.0.default.Name"), "HGW");

    s_pcmimport_check_self_callcount++;

    return amxd_status_ok;
}

static amxd_status_t s_mock_mod_pcm_pcmimport_empty (amxd_object_t* object UNUSED,
                                                     amxd_function_t* func UNUSED,
                                                     amxc_var_t* args UNUSED,
                                                     amxc_var_t* ret UNUSED) {

    s_pcmimport_empty_callcount++;

    return amxd_status_ok;
}

static void s_create_pcmimport_function(amxd_object_fn_t impl) {
    amxd_object_t* devices_obj = amxd_object_findf(amxd_dm_get_root(amxut_bus_dm()), "Devices.");
    amxd_function_t* function = NULL;
    assert_int_equal(0, amxd_function_new(&function,
                                          "PcmImport",
                                          AMXC_VAR_ID_NULL,
                                          impl));
    assert_int_equal(0, amxd_function_set_attr(function, amxd_fattr_protected, true));
    assert_int_equal(0, amxd_function_new_arg(function, "data", AMXC_VAR_ID_HTABLE, NULL));
    assert_int_equal(0, amxd_function_arg_is_attr_set(function, "data", amxd_aattr_in));
    assert_int_equal(0, amxd_function_arg_is_attr_set(function, "data", amxd_aattr_mandatory));
    assert_int_equal(0, amxd_object_add_function(devices_obj, function));
}

static int s_call_import(const char* data_file) {
    amxc_var_t* data = amxut_util_read_json_from_file(data_file);

    amxc_var_t args;
    amxc_var_t ret;
    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_set_key(&args, "data", data, AMXC_VAR_FLAG_COPY);
    int status_call = amxb_call(amxb_be_who_has("Devices"),
                                "Devices.",
                                "Import",
                                &args,
                                &ret,
                                1);
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    amxc_var_delete(&data);

    return status_call;
}

void test_import_pcm(UNUSED void** state) {
    test_util_scanMibDir("../test_common/mibs");
    s_pcmimport_empty_callcount = 0;
    s_create_pcmimport_function(s_mock_mod_pcm_pcmimport_empty);

    // GIVEN upgrade-persistent configuration data
    const char* data_file = "gmap-server_Devices-one-device.json";

    // WHEN importing the upgrade-persistent configuration data
    int status_call = s_call_import(data_file);

    // THEN this succeeded
    assert_int_equal(0, status_call);
    amxc_var_t values;
    amxc_var_init(&values);
    gmap_status_t status = gmaps_device_get("self", &values, GMAP_NO_ACTIONS);
    assert_int_equal(status, gmap_status_ok);
    // THEN the import function of PCM itself is called to continue doing the non-custom part of importing
    assert_int_equal(s_pcmimport_empty_callcount, 1);

    amxc_var_clean(&values);
}

void test_import_pcm_multiple_devices(UNUSED void** state) {
    test_util_scanMibDir("../test_common/mibs");
    s_pcmimport_check_self_callcount = 0;
    s_create_pcmimport_function(s_mock_mod_pcm_pcmimport_check_self);

    // GIVEN upgrade-persistent configuration data
    const char* data_file = "gmap-server_Devices.json";

    // WHEN importing the upgrade-persistent configuration data
    int status_call = s_call_import(data_file);

    // THEN multiple devices were created
    assert_int_equal(0, status_call);
    assert_non_null(gmaps_get_device("self"));
    assert_non_null(gmaps_get_device("bridge-lan_bridge"));
    assert_non_null(gmaps_get_device("bridge-guest_bridge"));
    assert_non_null(gmaps_get_device("ethIntf-ETH0"));
    assert_non_null(gmaps_get_device("ethIntf-ETH1"));
    assert_non_null(gmaps_get_device("ssid-vap5g0priv"));
    assert_non_null(gmaps_get_device("ssid-vap2g0priv"));
    assert_non_null(gmaps_get_device("ethIntf-ETH2"));
    assert_non_null(gmaps_get_device("ssid-vap6g0priv"));
    // AND THEN the asserts in s_mock_mod_pcm_pcmimport_check_self were checked.
    assert_int_equal(s_pcmimport_check_self_callcount, 1);
}
